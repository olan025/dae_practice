# ------------------------------------------------------------------------------
# + Connect to UCSM
# ------------------------------------------------------------------------------

$user = $ucsadmin
$password = $ucspsswd | ConvertTo-SecureString -AsPlainText -Force
$cred = New-Object system.Management.Automation.PSCredential($user, $password)
$handle1 = Connect-Ucs $ucsvip -Credential $cred

