﻿Param(
  [Parameter(Mandatory=$True,Position=1)]
  [string]$datasource_name,
  [Parameter(Mandatory=$True,Position=2)]
  [string]$build_cmds,
  [Parameter(Mandatory=$True,Position=3)]
  [string]$build_docs,
  [Parameter(Mandatory=$True,Position=4)]
  [string]$build_id
  
)
# temp adding of functions for testing
function Build-CmdHeader
	{
	Param(
		[Parameter(Mandatory=$True,Position=1)]
		[string]$ch_str
  		)
	$ch_break 		= "# " + "-" * 78
	$ch_contents 	= New-Object System.Collections.ArrayList
	[Void]$ch_contents.Add($ch_break)
	
	if ($ch_str -match ";")
		{
		$ch_strs = $ch_str.Split(";")
		$ch_strs | %{
			$ch_txt_line = "# + " + $_
			[Void]$ch_contents.Add($ch_txt_line)
			}
		[Void]$ch_contents.Add($ch_break)
		[Void]$ch_contents.Add("")
		}
	else
		{
		$ch_txt_line = "# + " + $ch_str
		[Void]$ch_contents.Add($ch_txt_line)
		[Void]$ch_contents.Add($ch_break)
		[Void]$ch_contents.Add("")
		}
	$ch_contents
	}

function Build-CmdContent
	{
	Param(
		[Parameter(Mandatory=$True,Position=1)]
		[string]$cc_str
  		)
	$cc_contents 	= New-Object System.Collections.ArrayList
	
	if ($cc_str -match ";")
		{
		$cc_strs = $cc_str.Split(";")
		$cc_strs | %{
			[Void]$cc_contents.Add($_)
			}
		}
	else
		{
		[Void]$cc_contents.Add($cc_str)
		}
	$cc_contents
	}

$this_script = ($MyInvocation.MyCommand).Name

#region Object Definition
$ucs_assumed_variables				= New-Object System.Collections.ArrayList
$ucs_connection						= New-Object System.Collections.ArrayList
$ucs_global_disc_policy				= New-Object System.Collections.ArrayList
$ucs_conf_fi_server_ports			= New-Object System.Collections.ArrayList
$ucs_admin_settings					= New-Object System.Collections.ArrayList
$ucs_conf_ldap						= New-Object System.Collections.ArrayList
$ucs_remove_defaults				= New-Object System.Collections.ArrayList
$ucs_create_lan_pCh					= New-Object System.Collections.ArrayList
$ucs_create_vsans					= New-Object System.Collections.ArrayList
$ucs_create_mgmt_fw_pkg				= New-Object System.Collections.ArrayList
$ucs_create_pools					= New-Object System.Collections.ArrayList
$ucs_create_qos_system_classes		= New-Object System.Collections.ArrayList
$ucs_create_qos_policies			= New-Object System.Collections.ArrayList
$ucs_create_cdp_policy				= New-Object System.Collections.ArrayList
$ucs_create_vlans					= New-Object System.Collections.ArrayList
$ucs_create_vnic_templates			= New-Object System.Collections.ArrayList
$ucs_create_vhba_templates			= New-Object System.Collections.ArrayList
$ucs_create_bios_policies			= New-Object System.Collections.ArrayList
$ucs_create_boot_policies			= New-Object System.Collections.ArrayList
$ucs_create_maint_policy			= New-Object System.Collections.ArrayList
$ucs_create_diskbios_scrub_policy	= New-Object System.Collections.ArrayList
$ucs_create_power_policy			= New-Object System.Collections.ArrayList
$ucs_create_vnic_vhba_placement_pol	= New-Object System.Collections.ArrayList
$ucs_create_service_prof_templates	= New-Object System.Collections.ArrayList
$ucs_deploy_service_prof_templates	= New-Object System.Collections.ArrayList
$ucs_conf_snmp						= New-Object System.Collections.ArrayList
$ucs_conf_callhome					= New-Object System.Collections.ArrayList
$all_cmds							= @()
#endregion

### Variables from Pre-Site

### Assumed Variables Section
$ucs_base_mac_prefix = "00:25:B5:"
$ucs_uuid_suffix_pool_assignment = "sequential"
$ucs_mac_pool_a_name = "Fabric-A"
$ucs_callhome_admin_state = "on"
$ucs_callhome_from 	=  '$ucs_domain_name + "@" + $dns_domain_name'
$ucs_callhome_profile_ciscotac = "callhome@cisco.com"
$ucs_callhome_system_inventory_send_periodically = "on"
$ucs_auth_ldap_provider_filter = 'sAMAccountName=$userid'
$ucs_auth_ldap_provider_vendor = "MS AD"
$ucs_auth_ldap_provider_group_auth = "enable"
$ucs_auth_ldap_provider_group_recursion = "recursive"
$ucs_auth_ldap_group_map_role ="admin"
$ucs_auth_domain_local_name = "local"
$ucs_auth_domain_local_realm = "Local"
$ucs_auth_domain_ldap_realm = "Ldap"
$ucs_auth_native_default_realm = "Ldap"
$ucs_auth_native_role_remote_users = "No Login"
$ucs_base_mac_prefix = "00:25:B5:"
$ucs_uuid_suffix_pool = "0" + $ucs_site_id + "0" + $ucs_domain_id + "-000000000001"
$ucs_uuid_suffix_pool_assignment = "sequential"
$ucs_mac_pool_a_name = "Fabric-A"
$ucs_mac_pool_a_base = $ucs_base_mac_prefix + $ucs_site_id + $ucs_domain_id + ":" + "A"
$ucs_mac_pool_a_start_postfix = $ucs_mac_pool_a_base + "0:00"
$ucs_mac_pool_a_end_postfix= $ucs_mac_pool_a_base + "0:FF"
$ucs_mac_pool_b_base = $ucs_base_mac_prefix + $ucs_site_id + $ucs_domain_id + ":" + "B"
$ucs_mac_pool_b_start_postfix = $ucs_mac_pool_b_base + "0:00"
$ucs_mac_pool_b_end_postfix = $ucs_mac_pool_b_base + "0:FF"
$ucs_mac_pool_a_pool_assignment = "sequential"
$ucs_mac_pool_b_name = "Fabric-B"
$ucs_mac_pool_b_pool_assignment = "sequential"
$ucs_wwnn_pool_name = "Lab_WWNN"
$ucs_wwn_pool_prefix = "20:00:00:25:B5:"
$ucs_wwn_pool_base = $ucs_wwn_pool_prefix + $ucs_site_id + $ucs_domain_id + ":"	
$ucs_wwnn_pool_start = $ucs_wwn_pool_base + "0:00"
$ucs_wwnn_pool_end = $ucs_wwn_pool_base + "0:FF"
$ucs_wwnn_pool_assignment = "sequential"
$ucs_wwpn_pool_a_name = "Fabric-A"
$ucs_wwpn_pool_base = $ucs_wwn_pool_prefix + $ucs_site_id + $ucs_domain_id + ":"	
$ucs_wwpn_pool_a_start = $ucs_wwpn_pool_base + "A0.00"	
$ucs_wwpn_pool_a_end = $ucs_wwpn_pool_base + "A0.FF"	
$ucs_wwpn_pool_a_pool_assignment = "sequential"
$ucs_wwpn_pool_b_name = "Fabric-B"
$ucs_wwpn_pool_b_start = $ucs_wwpn_pool_base + "B0.00"	
$ucs_wwpn_pool_b_end = $ucs_wwpn_pool_base + "B0.FF"	
$ucs_wwpn_pool_b_pool_assignment = "sequential"

### Foreach Loop Section
$ucs_qos_policy_gold_name = "Gold"
$ucs_qos_policy_gold_priority = $ucs_qos_policy_gold_name	
$ucs_qos_policy_silver_name = "Silver"
$ucs_qos_policy_silver_priority = $ucs_qos_policy_silver	
$ucs_qos_policy_bronze_name = "Bronze"
$ucs_qos_policy_bronze_priority = $ucs_qos_policy_bronze	
$ucs_qos_poilicy_besteffort_name = "Best-Effort"
$ucs_qos_policy_besteffort_priority = "Best-Effort"

$ucs_network_control_policy_cdp_name = "CDP-Enable"
$ucs_network_control_policy_cdp	= "CDP enabled"

$ucs_host_firmware_package_type = "simple"
#$ucs_host_firmware_package_name = "2.2(1c)"	 (modified?)
$ucs_local_disk_policy_name = "RAID1"
$ucs_local_disk_policy_mode = "RAID 1"
$ucs_maintenance_policy_name = "user_ack"
$ucs_maintenance_policy_type = "User Ack"
$ucs_scrub_policy_name = "BIOS_Only"
$ucs_scrub_policy_BIOS_settings = "yes"

$ucs_esx_vnic_tmpl_mgmt_a_fabric = "A"
$ucs_esx_vnic_tmpl_mgmt_a_type	= "Updating"
$ucs_esx_vnic_tmpl_mgmt_a_mtu	= "9000"

# line 183 in the ucs_presite.xls 

















#$header_string = 	"Assumed Variables"
#$header_content = 	"$ucs_base_mac_prefix;" +
#					"$ucs_uuid_suffix_pool_assignment;"
#
#[Void]$ucs_assumed_variables.Add((Build-CMDHeader $header_string))
#[Void]$ucs_assumed_variables.Add((Build-CMDContent $header_content))

### Connection Section
$header_string = 	"Connect to UCSM"
$header_content = 	'$user = $ucsadmin;' +
					'$password = $ucspsswd | ConvertTo-SecureString -AsPlainText -Force;' +
					'$cred = New-Object system.Management.Automation.PSCredential($user, $password);' +
					'$handle1 = Connect-Ucs $ucsvip -Credential $cred;' +
[Void]$ucs_connection.Add((Build-CMDHeader $header_string))
[Void]$ucs_connection.Add((Build-CMDContent $header_content))

$all_cmds = $ucs_connection +
			$ucs_global_disc_policy
			
$all_cmds | Out-File "test.ps1"
