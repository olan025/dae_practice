######################################################################################################################################
# Setup Prerequisit Requirements - VERY IMPORTATNT TO READ THIS LIST FOR PROPER SCRIPT OPERATION                                     #
#   - Microsoft PowerShell 2 is required. PowerShell 2 is part of Windows 7/2008 R2			   		                        	     #
#   - Cisco UCS PowerTool version 0.98 for UCSM - http://developer.cisco.com/web/unifiedcomputing/pshell-download                    #
#   - A UCS cluser that the initail IPs and cluster config have been completed from the console                                      #   
#   - Put the UCS Manager VIP IP address in the $ucsvip field                                                                        #
#   - Put the UCS Administrator name in the $ucsadmin variable below							                            	     #
#   - Put the UCS Administrator password in the $ucspsswd variable below					  		                                 #
#   - IN THE INPUT VARIABLES SECTION PROVIDE VALID VALUES FOR ALL VARIABLES                                                          # 
#   - For Site 1 UCS cluster 1 the MAC,UUID,WWPN,WWNN should be 11 in the site/cluster octet                                         #
#   - For Site 2 UCS cluster 1 the MAC,UUID,WWPN,WWNN should be 21 in the site/cluster octet                                         #
#   - For the LAN/SAN Port-Channels in the below script set the correct port and slot IDs                                            #
#   - In the below script set the proper VLANs and names and then match those to the proper vNIC templates                           #
#   - After the script completes login to UCSM and verify config                                                                     #
#   - Enable the LAN/SAN port-channels and verify they come up on both sides                                                         #
#   - Create the esxi-hosts server pool and addd the appropriate servers to the server pool                                          #
#   - Associate the ESXi Service Profile Template with the esxi-hosts server pool                                                    #
#															                                                                         #
######################################################################################################################################

#######################
# Input variables here#
#######################
$ucsvip = "10.207.175.197"
$ucsadmin = "admin"
$ucspsswd = "Datalink1"
$podnum = "11"
$serverports = "18-21"
$ethuplinkports = "1-4"
$ethuplinkslot = "1"
$ethpca1 = "101"
$ethpcb1 = "102"
$ethpcnamea1 = "vPC101"
$ethpcnameb1 = "vPC102"
$vsana = "10"
$vsanb = "20"
$fcvsannamea = "10"
$fcvsannameb = "20"
$fcoevlana = "3010"
$fcoevlanb = "3020"
$frmwarever = "2.2.2c"
$dns = "10.207.100.10,10.207.100.11"
$ntp = "10.207.100.10,10.207.100.11"
$timezone = "America/Chicago"
$smtpserver = "smtp.datalinklabs.local"
$phyaddr = "10050 Crosstown Cir. Suite 500, Eden Prairie, MN 55344"
$contactname = "Datalinklabs NOC"
$contactphone = "+19522795900"
$contactemail = "operations@datalinklabs.local"
$smtpfrom = "ucs@datalinklabs.local"
$smtprcpt = "operations@datalinklabs.local"
$adbasedn = "DC=DatalinkLabs,DC=local"
$adbinddn = "CN=ucsbind,DC=DatalinkLabs,DC=local"
$adadmindn = "CN=Lab Advocates,OU=Groups,OU=Labs,DC=DatalinkLabs,DC=local"
$domainname = "datalinklabs.local"
$defgw = "10.207.175.1"
$cimcfrom = "10.207.175.233"
$cimcto = "10.207.175.252"
$uuidname = "ESXi_UUID"
$uuidfrom = "00" + $podnum + "-110000000001"
$uuidto = "00" + $podnum + "-110000000100"
$macpoola = "Fabric-A"
$macpoolb = "Fabric-B"
$macafrom = "00:25:B5:" + $podnum + ":A0:00"
$macato = "00:25:B5:" + $podnum + ":A0:FF"
$macbfrom = "00:25:B5:" + $podnum + ":B0:00"
$macbto = "00:25:B5:" + $podnum + ":B0:FF"
$slotarray = (1,2,3,4,5,6,7,8)

$BootTarget1 = "50:00:00:00:00:00:00:A1"
$BootTarget2 = "50:00:00:00:00:00:00:B1"

$SiloCount = 1
$ProfileID = "ESXi"
$BootType = "BFS"
$HbaArray = @()
$NicArray = @()
$WwnnArray = @()
$WwpnArray = @()
$BootPolicyArray = @()
$BootLunid = "0"
$Fabric = ("A","B")
$HbaNameA = "vmhba1"
$HbaNameB = "vmhba2"


# $wwnnfrom = "20:11:00:25:B5:" + $podnum + ":00:00"
# $wwnnto = "20:11:00:25:B5:" + $podnum + ":00:FF"
# $wwpnafrom = "20:00:00:25:B5:" + $podnum + ":A0:10"
# $wwpnato = "20:00:00:25:B5:" + $podnum + ":A0:89"
# $wwpnbfrom = "20:00:00:25:B5:" + $podnum + ":BB:10"
# $wwpnbto = "20:00:00:25:B5:" + $podnum + ":BB:89"
# $BootTarget1 = "50:00:00:00:00:00:00:00"
# $BootTarget2 = "50:00:00:00:00:00:00:00"

$nictemplate_a = "ESXi_vNIC_A"
$nictemplate_b = "ESXi_vNIC_B"
$esxprefix = "ucsesx"
$vlanarray = (12,16,20,24,28,32,2072,2076,2080,2084,2088,2092,2096,2100,2104,2108,2112,2116,2120,2124,3028,3032,3036,3040)
$mgmtvlan = "4"
$vmovlan = "8"
$srlocation = "CTC SR5"

# $windowsprefix= "ucsw2k8-"
# $fcpca = "11"
# $fcpcb = "12"
# $fcuplinkport1 = "1"
# $fcuplinkport2 = "2"
# $fcuplinkslot = "2"
	$ucs_cleanup						= New-Object System.Collections.ArrayList
	$ks_header							= New-Object System.Collections.ArrayList
	$ks_disk_partitioning				= New-Object System.Collections.ArrayList
	$ks_install_media					= New-Object System.Collections.ArrayList
	$ks_root_password					= New-Object System.Collections.ArrayList
	$ks_default_mgmt_int				= New-Object System.Collections.ArrayList
	$ks_default_mgmt_int_data			= New-Object System.Collections.ArrayList
	$ks_reboot_after_install_flag		= New-Object System.Collections.ArrayList
	$ks_script_pre						= New-Object System.Collections.ArrayList
	$ks_script_post						= New-Object System.Collections.ArrayList
	$ks_script_1st_reboot				= New-Object System.Collections.ArrayList
	$ks_vswitch_config					= New-Object System.Collections.ArrayList
#Cleanup any unneeded stuffs



	[Void]$ucs_cleanup.Add("$section_break")
	[Void]$ucs_cleanup.Add('Get-UcsOrg | Where-Object {$_.Name -ne "root"} | Remove-UcsOrg -Force')
	[Void]$ucs_cleanup.Add("Get-UcsServerPool | Remove-UcsServerPool -Force")
	[Void]$ucs_cleanup.Add("Get-UcsVnicTemplate | Remove-UcsVnicTemplate -Force")
	[Void]$ucs_cleanup.Add("Get-UcsMacPool | Remove-UcsMacPool -Force")
	[Void]$ucs_cleanup.Add('Get-UcsIpPool | Where-Object {$_.name -ne "ext-mgmt"} | Remove-UcsIpPool -Force')
	[Void]$ucs_cleanup.Add("Get-UcsServiceProfile | Remove-UcsServiceProfile -Force")
	[Void]$ucs_cleanup.Add("Get-UcsLdapProvider | Remove-UcsLdapProvider -Force")
	[Void]$ucs_cleanup.Add("Get-UcsProviderGroup | Remove-UcsProviderGroup -Force")
	[Void]$ucs_cleanup.Add("Get-UcsLanCloud | Get-UcsVlan | Where-Object {$_.name -ne "default"} | Remove-UcsVlan -Force")
	[Void]$ucs_cleanup.Add("Get-UcsVhbaTemplate | Remove-UcsVhbaTemplate -Force")
	[Void]$ucs_cleanup.Add("Get-UcsPowerPolicy | Remove-UcsPowerPolicy -Force")
	[Void]$ucs_cleanup.Add("Get-UcsMaintenancePolicy | Where-Object {$_.name -ne "default"} | Remove-UcsMaintenancePolicy -Force")
	[Void]$ucs_cleanup.Add("Get-UcsLocalDiskConfigPolicy | Remove-UcsLocalDiskConfigPolicy -Force")
	[Void]$ucs_cleanup.Add("Get-UcsBootPolicy | Remove-UcsBootPolicy -Force")
	[Void]$ucs_cleanup.Add("Get-UcsBiosPolicy | Remove-UcsBiosPolicy -Force")
	[Void]$ucs_cleanup.Add("Get-UcsQosPolicy | Remove-UcsQosPolicy -Force")
	[Void]$ucs_cleanup.Add("Get-UcsUuidSuffixPool | Remove-UcsUuidSuffixPool -Force")
	[Void]$ucs_cleanup.Add("$section_break")
	[Void]$ucs_cleanup.Add("# root password in MD5 format")

#########################################
# Populate name arrays			#
#########################################
For ($i=1; $i -lt $SiloCount+1; $i++)  {
	$HbaName = $ProfileID + "_HBA_" + $i
	$NicName = $ProfileID + "_NIC_" + $i
	$WwnnName = $ProfileID + "_WWNN_" + $i
	$WwpnName = $ProfileID + "_WWPN_" + $i
	$BootPolicyName = $ProfileID + "_" + $BootType + "_" + $i
	$ProfileTempName = $ProfileID + "_Silo"  + $i

	$HbaArray = $HbaArray + $HbaName
	$NicArray = $NicArray + $NicName
	$WwnnArray = $WWNNArray + $WwnnName
	$WwpnArray = $WwpnArray + $WWPNName
	$BootPolicyArray = $BootPolicyArray + $BootPolicyName
}



#########################################
# Import the Cisco UCS PowerTool module #
#########################################
Import-Module CiscoUcsPS

############################################
# Authenticate to UCSM with the admin user #
############################################
$user = $ucsadmin
$password = $ucspsswd | ConvertTo-SecureString -AsPlainText -Force
$cred = New-Object system.Management.Automation.PSCredential($user, $password)
$handle1 = Connect-Ucs $ucsvip -Credential $cred

##############################################################
# Set Global System Policies for chassis discovery and power #
##############################################################
Get-UcsChassisDiscoveryPolicy | Set-UcsChassisDiscoveryPolicy -Action 2-link -LinkAggregationPref port-channel -Force
Get-UcsPowerControlPolicy | Set-UcsPowerControlPolicy -Redundancy grid -Force

##############################################
# Configure Fabric Interconnect Server Ports #
##############################################
Start-UcsTransaction
Get-UcsFabricServerCloud -Id A | Add-UcsServerPort -AdminState enabled -PortId $serverport1 -SlotId $ethuplinkslot
Get-UcsFabricServerCloud -Id A | Add-UcsServerPort -AdminState enabled -PortId $serverport2 -SlotId $ethuplinkslot
Get-UcsFabricServerCloud -Id A | Add-UcsServerPort -AdminState enabled -PortId $serverport3 -SlotId $ethuplinkslot
Get-UcsFabricServerCloud -Id A | Add-UcsServerPort -AdminState enabled -PortId $serverport4 -SlotId $ethuplinkslot
Complete-UcsTransaction

Start-UcsTransaction
Get-UcsFabricServerCloud -Id B | Add-UcsServerPort -AdminState enabled -PortId $serverport1 -SlotId $ethuplinkslot
Get-UcsFabricServerCloud -Id B | Add-UcsServerPort -AdminState enabled -PortId $serverport2 -SlotId $ethuplinkslot
Get-UcsFabricServerCloud -Id B | Add-UcsServerPort -AdminState enabled -PortId $serverport3 -SlotId $ethuplinkslot
Get-UcsFabricServerCloud -Id B | Add-UcsServerPort -AdminState enabled -PortId $serverport4 -SlotId $ethuplinkslot
Complete-UcsTransaction


##########################
# Set UCS Admin Settings #
##########################
# Get-UcsNativeAuth | Set-UcsNativeAuth -ConLogin local -DefLogin local -DefRolePolicy no-login -Force
Add-UcsDnsServer -Name $dns1
Add-UcsDnsServer -Name $dns2
Set-UcsTimezone -Timezone $timezone -Force
Add-UcsNtpServer -Name $ntp1
Add-UcsNtpServer -Name $ntp2

######################
# Configure LDAP     #
######################
Start-UcsTransaction
Set-UcsLdapGlobalConfig -Attribute "" -Basedn $adbasedn -Descr "" -FilterValue "cn=`$userid" -Retries 1 -Timeout 30 -Force
Complete-UcsTransaction

Start-UcsTransaction
$mo = Add-UcsLdapProvider -Attribute "" -Basedn $adbasedn -Descr "" -EnableSSL "no" -FilterValue "" -Key "7b56Yc3t" -Name $domainname -Order "lowest-available" -Port 389 -Retries 1 -Rootdn $adbinddn -Timeout 30 -vendor "MS-AD"
$mo_1 = $mo | Add-UcsLdapGroupRule -ModifyPresent -Authorization "enable" -Descr "" -TargetAttr "memberOf" -Traversal "recursive"
Complete-UcsTransaction

Start-UcsTransaction
$mo = Add-UcsLdapGroupMap -Descr "" -Name $adadmindn
$mo_1 = $mo | Add-UcsUserRole -Descr "" -Name "admin"
Complete-UcsTransaction

Start-UcsTransaction
$mo = Get-UcsLdapGlobalConfig | Add-UcsProviderGroup -Descr "" -Name $domainname
$mo_1 = $mo | Add-UcsProviderReference -ModifyPresent -Descr "" -Name $domainname -Order "1"
Complete-UcsTransaction

#########################################################
# Remove default Server, UUID, WWNN, WWPN and MAC pools #
#########################################################
Get-UcsServerPool -Name default -LimitScope | Remove-UcsServerPool -Force
Get-UcsUuidSuffixPool -Name default -LimitScope | Remove-UcsUuidSuffixPool -Force
Get-UcsWwnPool -Name node-default -LimitScope | Remove-UcsWwnPool -Force
Get-UcsWwnPool -Name default -LimitScope | Remove-UcsWwnPool -Force
Get-UcsMacPool -Name default -LimitScope | Remove-UcsMacPool -Force
Get-UcsManagedObject -Dn org-root/iqn-pool-default | Remove-UcsManagedObject -Force

############################
# Create LAN Port-Channels #
############################
Start-UcsTransaction
$mo = Get-UcsFabricLanCloud -Id A | Add-UcsUplinkPortChannel -AdminState disabled -Name $ethpcnamea1 -PortId $ethpca1
$mo_1 = $mo | Add-UcsUplinkPortChannelMember -ModifyPresent -AdminState enabled -PortId $ethuplinkport1 -SlotId $ethuplinkslot
$mo_2 = $mo | Add-UcsUplinkPortChannelMember -ModifyPresent -AdminState enabled -PortId $ethuplinkport2 -SlotId $ethuplinkslot
Complete-UcsTransaction

Start-UcsTransaction
$mo = Get-UcsFabricLanCloud -Id B | Add-UcsUplinkPortChannel -AdminState disabled -Name $ethpcnameb1 -PortId $ethpcb1
$mo_1 = $mo | Add-UcsUplinkPortChannelMember -ModifyPresent -AdminState enabled -PortId $ethuplinkport1 -SlotId $ethuplinkslot
$mo_2 = $mo | Add-UcsUplinkPortChannelMember -ModifyPresent -AdminState enabled -PortId $ethuplinkport2 -SlotId $ethuplinkslot
Complete-UcsTransaction

#Start-UcsTransaction
#$mo = Get-UcsFabricLanCloud -Id A | Add-UcsUplinkPortChannel -AdminState disabled -Name $ethpcnamea2 -PortId $ethpca2
#$mo_1 = $mo | Add-UcsUplinkPortChannelMember -ModifyPresent -AdminState enabled -PortId $ethuplinkport3 -SlotId $ethuplinkslot

#$mo_2 = $mo | Add-UcsUplinkPortChannelMember -ModifyPresent -AdminState enabled -PortId $ethuplinkport4 -SlotId $ethuplinkslot
#Complete-UcsTransaction

#Start-UcsTransaction
#$mo = Get-UcsFabricLanCloud -Id B | Add-UcsUplinkPortChannel -AdminState disabled -Name $ethpcnameb2 -PortId $ethpcb2
#$mo_1 = $mo | Add-UcsUplinkPortChannelMember -ModifyPresent -AdminState enabled -PortId $ethuplinkport3 -SlotId $ethuplinkslot
#$mo_2 = $mo | Add-UcsUplinkPortChannelMember -ModifyPresent -AdminState enabled -PortId $ethuplinkport4 -SlotId $ethuplinkslot
#Complete-UcsTransaction

################
# Create VSANs #
################
Get-UcsFabricSanCloud -Id A | Add-UcsVsan -FcoeVlan $fcoevlana -Id $vsana -Name $fcvsannamea
Get-UcsFabricSanCloud -Id B | Add-UcsVsan -FcoeVlan $fcoevlanb -Id $vsanb -Name $fcvsannameb

############################
# Create SAN Port-Channels #
############################
# Start-UcsTransaction
# $mo = Get-UcsFabricSanCloud -Id A | Add-UcsFcUplinkPortChannel -AdminState disabled -Name $fcvsannamea -PortId $fcpca
# $mo_1 = $mo | Add-UcsFabricFcSanPcEp -ModifyPresent -AdminState enabled -PortId $fcuplinkport1 -SlotId $fcuplinkslot
# $mo_2 = $mo | Add-UcsFabricFcSanPcEp -ModifyPresent -AdminState enabled -PortId $fcuplinkport2 -SlotId $fcuplinkslot
# Complete-UcsTransaction
# Get-UcsFabricSanCloud -Id A | Get-UcsVsan -Name $fcvsannamea | Add-UcsVsanMemberFcPortChannel -ModifyPresent -AdminState disabled -PortId $fcpca -SwitchId A

# Start-UcsTransaction
# $mo = Get-UcsFabricSanCloud -Id B | Add-UcsFcUplinkPortChannel -AdminState disabled -Name $fcvsannameb -PortId $fcpcb
# $mo_1 = $mo | Add-UcsFabricFcSanPcEp -ModifyPresent -AdminState enabled -PortId $fcuplinkport1 -SlotId $fcuplinkslot
# $mo_2 = $mo | Add-UcsFabricFcSanPcEp -ModifyPresent -AdminState enabled -PortId $fcuplinkport2 -SlotId $fcuplinkslot
# Complete-UcsTransaction
# Get-UcsFabricSanCloud -Id B | Get-UcsVsan -Name $fcvsannameb | Add-UcsVsanMemberFcPortChannel -ModifyPresent -AdminState disabled -PortId $fcpcb -SwitchId B

################################
# Create Host Firmware Package #
################################
Start-UcsTransaction
$mo = Get-UcsOrg -Level root  | Add-UcsFirmwareComputeHostPack -IgnoreCompCheck "yes" -Mode "staged" -Name $frmwarever -StageSize 0 -UpdateTrigger "immediate"
# $mo_1 = $mo | Add-UcsFirmwarePackItem -ModifyPresent -HwModel "B230-BASE-M2" -HwVendor "Cisco Systems Inc" -Type "board-controller" -Version "B230100C"
# $mo_2 = $mo | Add-UcsFirmwarePackItem -ModifyPresent -HwModel "N20-AC0002" -HwVendor "Cisco Systems Inc" -Type "adaptor" -Version "2.0(2q)"
# $mo_3 = $mo | Add-UcsFirmwarePackItem -ModifyPresent -HwModel "B230-BASE-M2" -HwVendor "Cisco Systems, Inc." -Type "blade-bios" -Version "B230.2.0.2b.0.030720121455"
# $mo_4 = $mo | Add-UcsFirmwarePackItem -ModifyPresent -HwModel "N20-B6620-1" -HwVendor "Cisco Systems, Inc." -Type "blade-bios" -Version "S5500.2.0.2c.0.040220121758"
# $mo_5 = $mo | Add-UcsFirmwarePackItem -ModifyPresent -HwModel "N20-B6625-1" -HwVendor "Cisco Systems, Inc." -Type "blade-bios" -Version "S5500.2.0.2c.0.040220121758"
# $mo_6 = $mo | Add-UcsFirmwarePackItem -ModifyPresent -HwModel "SAS1064E PCI-Express Fusion-MPT SAS" -HwVendor "LSI Logic   Symbios Logic" -Type "storage-controller" -Version "01.32.04.00|06.34.00.00|03.22.00.00"
$mo_7 = $mo | Add-UcsFirmwarePackItem -ModifyPresent -HwModel "UCSB-B200-M3" -HwVendor "Cisco Systems Inc" -Type "blade-bios" -Version "B200M3.2.0.3.0.051620121210"
$mo_8 = $mo | Add-UcsFirmwarePackItem -ModifyPresent -HwModel "UCSB-MLOM-40G-01" -HwVendor "Cisco Systems Inc" -Type "adaptor" -Version "2.0(3b)"
$mo_9 = $mo | Add-UcsFirmwarePackItem -ModifyPresent -HwModel "UCS-VIC-M82-8P" -HwVendor "Cisco Systems Inc" -Type "adaptor" -Version "2.0(3b)"
# $mo_10 = $mo | Add-UcsFirmwarePackItem -ModifyPresent -HwModel "SAS1064E PCI-Express Fusion-MPT SAS" -HwVendor "LSI Logic   Symbios Logic" -Type "storage-controller" -Version "01.32.04.00|06.34.00.00|03.22.00.00"
Complete-UcsTransaction

######################################
# Create Management Firmware Package #
######################################
Start-UcsTransaction
$mo = Get-UcsOrg -Level root  | Add-UcsFirmwareComputeMgmtPack -IgnoreCompCheck "yes" -Mode "staged" -Name $frmwarever -StageSize 0 -UpdateTrigger "immediate"
$mo_1 = $mo | Add-UcsFirmwarePackItem -ModifyPresent -HwModel "B230-BASE-M2" -HwVendor "Cisco Systems Inc" -Type "blade-controller" -Version "2.0(3b)"
$mo_2 = $mo | Add-UcsFirmwarePackItem -ModifyPresent -HwModel "UCSB-B200-M3" -HwVendor "Cisco Systems Inc" -Type "blade-controller" -Version "2.0(3b)"
Complete-UcsTransaction

###############################################################
# Create CIMC, UUID, WWNN, WWPN, MAC, IP CIMC and iSCSI pools #
###############################################################
Get-UcsIpPool -org root -Name ext-mgmt -LimitScope | Add-UcsIpPoolBlock -DefGw $defgw -From $cimcfrom -To $cimcto

Start-UcsTransaction
$uuid = Get-UcsOrg -Level root  | Add-UcsUuidSuffixPool -Descr "UUID Pool for Server system board IDs" -Name $uuidname -Prefix derived
$uuid_1 = $uuid | Add-UcsUuidSuffixBlock -From $uuidfrom -To $uuidto
Complete-UcsTransaction

Start-UcsTransaction
$maca = Get-UcsOrg -Level root  | Add-UcsMacPool -Descr "MAC Address Pool for vNICs on Fabric A" -Name $macpoola
$maca_1 = $maca | Add-UcsMacMemberBlock -From $macafrom -To $macato
Complete-UcsTransaction

Start-UcsTransaction
$macb = Get-UcsOrg -Level root  | Add-UcsMacPool -Descr "MAC Address Pool for vNICs on Fabric B" -Name $macpoolb
$macb_1 = $macb | Add-UcsMacMemberBlock -From $macbfrom -To $macbto
Complete-UcsTransaction

Start-UcsTransaction
Foreach ($wwnn_pool in $slotarray) {
$wwnn_name = "ESXi_WWNN_Slot_" + $wwnn_pool
$wwnnfrom = "20:11:00:25:B5:" + $podnum + ":00:" + $wwnn_pool + "0"
$wwnnto = "20:11:00:25:B5:" + $podnum + ":00:" + $wwnn_pool + "7"
Get-UcsOrg -Level root  | Add-UcsWwnPool -Descr "ESXi Server WWNN Pool" -Name $wwnn_name -Purpose node-wwn-assignment
$wwnn_name | Add-UcsWwnMemberBlock -From $wwnnfrom -To $wwnnto
}
Complete-UcsTransaction

Start-UcsTransaction
Foreach ($wwpn_id in $slotarray) {
$wwpn_name = "ESXi_WWPN_Slot_" + $wwpn_id + "A"
$wwpnfrom = "20:11:00:25:B5:" + $podnum + ":A0:" + $wwpn_id + "0"
$wwpnto = "20:11:00:25:B5:" + $podnum + ":A0:" + $wwpn_id + "7"
Get-UcsOrg -Level root  | Add-UcsWwnPool -Descr "ESXi Server WWPN Pool A" -Name $wwpn_name -Purpose port-wwn-assignment
$wwpn_name | Add-UcsWwnMemberBlock -From $wwpnfrom -To $wwpnto
}
Complete-UcsTransaction

Start-UcsTransaction
Foreach ($wwpn_id in $slotarray) {
$wwpn_name = "ESXi_WWPN_Slot_" + $wwpn_id + "B"
$wwpnfrom = "20:11:00:25:B5:" + $podnum + ":B0:" + $wwpn_id + "0"
$wwpnto = "20:11:00:25:B5:" + $podnum + ":B0:" + $wwpn_id + "7"
Get-UcsOrg -Level root  | Add-UcsWwnPool -Descr "ESXi Server WWPN Pool B" -Name $wwpn_name -Purpose port-wwn-assignment
$wwpn_name | Add-UcsWwnMemberBlock -From $wwpnfrom -To $wwpnto
}
Complete-UcsTransaction

################################
# Configure QoS System classes #
################################
Start-UcsTransaction
# Set-UcsQosClass -QosClass platinum -Weight 9 -AdminState enabled -Force
# Set-UcsQosClass -QosClass silver -Weight 6 -AdminState enabled -Force
# Set-UcsQosClass -QosClass bronze -Weight best-effort -AdminState enabled -Force
Set-UcsBestEffortQosClass -Weight 6 -mtu 9000 -Force
Set-UcsFcQosClass -Weight 4 -Force
Complete-UcsTransaction

#######################
# Create QoS Policies #
#######################
Start-UcsTransaction
$mo = Get-UcsOrg -Level root  | Add-UcsQosPolicy -Name ESXi_FC
$mo_1 = $mo | Add-UcsVnicEgressPolicy -ModifyPresent -Burst 10240 -HostControl none -Prio fc -Rate line-rate
Complete-UcsTransaction

Start-UcsTransaction
$mo = Get-UcsOrg -Level root  | Add-UcsQosPolicy -Name ESXi_ETH
$mo_1 = $mo | Add-UcsVnicEgressPolicy -ModifyPresent -Burst 10240 -HostControl full -Prio best-effort -Rate line-rate
Complete-UcsTransaction

# Start-UcsTransaction
# $mo = Get-UcsOrg -Level root  | Add-UcsQosPolicy -Name esxi-vmotion
# $mo_1 = $mo | Add-UcsVnicEgressPolicy -ModifyPresent -Burst 10240 -HostControl none -Prio bronze -Rate line-rate
# Complete-UcsTransaction

# Start-UcsTransaction
# $mo = Get-UcsOrg -Level root  | Add-UcsQosPolicy -Name esxi-vm
# $mo_1 = $mo | Add-UcsVnicEgressPolicy -ModifyPresent -Burst 10240 -HostControl none -Prio platinum -Rate line-rate
# Complete-UcsTransaction

# Start-UcsTransaction
# $mo = Get-UcsOrg -Level root  | Add-UcsQosPolicy -Name windows-fc
# $mo_1 = $mo | Add-UcsVnicEgressPolicy -ModifyPresent -Burst 10240 -HostControl none -Prio fc -Rate line-rate
# Complete-UcsTransaction

# Start-UcsTransaction
# $mo = Get-UcsOrg -Level root  | Add-UcsQosPolicy -Name windows-ethernet
# $mo_1 = $mo | Add-UcsVnicEgressPolicy -ModifyPresent -Burst 10240 -HostControl none -Prio platinum -Rate line-rate
# Complete-UcsTransaction

###############################################
# Create Network Control Policy to Enable CDP #
###############################################
Start-UcsTransaction
$mo = Get-UcsOrg -Level root  | Add-UcsNetworkControlPolicy -Cdp enabled -MacRegisterMode only-native-vlan -Name cdp-enable -UplinkFailAction link-down
$mo_1 = $mo | Add-UcsPortSecurityConfig -ModifyPresent -Forge allow
Complete-UcsTransaction

################
# Create VLANs #
################
Start-UcsTransaction
Get-UcsLanCloud | Add-UcsVlan -DefaultNet yes -Id $mgmtvlan -Name esxi_mgmt
Get-UcsLanCloud | Add-UcsVlan -DefaultNet no -Id $vmovlan -Name vmotion

Foreach ($vlanid in $vlanarray) {
$vlanname = "vm_data_" + $vlanid
Get-UcsLanCloud | Add-UcsVlan -DefaultNet no -Id $vlanid -Name $vlanname
}
Complete-UcsTransaction

#########################
# Create vNIC Templates #
#########################
Start-UcsTransaction
$mo = Get-UcsOrg -Level root  | Add-UcsVnicTemplate -Descr "VMware ESXi vmnic0 on Fabric A" -IdentPoolName $macpoola -Mtu 9000 -Name "$nictemplate_a" -NwCtrlPolicyName cdp-enable -QosPolicyName Esxi_ETH -StatsPolicyName default -SwitchId A -TemplType updating-template
$mo_1 = $mo | Add-UcsVnicInterface -ModifyPresent -DefaultNet yes -Name esxi_mgmt
$mo_2 = $mo | Add-UcsVnicInterface -ModifyPresent -DefaultNet no -Name vmotion
Complete-UcsTransaction

Start-UcsTransaction
Foreach ($vlanid in $vlanarray) {
Add-UcsVnicInterface -VnicTemplate $nictemplate_a -ModifyPresent -DefaultNet no -Name "vm_data_$vlanid"
}
Complete-UcsTransaction

Start-UcsTransaction
$mo = Get-UcsOrg -Level root  | Add-UcsVnicTemplate -Descr "VMware ESXi vmnic1 on Fabric B" -IdentPoolName $macpoolb -Mtu 9000 -Name "$nictemplate_b" -NwCtrlPolicyName cdp-enable -QosPolicyName Esxi_ETH -StatsPolicyName default -SwitchId B -TemplType updating-template
$mo_1 = $mo | Add-UcsVnicInterface -ModifyPresent -DefaultNet yes -Name esxi_mgmt
$mo_2 = $mo | Add-UcsVnicInterface -ModifyPresent -DefaultNet no -Name vmotion
Complete-UcsTransaction

Start-UcsTransaction
Foreach ($vlanid in $vlanarray) {
Add-UcsVnicInterface  -VnicTemplate $nictemplate_b -ModifyPresent -DefaultNet no -Name "vm_data_$vlanid"
}
Complete-UcsTransaction

##########################
# Create vHBA Templates  #
##########################
Start-UcsTransaction
Foreach ($vhbaid in $slotarray) {
$vhbaname = "vHBA_Slot_" + $vhbaid + "A"
$wwpn_pool = "ESXi_WWPN_Slot_" + $vhbaid + "A"
$mo = Get-UcsOrg -Level root  | Add-UcsVhbaTemplate -Descr "VMware ESXi vHBA 1 for Fabric A" -IdentPoolName $wwpn_pool -MaxDataFieldSize 2048 -Name $vhbaname -QosPolicyName ESXi_FC -StatsPolicyName default -SwitchId A -TemplType updating-template
$mo_1 = $mo | Add-UcsVhbaInterface -ModifyPresent -Name $fcvsannamea
}
Complete-UcsTransaction

Start-UcsTransaction
Foreach ($vhbaid in $slotarray) {
$vhbaname = "vHBA_Slot_" + $vhbaid + "B"
$wwpn_pool = "ESXi_WWPN_Slot_" + $vhbaid + "B"
$mo = Get-UcsOrg -Level root  | Add-UcsVhbaTemplate -Descr "VMware ESXi vHBA 1 for Fabric B" -IdentPoolName $wwpn_pool -MaxDataFieldSize 2048 -Name $vhbaname -QosPolicyName ESXi_FC -StatsPolicyName default -SwitchId B -TemplType updating-template
$mo_1 = $mo | Add-UcsVhbaInterface -ModifyPresent -Name $fcvsannameb
}
Complete-UcsTransaction

############################
# Create BIOS Policies     #
############################

Start-UcsTransaction
$mo = Get-UcsOrg -Level root  | Add-UcsBiosPolicy -Descr "" -Name "ESXi_Bios" -RebootOnUpdate "no"
$mo_5 = $mo | Set-UcsBiosVfCPUPerformance -VpCPUPerformance "enterprise" -Force
$mo_8 = $mo | Set-UcsBiosVfDirectCacheAccess -VpDirectCacheAccess "enabled" -Force
$mo_9 = $mo | Set-UcsBiosEnhancedIntelSpeedStep -VpEnhancedIntelSpeedStepTech "enabled" -Force
$mo_12 = $mo | Set-UcsBiosHyperThreading -VpIntelHyperThreadingTech "enabled" -Force
$mo_13 = $mo | Set-UcsBiosTurboBoost -VpIntelTurboBoostTech "enabled" -Force
$mo_14 = $mo | Set-UcsBiosIntelDirectedIO -VpIntelVTDATSSupport "platform-default" -VpIntelVTDCoherencySupport "platform-default" -VpIntelVTDInterruptRemapping "platform-default" -VpIntelVTDPassThroughDMASupport "platform-default" -VpIntelVTForDirectedIO "enabled" -Force
$mo_15 = $mo | Set-UcsBiosVfIntelVirtualizationTechnology -VpIntelVirtualizationTechnology "enabled" -Force
$mo_17 = $mo | Set-UcsBiosLvDdrMode -VpLvDDRMode "performance-mode" -Force
$mo_22 = $mo | Set-UcsBiosNUMA -VpNUMAOptimized "enabled" -Force
$mo_29 = $mo | Set-UcsBiosVfProcessorCState -VpProcessorCState "enabled" -Force
$mo_30 = $mo | Set-UcsBiosVfProcessorC1E -VpProcessorC1E "enabled" -Force
$mo_31 = $mo | Set-UcsBiosVfProcessorC3Report -VpProcessorC3Report "acpi-c3" -Force
$mo_32 = $mo | Set-UcsBiosVfProcessorC6Report -VpProcessorC6Report "enabled" -Force
$mo_33 = $mo | Set-UcsBiosVfProcessorC7Report -VpProcessorC7Report "enabled" -Force
$mo_34 = $mo | Set-UcsBiosVfQuietBoot -VpQuietBoot "disabled" -Force
$mo_35 = $mo | Set-UcsBiosVfResumeOnACPowerLoss -VpResumeOnACPowerLoss "last-state" -Force
$mo_36 = $mo | Set-UcsBiosVfSelectMemoryRASConfiguration -VpSelectMemoryRASConfiguration "maximum-performance" -Force
$mo_37 = $mo | Set-UcsBiosVfSerialPortAEnable -VpSerialPortAEnable "disabled" -Force
Complete-UcsTransaction

###############################
# Create SAN Boot Policies    #
###############################
Foreach ($BootPolicy in $BootPolicyArray) {
	Start-UcsTransaction
	$mo = Get-UcsOrg -Level root  | Add-UcsBootPolicy -Descr "Boot from SAN policy for ESXi hosts" -EnforceVnicName yes -Name $BootPolicy -RebootOnUpdate no
	$mo_1 = $mo | Add-UcsLsbootVirtualMedia -Access "read-only" -Order "1"
	$mo_2 = $mo | Add-UcsLsbootStorage -ModifyPresent -Order "2"
	$mo_2_1 = $mo_2 | Add-UcsLsbootSanImage -Type primary -VnicName $HbaNameA
	$mo_2_1_1 = $mo_2_1 | Add-UcsLsbootSanImagePath -Lun $bootlunid -Type primary -Wwn $BootTarget1
	$mo_2_2 = $mo_2 | Add-UcsLsbootSanImage -Type secondary -VnicName $HbaNameB
	$mo_2_2_1 = $mo_2_2 | Add-UcsLsbootSanImagePath -Lun $bootlunid -Type primary -Wwn $BootTarget2
	Complete-UcsTransaction
}

############################
# Create Local Disk Policy # - Need to review, not working
############################
Start-UcsTransaction
$mo = Get-UcsOrg -Level root  | Set-UcsOrg -Descr ""
$mo_1 = Get-UcsOrg -Level root | Get-UcsLocalDiskConfigPolicy -Name "default" -LimitScope | Remove-UcsLocalDiskConfigPolicy -Force
Complete-UcsTransaction

Get-UcsOrg -Level root | Add-UcsLocalDiskConfigPolicy -Descr "" -Mode "raid-mirrored" -Name "Mirrored" -ProtectConfig "yes"
Get-UcsOrg -Level root | Add-UcsLocalDiskConfigPolicy -Descr "" -Mode "no-local-storage" -Name "No_Local_Disk" -ProtectConfig "yes"

#############################
# Create maintenance policy # - Need to review, not working
#############################
# Get-UcsOrg -Level root  | Add-UcsMaintenancePolicy -Descr "User acknowledge is required to reboot a server after a disruptive change" -Name user-acknowledge -UptimeDisr user-ack
Get-UcsOrg -Level root | Get-UcsMaintenancePolicy -Name "default" -LimitScope | Set-UcsMaintenancePolicy -Descr "" -SchedName "" -UptimeDisr "user-ack" -Force
Get-UcsOrg -Level root  | Add-UcsMaintenancePolicy -Descr "" -Name "User_Ack" -SchedName "" -UptimeDisr "user-ack"

#################################
# Create disk/BIOS Scrub Policy #
#################################
Get-UcsOrg -Level root  | Add-UcsScrubPolicy -BiosSettingsScrub "no" -Descr ""-DiskScrub "no" -Name "No_Scrub"

################################
# Create a no-power cap policy #
################################
Get-UcsOrg -Level root  | Add-UcsPowerPolicy -Name no-power-cap -Prio no-cap

#####################################
# Create vNIC/vHBA Placement Policy #
#####################################
Start-UcsTransaction
$mo = Get-UcsOrg -Level root  | Add-UcsPlacementPolicy -Descr "For Half-width blades" -Name b200-b230
$mo_1 = $mo | Add-UcsFabricVCon -ModifyPresent -Fabric NONE -Id 1 -Placement physical -Select all -Share shared -Transport ethernet,fc
Complete-UcsTransaction

########################################
# Create Service Profile Templates     #
########################################

Foreach ($TemplateName in $BootPolicyArray) {
Start-UcsTransaction
$mo = Get-UcsOrg -Level root  | Add-UcsServiceProfile -BiosProfileName esxi -BootPolicyName esxi-san-boot -Descr "Service Profile Template for VMware ESXi hosts" -ExtIPState pooled -HostFwPolicyName $frmwarever -IdentPoolName server-uuid -LocalDiskPolicyName any-config -MaintPolicyName user-acknowledge -MgmtFwPolicyName $frmwarever -Name CC-ESXi -PowerPolicyName default -ScrubPolicyName no-scrub -StatsPolicyName default -Type updating-template -VconProfileName b200-b230
$mo_1 = $mo | Add-UcsVnic -AdaptorProfileName VMWare -Addr derived -AdminVcon 1 -Mtu 1500 -Name vmnic0-mgt-a -NwTemplName esx-vmnic0-mgt-a -Order 1 -StatsPolicyName default -SwitchId A
$mo_2 = $mo | Add-UcsVnic -AdaptorProfileName VMWare -Addr derived -AdminVcon 1 -Mtu 1500 -Name vmnic1-mgt-b -NwTemplName esx-vmnic1-mgt-b -Order 2 -StatsPolicyName default -SwitchId A
$mo_3 = $mo | Add-UcsVnic -AdaptorProfileName VMWare -Addr derived -AdminVcon 1 -Mtu 1500 -Name vmnic2-vmo-a -NwTemplName esx-vmnic2-vmo-a -Order 3 -StatsPolicyName default -SwitchId A
$mo_4 = $mo | Add-UcsVnic -AdaptorProfileName VMWare -Addr derived -AdminVcon 1 -Mtu 1500 -Name vmnic3-vmo-b -NwTemplName esx-vmnic3-vmo-b -Order 4 -StatsPolicyName default -SwitchId A
$mo_5 = $mo | Add-UcsVnic -AdaptorProfileName VMWare -Addr derived -AdminVcon 1 -Mtu 1500 -Name vmnic4-vm-a -NwTemplName esx-vmnic4-vm-a -Order 5 -StatsPolicyName default -SwitchId A
$mo_6 = $mo | Add-UcsVnic -AdaptorProfileName VMWare -Addr derived -AdminVcon 1 -Mtu 1500 -Name vmnic5-vm-b -NwTemplName esx-vmnic5-vm-b -Order 6 -StatsPolicyName default -SwitchId A
$mo_7 = $mo | Add-UcsVnicFcNode -ModifyPresent -Addr pool-derived -IdentPoolName server-wwnn
$mo_8 = $mo | Add-UcsVhba -AdaptorProfileName VMWare -Addr derived -AdminVcon 1 -MaxDataFieldSize 2048 -Name vmhba1-a -NwTemplName esx-vmhba1-a -Order 7 -PersBind disabled -PersBindClear no -StatsPolicyName default -SwitchId A
$mo_9 = $mo | Add-UcsVhba -AdaptorProfileName VMWare -Addr derived -AdminVcon 1 -MaxDataFieldSize 2048 -Name vmhba2-b -NwTemplName esx-vmhba2-b -Order 8 -PersBind disabled -PersBindClear no -StatsPolicyName default -SwitchId A
#$mo_10 = $mo | Set-UcsServerPower -State admin-up -Force
Complete-UcsTransaction
}

#########################################
# Deploy Service Profiles from Template #
#########################################
# Get-UcsServiceProfile -Name esxi-sp-template -Org org-root | Add-UcsServiceProfileFromTemplate -Prefix $esxprefix -Count 3 -DestinationOrg org-root
# Get-UcsServiceProfile -Name windows-sp-template -Org org-root | Add-UcsServiceProfileFromTemplate -Prefix $windowsprefix -Count 3 -DestinationOrg org-root


######################
# Configure SNMP     #
######################

#Add-UcsSnmpTrap -Community "IntelOps" -Hostname "10.114.32.104" -NotificationType "traps" -Port 162 -V3Privilege "noauth" -Version "v2c"
#Add-UcsSnmpTrap -Community "IntelOps" -Hostname "10.177.9.82" -NotificationType "traps" -Port 162 -V3Privilege "noauth" -Version "v2c"
#Add-UcsSnmpTrap -Community "IntelOps" -Hostname "10.179.72.72" -NotificationType "traps" -Port 162 -V3Privilege "noauth" -Version "v2c"

#Start-UcsTransaction
#$mo = Get-UcsSvcEp -Descr ""
#$mo_1 = Get-UcsSnmp | Set-UcsSnmp -AdminState "enabled" -Community "public" -Descr "SNMP Service" -SysContact "Datalinklabs Ops" -SysLocation "$srlocation" -Force
#Complete-UcsTransaction

######################
# Configure Callhome #
######################
Start-UcsTransaction
$mo = Get-UcsCallhome | Set-UcsCallhome -AdminState on -AlertThrottlingAdminState on -Force
$mo_1 = Get-UcsCallhomeSmtp | Set-UcsCallhomeSmtp -Host $smtpserver -Port 25 -Force
$mo_2 = Get-UcsCallhomeSource | Set-UcsCallhomeSource -Addr $phyaddr -Contact $contactname -Email $contactemail -From $smtpfrom -Phone $contactphone -ReplyTo $smtpfrom -Urgency debug -Force
Complete-UcsTransaction
Get-UcsCallhomeProfile -Name full_txt | Add-UcsCallhomeRecipient -Email $smtprcpt
Get-UcsCallhomeProfile -Name "CiscoTAC-1" | Add-UcsCallhomeRecipient -Email "callhome@cisco.com"
