#This script builds a config for a Nexus 9K. T
#This still needs to account for the VPC pair.
#1. Need to check outputs
#2. Need to verify descriptions on ports
#3. Variables to import from presitedoc
#4. Would like to build functions for repeated tasks
#5. VPC configuration - add IP Arp Sync, auto-recovery
#6. Add QoS policy, CoS 4 for NFS, add jumbo frame support
#
$hostname = "n5k1"
$uname = "admin"
$pw = "datalink"
$domain_name = "cloudcomplete.com"
$name_server = "1.1.1.1"
$snmp_server = "1.1.1.1"
$snmp_community = "datalinkro"
$vlan = ("100","101","102","103")
$UCS_vlan = ("101","102","103")
$NTAP_vlan = ("101","102")
$sw1_mgmt_IP = "192.168.1.90"
$sw2_mgmt_IP = "192.168.1.91"
$mgmt_vlan = "100"
$timezone = "-6"
$NTAP_hostname = "CCDC1NTAP"
$UCS_hostname = "CCDC1UCS"
$default_gw = "192.168.1.1"
$uplinksw = ("e1/9", "e1/10")
$uplink_speed = "one"
#
#config items that are repeated
#
$Jumbo_Frame_Policy = ("policy-map type network-qos jumbo", "class type network-qos class-default", "mtu 9216", "system qos", "service-policy type network-qos jumbo")
$STP_HOST_Trunk = ("spanning-tree port type edge trunk", "spanning-tree bpduguard enable", "spanning-tree bpdufilter enable")
$STP_HOST_Access = ("spanning-tree port type edge", "spanning-tree bpduguard enable", "spanning-tree bpdufilter enable")
$features_installed = ("feature vpc", "feature lacp", "feature udld", "feature nxapi")
#
#Define Config Sections as array list
#
$feature_install = New-Object System.Collections.ArrayList
$domain_lookup = New-Object System.Collections.ArrayList
$qos_policy = New-Object System.Collections.ArrayList
$snmp_install = New-Object System.Collections.ArrayList
$vlan_install = New-Object System.Collections.ArrayList
$mgmt_install = New-Object System.Collections.ArrayList
$vpc_domain_install = New-Object System.Collections.ArrayList
$vpc_peer_link_create = New-Object System.Collections.ArrayList
$NTAP_VPC_create = New-Object System.Collections.ArrayList
$UCS_VPC_create = New-Object System.Collections.ArrayList
$uplink_VPC_create = New-Object System.Collections.ArrayList
$time_create = New-Object System.Collections.ArrayList
$vrf_create = New-Object System.Collections.ArrayList
#
$feature_install.add($features_installed)
$feature_install.add("!")
$credentials = "username $uname password $pw role network-admin"
[Void]$ssh_install = "ssh key rsa 2048"
[Void]$domain_lookup.add("!")
[Void]$domain_lookup.add("ip domain-name $domain_name")
if ($name_server -eq "not_used")
	{
	[Void]$domain_lookup.add("no ip domain-lookup")
	}
else
	{
	foreach($i in $name_server)
		{
		[Void]$domain_lookup.add("ip name-server $name_server use-vrf management")
		}
	}
#need to get vlan names in
$vlan_install.add("!")
foreach($i in $vlan)
	{
	[Void]$vlan_install.add("vlan $i")
	}
[Void]$mgmt_install.add("!")
[Void]$mgmt_install.add("vrf context management")
[Void]$mgmt_install.add("ip route 0.0.0.0/0 $default_gw")
[Void]$mgmt_install.add("!")
[Void]$mgmt_install.add("interface m0")
[Void]$mgmt_install.add("ip address $sw1_mgmt_IP /24")
#
[Void]$qos_policy.add("!")
[Void]$qos_policy.add($Jumbo_Frame_Policy)
#
#snmp configuration
#
[Void]$snmp_install.add("!")
[Void]$snmp_install.add("snmp-server host $snmp_server use-vrf management")
[Void]$snmp_install.add("snmp-server community $snmp_community ro")
#
#create VPC peer link
#
[Void]$vpc_domain_install.add("!")
[Void]$vpc_domain_install.add("vpc domain 900")
[Void]$vpc_domain_install.add("peer-keepalive destination $sw2_mgmt_IP")
[Void]$vpc_peer_link_create.add("!")
[Void]$vpc_peer_link_create.add("int e1/47-48")
[Void]$vpc_peer_link_create.add("description VPC 900 VPC peer-link")
[Void]$vpc_peer_link_create.add("channel-group 900 mode active")
[Void]$vpc_peer_link_create.add("int po900")
[Void]$vpc_peer_link_create.add("description VPC 900 20Gb VPC peer-link")
[Void]$vpc_peer_link_create.add("switchport mode trunk")
[Void]$vpc_peer_link_create.add("vpc peer-link")
#
#create UCS VPC
#
[Void]$UCS_VPC_create.add("!")
[Void]$UCS_VPC_create.add("int e1/1")
[Void]$UCS_VPC_create.add("description $UCS_hostname-A e1/15 10Gb Member VPC 30")
[Void]$UCS_VPC_create.add("channel-group 30 mode active")
[Void]$UCS_VPC_create.add("!")
[Void]$UCS_VPC_create.add("int e1/2")
[Void]$UCS_VPC_create.add("description $UCS_hostname-B e1/15 10Gb Member VPC 40")
[Void]$UCS_VPC_create.add("channel-group 40 mode active")
[Void]$UCS_VPC_create.add("!")
[Void]$UCS_VPC_create.add("int po30")
[Void]$UCS_VPC_create.add("description $UCS_hostname-A 20Gb VPC 30")
[Void]$UCS_VPC_create.add("switchport mode trunk")
[Void]$UCS_VPC_create.add("switchport trunk allowed vlan none")
foreach ($i in $UCS_vlan)
	{
	[Void]$UCS_VPC_create.add("switchport trunk allowed vlan add $i")
	}
[Void]$UCS_VPC_create.add($STP_Host_Trunk)
[Void]$UCS_VPC_create.add("vpc 30")
[Void]$UCS_VPC_create.add("!")
[Void]$UCS_VPC_create.add("int po40")
[Void]$UCS_VPC_create.add("description $UCS_hostname-B 20Gb VPC 30")
[Void]$UCS_VPC_create.add("switchport mode trunk")
[Void]$UCS_VPC_create.add("switchport trunk allowed vlan none")
foreach ($i in $UCS_vlan)
	{
	[Void]$UCS_VPC_create.add("switchport trunk allowed vlan add $i")
	}
[Void]$UCS_VPC_create.add($STP_Host_Trunk)
[Void]$UCS_VPC_create.add("vpc 40")
#
#create VPC for NetApp
#

[Void]$NTAP_VPC_create.add("!")
[Void]$NTAP_VPC_create.add("int e1/5")
[Void]$NTAP_VPC_create.add("description $NTAP_hostname e0a 10Gb Member VPC 100")
[Void]$NTAP_VPC_create.add("channel-group 100 mode active")
[Void]$NTAP_VPC_create.add("!")
[Void]$NTAP_VPC_create.add("int e1/6")
[Void]$NTAP_VPC_create.add("description $NTAP_hostname e0a 10Gb Member VPC 110")
[Void]$NTAP_VPC_create.add("channel-group 110 mode active")
[Void]$NTAP_VPC_create.add("!")
[Void]$NTAP_VPC_create.add("int po100")
[Void]$NTAP_VPC_create.add("description $NTAP_hostname 20Gb VPC 100")
[Void]$NTAP_VPC_create.add("switchport mode trunk")
[Void]$NTAP_VPC_create.add("switchport trunk allowed vlan none")
foreach ($i in $NTAP_vlan)
	{
	[Void]$NTAP_VPC_create.add("switchport trunk allowed vlan add $i")
	}
[Void]$NTAP_VPC_create.add($STP_Host_Trunk)
[Void]$NTAP_VPC_create.add("vpc 100")
[Void]$NTAP_VPC_create.add("!")
[Void]$NTAP_VPC_create.add("int po110")
[Void]$NTAP_VPC_create.add("description $NTAP_hostname 20Gb VPC 110")
[Void]$NTAP_VPC_create.add("switchport mode trunk")
[Void]$NTAP_VPC_create.add("switchport trunk allowed vlan none")
foreach ($i in $NTAP_vlan)
	{
	[Void]$NTAP_VPC_create.add("switchport trunk allowed vlan add $i")
	}
[Void]$NTAP_VPC_create.add($STP_Host_Trunk)
[Void]$NTAP_VPC_create.add("VPC 110")
[Void]$NTAP_VPC_create.add("!")
[Void]$NTAP_VPC_create.add("int e1/9")
[Void]$NTAP_VPC_create.add("description $NTAP_hostname SP")
[Void]$NTAP_VPC_create.add("switchport mode access")
[Void]$NTAP_VPC_create.add("switchport mode access $mgmt_vlan")
[Void]$NTAP_VPC_create.add($STP_Host_Access)
#
foreach ($i in $uplinksw)
	{
	[Void]$uplink_VPC_create.add("!")
	[Void]$uplink_VPC_create.add("int $i")
	[Void]$uplink_VPC_create.add("description Uplink Member VPC 10")
	[Void]$uplink_VPC_create.add("channel-group 10 mode active")
	[Void]$uplink_VPC_create.add("!")
	}
[Void]$uplink_VPC_create.add("int po10")
[Void]$uplink_VPC_create.add("switchport mode trunk")
[Void]$uplink_VPC_create.add("switchport trunk allowed vlan none")
foreach ($i in $vlan)
	{
	[Void]$uplink_VPC_create.add("switchport trunk allowed vlan add $i")
	}
$uplink_VPC_create.add("spanning-tree port type normal")
if ($ten_gig = "one")
	{
	[Void]$uplink_VPC_create.add("speed 1000")
	}
else
	{
	}
$uplink_VPC_create.add("vpc 10")


$output_cmds	=	$feature_install + `
					$credentials + `
					$ssh_install + `
					$domain_lookup + `
					$vlan_install + `
					$mgmt_install + `
					$qos_policy + `
					$snmp_install + `
					$vpc_domain_install + `
					$vpc_peer_link_create + `
					$UCS_VPC_create + `
					$NTAP_VPC_create + `
					$uplink_VPC_create
					

					
					

					
					
					
$output_cmds | out-file test1 -Encoding	ASCII
				


