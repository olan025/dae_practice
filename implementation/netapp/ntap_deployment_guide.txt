login as: admin
SP e2c1ntap-01> system console
Type Ctrl-D to exit.





Welcome to the cluster setup wizard.

You can enter the following commands at any time:
  "help" or "?" - if you want to have a question clarified,
  "back" - if you want to change previously answered questions, and
  "exit" or "quit" - if you want to quit the cluster setup wizard.
     Any changes you made before quitting will be saved.

You can return to cluster setup at any time by typing "cluster setup".
To accept a default or omit a question, do not enter a value.


Do you want to create a new cluster or join an existing cluster? {create, join}:
create


Do you intend for this node to be used as a single node cluster? {yes, no} [no]:
no
Will the cluster network be configured to use network switches? [yes]:
no
Non-HA mode, Reboot node to activate HA

Do you want to reboot now to set storage failover (SFO) to HA mode? {yes, no}
[yes]: yes

Rebooting now




Tue Nov 11 21:04:33 UTC 2014
SP-login: login:
...

Welcome to the cluster setup wizard.

You can enter the following commands at any time:
  "help" or "?" - if you want to have a question clarified,
  "back" - if you want to change previously answered questions, and
  "exit" or "quit" - if you want to quit the cluster setup wizard.
     Any changes you made before quitting will be saved.

You can return to cluster setup at any time by typing "cluster setup".
To accept a default or omit a question, do not enter a value.


Do you want to create a new cluster or join an existing cluster? {create, join}:
create


Do you intend for this node to be used as a single node cluster? {yes, no} [no]:

Will the cluster network be configured to use network switches? [no]:


Existing cluster interface configuration found:

Port    MTU     IP              Netmask
e1a     9000    169.254.40.103  255.255.0.0
e2a     9000    169.254.107.175 255.255.0.0

Do you want to use this configuration? {yes, no} [yes]:



Step 1 of 5: Create a Cluster
You can type "back", "exit", or "help" at any question.

Enter the cluster name: e2ntap1c
Enter the cluster base license key: UJBGVLVQJHOJKBAAAAAAAAAAAAAA

Creating cluster e2ntap1c

System start up .........

Cluster e2ntap1c has been created.


Step 2 of 5: Add Feature License Keys
You can type "back", "exit", or "help" at any question.

Enter an additional license key []:


Step 3 of 5: Set Up a Vserver for Cluster Administration
You can type "back", "exit", or "help" at any question.

Enter the cluster administrator's (username "admin") password:

Retype the password:

Enter the cluster management interface port [e0a]: e0M
Enter the cluster management interface IP address: 10.202.13.21
Enter the cluster management interface netmask: 255.255.255.0
Enter the cluster management interface default gateway: 10.202.13.1

A cluster management interface on port e0M with IP address 10.202.13.21 has been created.  You can use this address to connect to and manage the cluster.

Enter the DNS domain names: datalinklabs.local
Enter the name server IP addresses: 10.207.1.10,10.200.1.10
DNS lookup for the admin Vserver will use the datalinklabs.local domain.

Step 4 of 5: Configure Storage Failover (SFO)
You can type "back", "exit", or "help" at any question.


SFO will be enabled when the partner joins the cluster.


Step 5 of 5: Set Up the Node
You can type "back", "exit", or "help" at any question.

Where is the controller located []: Datalinklabs, 10050 Crosstown Cir, Ste. 500, Eden Prairie, MN 55344
Enter the node management interface port [e0M]:
Enter the node management interface IP address: 10.202.11.23
Enter the node management interface netmask: 255.255.255.0
Enter the node management interface default gateway:
login as: admin
admin@10.202.11.23's password:
SP e2c1ntap-01> system console
Type Ctrl-D to exit.
10.202.11.1
A node management interface on port e0M with IP address 10.202.11.23 has been created.

Enable IPv4 DHCP on the service processor interface [no]:
Enter the service processor interface IP address [10.202.11.23]:
10.202.11.21
Enter the service processor interface netmask [255.255.255.0]:

Enter the service processor interface default gateway [10.202.11.1]:


