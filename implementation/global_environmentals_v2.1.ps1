# Define paths for all supported build modules

$esxi_script_version = "2.1"
$ucsm_script_version = "2.1"
$nxos_script_version = "2.1"
$cdot_script_version = "2.2"

$setup_dirs_str			= ""
$global_base_path = (Get-Location).Path
$global_conf_path 		= Join-Path -Path $global_base_path -ChildPath "conf"
$global_reports_path 	= Join-Path -Path $global_base_path -ChildPath "reports"
$setup_dirs_str			= $global_conf_path + ";" + $global_reports_path

$esxi_base_path		= Join-Path -Path $global_base_path -ChildPath "esxi"
$esxi_conf_path 		= Join-Path -Path $esxi_base_path -ChildPath "conf"
$esxi_cmds_path 		= Join-Path -Path $esxi_base_path -ChildPath "cmds"
$esxi_reports_path 	= Join-Path -Path $esxi_base_path -ChildPath "reports"
$esxi_images_path 	= Join-Path -Path $esxi_base_path -ChildPath "images"
$esxi_templates_path 	= Join-Path -Path $esxi_base_path -ChildPath "templates"
$esxi_module_name		= "esxi_builder_v" + $esxi_script_version + ".ps1"
$esxi_module_script		= Join-Path -Path $esxi_base_path -ChildPath  $esxi_module_name 	
$esxi_datasource_name	= "esxi_presite_v" + $esxi_script_version + ".xls"
$esxi_datasource_file	= Join-Path -Path $esxi_base_path -ChildPath $esxi_datasource_name
$setup_dirs_str			= $setup_dirs_str + ";" + $esxi_base_path + ";" + $esxi_conf_path + ";" + $esxi_cmds_path + ";" + $esxi_reports_path + ";" + $esxi_images_path + ";" + $esxi_templates_path

$ucsm_base_path			= Join-Path -Path $global_base_path -ChildPath "ucsm"
$ucsm_conf_path 		= Join-Path -Path $ucsm_base_path -ChildPath "conf"
$ucsm_cmds_path 		= Join-Path -Path $ucsm_base_path -ChildPath "cmds"
$ucsm_reports_path 		= Join-Path -Path $ucsm_base_path -ChildPath "reports"
$ucsm_images_path 		= Join-Path -Path $ucsm_base_path -ChildPath "images"
$ucsm_templates_path 	= Join-Path -Path $ucsm_base_path -ChildPath "templates"
$ucsm_module_name		= "ucsm_builder_v" + $ucsm_script_version + ".ps1"
$ucsm_module_script		= Join-Path -Path $ucsm_base_path -ChildPath  $ucsm_module_name 	
$ucsm_datasource_name	= "ucsm_presite_v" + $ucsm_script_version + ".xls"
$ucsm_datasource_file	= Join-Path -Path $ucsm_base_path -ChildPath $ucsm_datasource_name
$setup_dirs_str			= $setup_dirs_str + ";" + $ucsm_base_path + ";" + $ucsm_conf_path + ";" + $ucsm_cmds_path + ";" + $ucsm_reports_path + ";" + $ucsm_images_path + ";" + $ucsm_templates_path

$nxos_base_path			= Join-Path -Path $global_base_path -ChildPath "nxos"
$nxos_conf_path 		= Join-Path -Path $nxos_base_path -ChildPath "conf"
$nxos_cmds_path 		= Join-Path -Path $nxos_base_path -ChildPath "cmds"
$nxos_reports_path 		= Join-Path -Path $nxos_base_path -ChildPath "reports"
$nxos_images_path 		= Join-Path -Path $nxos_base_path -ChildPath "images"
$nxos_templates_path 	= Join-Path -Path $nxos_base_path -ChildPath "templates"
$nxos_module_name		= "nxos_builder_v" + $nxos_script_version + ".ps1"
$nxos_module_script		= Join-Path -Path $nxos_base_path -ChildPath  $nxos_module_name	
$nxos_datasource_name	= "nxos_presite_v" + $nxos_script_version + ".xls"
$nxos_datasource_file	= Join-Path -Path $nxos_base_path -ChildPath $nxos_datasource_name
$setup_dirs_str			= $setup_dirs_str + ";" + $nxos_base_path + ";" + $nxos_conf_path + ";" + $nxos_cmds_path + ";" + $nxos_reports_path + ";" + $nxos_images_path + ";" + $nxos_templates_path

$cdot_base_path			= Join-Path -Path $global_base_path -ChildPath "cdot"
$cdot_conf_path 		= Join-Path -Path $cdot_base_path -ChildPath "conf"
$cdot_cmds_path 		= Join-Path -Path $cdot_base_path -ChildPath "cmds"
$cdot_reports_path 		= Join-Path -Path $cdot_base_path -ChildPath "reports"
$cdot_images_path 		= Join-Path -Path $cdot_base_path -ChildPath "images"
$cdot_templates_path 	= Join-Path -Path $cdot_base_path -ChildPath "templates"
$cdot_module_name		= "cdot_builder_v" + $cdot_script_version + ".ps1"
$cdot_module_script		= Join-Path -Path $cdot_base_path -ChildPath  $cdot_module_name	
$cdot_datasource_name	= "cdot_presite_v" + $cdot_script_version + ".xls"
$cdot_datasource_file	= Join-Path -Path $cdot_base_path -ChildPath  $cdot_datasource_name	
$setup_dirs_str			= $setup_dirs_str + ";" + $cdot_base_path + ";" + $cdot_conf_path + ";" + $cdot_cmds_path + ";" + $cdot_reports_path + ";" + $cdot_images_path + ";" + $cdot_templates_path

$wdColorGray15 			= 14277081
$mysql_dll				= "C:\Program Files (x86)\MySQL\Connector.NET 6.9\Assemblies\v2.0\MySQL.Data.dll"

$date_time 	= Get-Date -Format "MM/dd/yyyy@HH:mm"
$section_break 			= "#" * 80
$script_user 			= [Environment]::UserName
$script_user_domain 	= [Environment]::UserDomainName
$script_machine_name	= [Environment]::MachineName
$script_run_str			= "Run by: " + $script_user_domain + "`\" + $script_user + " on system " + $script_machine_name + " at " + $date_time
$all_svm_protocols 		= @("nfs","cifs","fcp","iscsi")