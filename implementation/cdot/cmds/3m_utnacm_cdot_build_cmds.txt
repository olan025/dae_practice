# ------------------------------------------------------------------------------
# + Welcome to cDOT setup
# + Summary of installation parameters defined in: C:\Users\jakerson\Dropbox\CloudCompleteAutomation\implementation\netapp\cDOTClusterPresite_v2.0.xls
# + Customer Name: 3M
# + Cluster Name: utnacm
# + Cluster Base License: OUFQVUKAHJMIWBAAAAAAAAAAAAAA
# + Administrator Password (user: admin): datalink1
# + Domain Name: usac.mmm.com
# + DNS Servers: 143.122.30.470,143.122.30.68
# + Location: 3M Health Information Systems, 575 West Murray Blvd, Murray, UT 84123
# + Node Management Port (temporary for install): e0M
# + IP address summary for cluster: utnacm
# + Cluster Name: utnacm Cluster Mgmt IP: 143.122.48.10 |  | 
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# + Node Mgmt IP Summary for cluster node utna01 :	143.122.48.11 | 255.255.248.0 | 143.122.48.1
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# + Node Mgmt IP Summary for cluster node utna02 :	143.122.48.12 | 255.255.248.0 | 143.122.48.1
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# + SP Mgmt IP Summary for cluster node utna01 :	143.122.30.129 | 255.255.255.0 | 143.122.30.62
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# + SP Mgmt IP Summary for cluster node utna02 :	143.122.30.135 | 255.255.255.0 | 143.122.30.62
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# + Connect to first node:  ()
# + Node management interface via SSH to issue remaining
# + commands as console can have issues with line ends
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# + Connect to first node:  ()
# + Node management interface via SSH to issue remaining
# + commands as console can have issues with line ends
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# + Set system to factory defaults
# ------------------------------------------------------------------------------

set-defaults
setenv bootarg.init.boot_clustered true
saveenv
autoboot
ctrl-c (boot menu)
5 (maintenance mode) to assign disks
reboot
ctrl-c (boot menu)
4 (zero)

# ------------------------------------------------------------------------------
# + Set terminal to eliminate pause
# ------------------------------------------------------------------------------

rows 0
# ------------------------------------------------------------------------------
# + Verify Storage Failover/Cluster HA are enabled in a 2-node cluster
# ------------------------------------------------------------------------------

set -privilege diagnostic
storage failover show
cluster ha show
## If the 'Takeover Possible' is 'false' issue the following, otherwise issue 'set -privilege admin' only
cluster ha modify -configured true
cluster ha show
storage failover show
set -privilege admin

# ------------------------------------------------------------------------------
# + Rename nodes
# ------------------------------------------------------------------------------

system node show
system node rename utnacm-01 -newname utna01
system node rename utnacm-02 -newname utna02
system node show

# ------------------------------------------------------------------------------
# + Rename mroot aggregates
# ------------------------------------------------------------------------------

aggregate show
storage aggregate rename -aggregate aggr0 -newname utna01_mroot
storage aggregate rename -aggregate aggr0_utnacm_02_0 -newname utna02_mroot
aggregate show

# ------------------------------------------------------------------------------
# + Create 10GbE interface groups on each node
# ------------------------------------------------------------------------------

network port ifgrp show
network port ifgrp create -node utna01 -ifgrp a0a -mode multimode_lacp -distr-func ip
network port ifgrp add-port -node utna01 -ifgrp a0a -port e0e
network port ifgrp add-port -node utna01 -ifgrp a0a -port e0f
network port ifgrp create -node utna02 -ifgrp a0a -mode multimode_lacp -distr-func ip
network port ifgrp add-port -node utna02 -ifgrp a0a -port e0e
network port ifgrp add-port -node utna02 -ifgrp a0a -port e0f
network port ifgrp show

# ------------------------------------------------------------------------------
# + Create 1GbE interface groups on each node
# ------------------------------------------------------------------------------

network port ifgrp show
network port ifgrp create -node utna01 -ifgrp a0b -mode multimode_lacp -distr-func ip
network port ifgrp add-port -node utna01 -ifgrp a0b -port e0i
network port ifgrp add-port -node utna01 -ifgrp a0b -port e0k
network port ifgrp create -node utna02 -ifgrp a0b -mode multimode_lacp -distr-func ip
network port ifgrp add-port -node utna02 -ifgrp a0b -port e0i
network port ifgrp add-port -node utna02 -ifgrp a0b -port e0k
network port show

# ------------------------------------------------------------------------------
# + Create node management failover groups
# ------------------------------------------------------------------------------

network interface failover-groups show
network interface failover show
network interface failover-groups create -failover-group utna01_mgmt_node_fg -node utna01 -port a0b
network interface failover-groups create -failover-group utna01_mgmt_node_fg -node utna01 -port e0M
network interface failover-groups create -failover-group utna02_mgmt_node_fg -node utna02 -port a0b
network interface failover-groups create -failover-group utna02_mgmt_node_fg -node utna02 -port e0M
network interface failover-groups show
network interface failover show

# ------------------------------------------------------------------------------
# + Create cluster/SVM management failover groups
# ------------------------------------------------------------------------------

network interface failover-groups show
network interface failover show
network interface failover-groups create -failover-group utnacm_mgmt_cluster_fg -node utna01 -port a0b
network interface failover-groups show
network interface failover show

# ------------------------------------------------------------------------------
# + Create data failover groups
# ------------------------------------------------------------------------------

network interface failover-groups show
network interface failover show
network interface failover-groups create -failover-group nfs_300_fg -node utna01 -port a0a-300
network interface failover-groups create -failover-group nfs_300_fg -node utna02 -port a0a-300
network interface failover-groups show
network interface failover show

# ------------------------------------------------------------------------------
# + Rename mgmt_cluster, mgmt_node LIFs
# ------------------------------------------------------------------------------

network interface show
network interface rename -vserver utna01 -lif mgmt1 -newname utna01_mgmt_node_lif
network interface rename -vserver utna02 -lif mgmt1 -newname utna02_mgmt_node_lif
network interface rename -vserver utnacm -lif cluster_mgmt -newname utnacm_mgmt_cluster_lif
network interface show

# ------------------------------------------------------------------------------
# + Modify mgmt_cluster, mgmt_node LIFs
# ------------------------------------------------------------------------------

network interface show
network interface failover show
network interface modify -vserver utna01 -lif utna01_mgmt_node_lif -home-node utna01 -home-port a0b -failover-group utna01_mgmt_node_fg -auto-revert true
network interface modify -vserver utna02 -lif utna02_mgmt_node_lif -home-node utna02 -home-port a0b -failover-group utna02_mgmt_node_fg -auto-revert true
network interface modify -vserver utnacm -lif utnacm_mgmt_cluster_lif -home-node utna01 -home-port a0b -failover-group utnacm_mgmt_cluster_fg -auto-revert true
network interface show
network interface show -failover

# ------------------------------------------------------------------------------
# + Create mgmt_cluster routing group default route
# ------------------------------------------------------------------------------

network routing-groups route show
network routing-groups route create -vserver utnacm_svm1 -routing-group c143.122.48.0/21 -gateway 143.122.48.1 -metric 20
network routing-groups route show

# ------------------------------------------------------------------------------
# + Configure current date, time and timezone
# ------------------------------------------------------------------------------

date
date [yymmdd]hhmm
timezone -timezone US/Mountain -version true
date

# ------------------------------------------------------------------------------
# + Configure SNMP
# ------------------------------------------------------------------------------

system snmp community show
system snmp community add -type ro -community-name public
system snmp community show
# ------------------------------------------------------------------------------
# + Configure NTP
# ------------------------------------------------------------------------------

ntp server show
ntp server create -node utna01 -server ntptime.mmm.com -version max
ntp server create -node utna02 -server ntptime.mmm.com -version max
set -privilege diagnostic
system services ntp config show
system services ntp config modify -enabled true
system services ntp config show
set -privilege admin

# ------------------------------------------------------------------------------
# + Enable CDP on each node
# ------------------------------------------------------------------------------

system node run -node utna01 -command options cdpd.enable on
system node run -node utna02 -command options cdpd.enable on

# ------------------------------------------------------------------------------
# + Verify that hardware assist is configured
# ------------------------------------------------------------------------------

storage failover hwassist show
storage failover modify -hwassist-partner-ip 143.122.48.12 -node utna01 -enabled true
storage failover modify -hwassist-partner-ip 143.122.48.11 -node utna02 -enabled true
storage failover hwassist show

# ------------------------------------------------------------------------------
# + Move network interfaces to their home nodes
# + Note: the following command will disconnect your current SSH session
# + Re-connect to utnacm cluster management interface via SSH (143.122.48.10)
# ------------------------------------------------------------------------------

network interface show -is-home false
network interface revert *
network interface show -is-home false

# ------------------------------------------------------------------------------
# + Perform connectivity tests
# ------------------------------------------------------------------------------

network ping -lif-owner utnacm -lif  -destination 

# ------------------------------------------------------------------------------
# + Add cluster software licenses
# ------------------------------------------------------------------------------

system license show
system license add -license-code ILBFJJXAFDUFZGFQXNBAAAAAAAAA
system license add -license-code GAXIKJXAFDUFZGFQXNBAAAAAAAAA
system license add -license-code CEOQMJXAFDUFZGFQXNBAAAAAAAAA
system license add -license-code WQDQIJXAFDUFZGFQXNBAAAAAAAAA
system license add -license-code MNHJOJXAFDUFZGFQXNBAAAAAAAAA
system license add -license-code QJQBMJXAFDUFZGFQXNBAAAAAAAAA
system license add -license-code EPSMLJXAFDUFZGFQXNBAAAAAAAAA
system license add -license-code OYLFNJXAFDUFZGFQXNBAAAAAAAAA
system license add -license-code UFZTJJXAFDUFZGFQXNBAAAAAAAAA
system license add -license-code ULKRWXKQACAAAAFQXNBAAAAAAAAA
system license add -license-code SAGVXXKQACAAAAFQXNBAAAAAAAAA
system license add -license-code OEXCAYKQACAAAAFQXNBAAAAAAAAA
system license add -license-code IRMCWXKQACAAAAFQXNBAAAAAAAAA
system license add -license-code YNQVBYKQACAAAAFQXNBAAAAAAAAA
system license add -license-code CKZNZXKQACAAAAFQXNBAAAAAAAAA
system license add -license-code QPBZYXKQACAAAAFQXNBAAAAAAAAA
system license add -license-code AZURAYKQACAAAAFQXNBAAAAAAAAA
system license add -license-code GGIGXXKQACAAAAFQXNBAAAAAAAAA
system license show

# ------------------------------------------------------------------------------
# + Create data aggregates on each node
# ------------------------------------------------------------------------------

storage aggregate show
storage aggregate create -aggregate utna02_900g_sas_aggr0 -node utna01 -diskcount 66 -disktype SAS -disksize 836 -maxraidsize 22 -raidtype raid_dp
storage aggregate create -aggregate utna02_900g_sas_aggr0 -node utna02 -diskcount 66 -disktype SAS -disksize 836 -maxraidsize 22 -raidtype raid_dp
storage aggregate show

# ------------------------------------------------------------------------------
# + Create data aggregates on each node
# ------------------------------------------------------------------------------

storage aggregate show
system node run -node utna01 -command snap sched -A utna01_mroot 0 0 0
system node run -node utna01 -command aggr options utna01_mroot nosnap on
system node run -node utna01 -command snap delete -A -a -f utna01_mroot
system node run -node utna01 -command snap reserve -A utna01_mroot 0
system node run -node utna01 -command snap sched -A utna02_900g_sas_aggr0 0 0 0
system node run -node utna01 -command aggr options utna02_900g_sas_aggr0 nosnap on
system node run -node utna01 -command snap delete -A -a -f utna02_900g_sas_aggr0
system node run -node utna01 -command snap reserve -A utna02_900g_sas_aggr0 0
system node run -node utna02 -command snap sched -A utna02_mroot 0 0 0
system node run -node utna02 -command aggr options utna02_mroot nosnap on
system node run -node utna02 -command snap delete -A -a -f utna02_mroot
system node run -node utna02 -command snap reserve -A utna02_mroot 0
system node run -node utna02 -command snap sched -A utna02_900g_sas_aggr0 0 0 0
system node run -node utna02 -command aggr options utna02_900g_sas_aggr0 nosnap on
system node run -node utna02 -command snap delete -A -a -f utna02_900g_sas_aggr0
system node run -node utna02 -command snap reserve -A utna02_900g_sas_aggr0 0
system node run -node * -command snap list -A
system node run -node * -command snap sched -A

# ------------------------------------------------------------------------------
# + Enable auto-giveback
# ------------------------------------------------------------------------------

storage failover show -fields auto-giveback
storage failover show -fields auto-giveback-after-panic
storage failover modify -node * -auto-giveback true
storage failover modify -node * -auto-giveback-after-panic true
storage failover show -fields auto-giveback
storage failover show -fields auto-giveback-after-panic

# ------------------------------------------------------------------------------
# + Disable/Enable Failover/Cluster HA on a 2-node cluster
# ------------------------------------------------------------------------------

storage ha show
cluster ha show
cluster ha modify -configured false
storage failover modify -node * -enabled false
## Wait a couple minutes prior to issuing following command
cluster ha modify -configured true
storage ha show
cluster ha show

# ------------------------------------------------------------------------------
# + Create storage virtual machine (SVM)
# ------------------------------------------------------------------------------

vserver create -vserver utnacm_svm1 -rootvolume  -aggregate utna02_900g_sas_aggr0 -ns-switch file -rootvolume-security-style unix -language en_US -quota-policy default
vserver modify -vserver utnacm_svm1 -disallowed-protocols cifs,fcp,iscsi
export-policy rule create -vserver utnacm_svm1 -policyname default -clientmatch 0.0.0.0/0 -rorule none -rwrule none -superuser none
vserver nfs create -vserver utnacm_svm1 -v3 enabled -v4.0 enabled
vserver show -vserver utnacm_svm1

# ------------------------------------------------------------------------------
# + Create SVM logical networking interfaces (LIFs - data & mgmt_svm)
# ------------------------------------------------------------------------------

network interface create -vserver utnacm_svm1 -lif utnacm_svm1_mgmt_svm_lif -role data -data-protocol none -home-node utna01 -home-port a0b -address 143.122.48.13 -netmask 255.255.248.0 -status-admin up -failover-policy nextavail -firewall-policy mgmt -auto-revert true -failover-group utnacm_mgmt_cluster_fg
network interface create -vserver utnacm_svm1 -lif nfs_300_lif -role data -data-protocol nfs -home-node utna01 -home-port a0a-300 -address 10.1.0.11 -netmask 255.255.255.0 -status-admin up -failover-policy nextavail -firewall-policy data -auto-revert true -failover-group nfs_300_fg
network interface create -vserver utnacm_svm1 -lif nfs_300_lif -role data -data-protocol nfs -home-node utna02 -home-port a0a-300 -address 10.1.0.12 -netmask 255.255.255.0 -status-admin up -failover-policy nextavail -firewall-policy data -auto-revert true -failover-group nfs_300_fg
################################################################################
### Create SVM root/loadshare mirror schedule
################################################################################

schedule interval create -name 5minRootLS -minutes 5

################################################################################
### Create SVM root/loadshare mirrors
################################################################################


################################################################################
### Create root/loadshare mirror snapmirror relationship
################################################################################


################################################################################
### Start root/loadshare snapmirror
################################################################################

snapmirror initialize-ls-set -source-path  -foreground true
snapmirror show -type LS

################################################################################
### Remove mroot aggrs from SVM List
################################################################################

vserver show -vserver utnacm_svm1 -fields aggr-list
vserver modify -vserver utnacm_svm1 -aggr-list utna02_900g_sas_aggr0,utna02_900g_sas_aggr0
vserver show -vserver utnacm_svm1 -fields aggr-list

# ------------------------------------------------------------------------------
# + Configure autosupport and send test message
# ------------------------------------------------------------------------------

system autosupport show
system autosupport modify -node utna01 -state enable -mail-hosts mailserv.mmm.com -from  -support enable -transport https -to rvkasparian@mmm.com
system autosupport modify -node utna02 -state enable -mail-hosts mailserv.mmm.com -from  -support enable -transport https -to rvkasparian@mmm.com
system autosupport invoke -node * -type test -message test
system autosupport show

# ------------------------------------------------------------------------------
# + Create management application local accounts ( use password: datalink1 )
# ------------------------------------------------------------------------------

security login show
security login create -username ocumc -application http -authmethod password -role admin
security login create -username ocumc -application ontapi -authmethod password -role admin
security login create -username ocumc -application ssh -authmethod password -role admin
security login create -username ocopm -application http -authmethod password -role admin
security login create -username ocopm -application ontapi -authmethod password -role admin
security login create -username ocopm -application ssh -authmethod password -role admin
security login show
# ------------------------------------------------------------------------------
# + Configure autosupport and send test message
# ------------------------------------------------------------------------------

system autosupport show
system autosupport modify -node utna01 -state enable -mail-hosts mailserv.mmm.com -from  -support enable -transport https -to rvkasparian@mmm.com
system autosupport modify -node utna02 -state enable -mail-hosts mailserv.mmm.com -from  -support enable -transport https -to rvkasparian@mmm.com
system autosupport invoke -node * -type test -message test
system autosupport show

