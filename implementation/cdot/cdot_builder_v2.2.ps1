Param(
  [Parameter(Mandatory=$True,Position=1)]
  [string]$datasource_name,
  [Parameter(Mandatory=$True,Position=2)]
  [string]$build_cmds,
  [Parameter(Mandatory=$True,Position=3)]
  [string]$build_docs,
  [Parameter(Mandatory=$True,Position=4)]
  [string]$build_id
  
)

$this_script = ($MyInvocation.MyCommand).Name

#region Header

$node_mgmt_ip_lookup				= @{}
$sp_mgmt_ip_lookup					= @{}
$svm_data_protocol_lookup			= @{}
$node_attrs							= New-Object System.Collections.ArrayList
$svm_disallowed_protocols			= New-Object System.Collections.ArrayList

$as_built_build_id						= New-Object System.Collections.ArrayList
$as_built_customer						= New-Object System.Collections.ArrayList
$as_built_networks						= New-Object System.Collections.ArrayList
$as_built_cluster						= New-Object System.Collections.ArrayList
$as_built_nodes							= New-Object System.Collections.ArrayList
$as_built_aggrs							= New-Object System.Collections.ArrayList
$as_built_ifgrps							= New-Object System.Collections.ArrayList
$as_built_svms							= New-Object System.Collections.ArrayList
$as_built_lifs							= New-Object System.Collections.ArrayList

#$aggr_rename_cmds					= New-Object System.Collections.ArrayList
#$network_create_ifgrp_ten_gbe_cmds	= New-Object System.Collections.ArrayList
#$network_create_vlan_ten_gbe_cmds	= New-Object System.Collections.ArrayList
#$network_create_vlan_one_gbe_cmds	= New-Object System.Collections.ArrayList
#$network_create_mgmt_node_fg_cmds	= New-Object System.Collections.ArrayList
#$network_create_mgmt_cluster_fg_cmds = New-Object System.Collections.ArrayList
#$network_create_data_fg_cmds		= New-Object System.Collections.ArrayList
#$network_create_ic_fg_cmds			= New-Object System.Collections.ArrayList
#$network_rename_mgmt_lif_cmds		= New-Object System.Collections.ArrayList
#$network_modify_mgmt_lif_cmds		= New-Object System.Collections.ArrayList
#$network_create_ic_lif_cmds			= New-Object System.Collections.ArrayList
#$network_svm_create_routing_group_cmds = New-Object System.Collections.ArrayList
#$network_create_ic_routing_group_cmds = New-Object System.Collections.ArrayList
#$network_revert_interface_cmds		= New-Object System.Collections.ArrayList
#$cdp_cmds							= New-Object System.Collections.ArrayList
#$hardware_assist_cmds				= New-Object System.Collections.ArrayList
#$ping_cmds							= New-Object System.Collections.ArrayList
#$license_cmds						= New-Object System.Collections.ArrayList
#$aggr_create_cmds					= New-Object System.Collections.ArrayList
#$aggr_no_snap_cmds					= New-Object System.Collections.ArrayList
#$auto_giveback_cmds					= New-Object System.Collections.ArrayList
#$two_node_failover_ha_disable_enable_cmds	= New-Object System.Collections.ArrayList
#$two_node_failover_ha_verify_cmds	= New-Object System.Collections.ArrayList
#$svm_create_cmds					= New-Object System.Collections.ArrayList
#$autosupport_cmds					= New-Object System.Collections.ArrayList
#$onboard_tgt_disable_cmds 			= New-Object System.Collections.ArrayList
#$onboard_tgt_modify_cmds			= New-Object System.Collections.ArrayList
#$reboot_all_cmds					= New-Object System.Collections.ArrayList
#$ucadmin_show_cmds					= New-Object System.Collections.ArrayList
#$disable_flow_ctl_cmds				= New-Object System.Collections.ArrayList
#$network_create_ifgrp_ten_gbe_cmds	= New-Object System.Collections.ArrayList
#$ntp_cmds							= New-Object System.Collections.ArrayList
$non_mroot_aggrs					= New-Object System.Collections.ArrayList
$svm_remove_mroot_cmds				= New-Object System.Collections.ArrayList
$svm_snapmirror_ls_mir_cmds			= New-Object System.Collections.ArrayList
$svm_lif_create_cmds				= New-Object System.Collections.ArrayList
$svm_create_ls_mir_cmds				= New-Object System.Collections.ArrayList
$all_cmds							= @()

$one_gbe_only_node_model_str = "FAS2220"
[int]$node_count		= $cdot_data.get_Item("cdot_nodes").Count
$customer_name_key		= ((($global_customer | select customer_name).customer_name).Replace(" ","")).ToLower()
$cluster_name			= $global_cdot_cluster_name
$cluster_name_nodash 	= $cluster_name.Replace("-","_")
$node_model				= $global_cdot_node_model

$datasource_basename	= Split-Path -Path $datasource_name -Leaf
$cmd_output_name 		= $customer_name_key + "_" + $cluster_name + "_cdot_build_cmds.txt"
$cmd_output_file 		= Join-Path -Path $cdot_cmds_path -ChildPath $cmd_output_name
$table_defs_name 		= "cdot_table_defs_" + $datasource_name + ".txt"
$table_defs_file 		= Join-Path -Path $cdot_reports_path -ChildPath $table_defs_name

# Synopsis:							Filter out "cdot_" network labels from $global_networks 
#									Trim "cdot_" prefix from network_label for use going forward
# Create an ArrayList: 				$cdot_networks
# of Custom Objects:				$cdot_networks_obj
# with Properties: 					build_id, network_label, vlan_id, subnet, ip_address_range
# from ArrayList of Custom Objects: $global_networks
# Unique key:						network_label
# Used by:							$cdot_ip_addresses ArrayList

#$cdot_networks 			= New-Object System.Collections.ArrayList
#$global_networks | where { $_.network_label -match "cdot_" } | %{
#	$cdot_networks_obj 			= "" | select build_id, network_label, vlan_id, subnet, gateway, ip_address_range
#	$cdot_networks_obj.build_id	= $_.build_id
#	$network_label_str  		= $_.network_label
#	if ($network_label_str -match "cdot_")
#		{
#		$cdot_networks_obj.network_label = $network_label_str.Substring(5)
#		}
#	else
#		{
#		$cdot_networks_obj.network_label = $network_label_str
#		}
#	$cdot_networks_obj.vlan_id     		= $_.vlan_id
#	$cdot_networks_obj.subnet    		= $_.subnet
#	$cdot_networks_obj.gateway    		= $_.gateway
#	$cdot_networks_obj.ip_address_range = $_.ip_address_range
#	[Void]$cdot_networks.Add($cdot_networks_obj)
#	}


#region Get global data
$global_customer | %{			
	$customer_name			= $_.customer_name
	$contact_name			= $_.contact_name
	$contact_email			= $_.contact_email
	$contact_phone			= $_.contact_phone
	}
$global_location | %{
	$customer_address		= $_.customer_address
	$system_install_address	= $_.system_install_address
	$rma_address			= $_.rma_address
	$rma_attn_to_name		= $_.rma_attn_to_name
	}
$global_datacenter | %{
	$domain_name			= $_.domain_name
	$dns_server_ips_str		= $_.dns_server_ips
	$ntp_server_names_str	= $_.ntp_server_names
	$smtp_server_name		= $_.smtp_server_name
	$snmp_server_name		= $_.snmp_server_name
	$snmp_community_string	= $_.snmp_community_string
	$timezone				= $_.timezone
	$default_admin_user		= $_.default_admin_user
	$default_admin_pass		= $_.default_admin_pass
	
	if ($dns_server_ips_str -match ",")
		{
		$dns_server_ips		= $dns_server_ips_str.Split(",")
		}
	else
		{
		$dns_server_ips		= @($dns_server_ips_str)
		}
	if ($ntp_server_names_str -match ",")
		{
		$ntp_server_names	= $ntp_server_names_str.Split(",")
		}
	else
		{
		$ntp_server_names	= @($ntp_server_names_str)
		}
	}
	
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "contact_name"
$as_built_customer_obj.customer_value	= $contact_name
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "contact_email"
$as_built_customer_obj.customer_value	= $contact_email
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "contact_phone"
$as_built_customer_obj.customer_value	= $contact_phone
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "customer_address"
$as_built_customer_obj.customer_value	= $customer_address
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "system_install_address"
$as_built_customer_obj.customer_value	= $system_install_address	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "rma_address"
$as_built_customer_obj.customer_value	= $rma_address	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "rma_attn_to_name"
$as_built_customer_obj.customer_value	= $rma_attn_to_name	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "domain_name"
$as_built_customer_obj.customer_value	= $domain_name	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "dns_server_ips"
$as_built_customer_obj.customer_value	= $dns_server_ips
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "ntp_server_names"
$as_built_customer_obj.customer_value	= $ntp_server_names	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "smtp_server_name"
$as_built_customer_obj.customer_value	= $smtp_server_name	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "snmp_server_name"
$as_built_customer_obj.customer_value	= $snmp_server_name	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "snmp_community_string"
$as_built_customer_obj.customer_value	= $snmp_community_string
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "timezone"
$as_built_customer_obj.customer_value	= $timezone	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "default_admin_user"
$as_built_customer_obj.customer_value	= $default_admin_user	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "default_admin_pass"
$as_built_customer_obj.customer_value	= $default_admin_pass	
[Void]$as_built_customer.Add($as_built_customer_obj)

#endregion


# Synopsis:							Calcuate each LIF ip_address based on specified ip_address_range 
#									and calculate LIF node_id based on specified node_range
# Create an ArrayList: 				$cdot_ip_addresses
# of Custom Objects:				$cdot_ip_addresses_obj
# with Properties: 					ip_address, subnet, netmask, gateway, vlan_id, network_label, node_id
# from ArrayList of Custom Objects: $cdot_networks
# Unique key:						ip_address
# Called by:						$cdot_network_labels ArrayList

$cdot_ip_addresses = New-Object System.Collections.ArrayList
$global_networks | where { $_.network_label -match "cdot_" } | %{
	$network_label_str		= $_.network_label
	$network_label 			= ($network_label_str).Substring(5)
	$node_number_range		= (($cdot_data.get_Item("cdot_lifs") | where { $_.network_label -eq $network_label_str } | select node_number_range).node_number_range).Split("-")
	[int]$node_range_start	= $node_number_range[0]
	[int]$node_range_end	= $node_number_range[1]
	[int]$lif_counter		= 1
	$node_range_counter 	= $node_range_start
	$cdot_ip_addresses_obj 	= "" | select ip_address, subnet, netmask, gateway, vlan_id, network_label, node_number, lif_counter
	$cdot_ip_address_range	= $_.ip_address_range
	$subnet_octets 			= ($_.subnet).Split(".")
	if ($cdot_ip_address_range -match ",")
		{
		$last_octets = $cdot_ip_address_range.Split(",")
		foreach ($octet in $last_octets)
			{
			if ($node_range_counter -gt $node_range_end)
				{
				$node_range_counter = 1
				}
			$cdot_ip_addresses_obj = "" | select ip_address, subnet, netmask, gateway, vlan_id, network_label, node_number, lif_counter
			$subnet_octets[3] = $octet
			$ip_address_str = $subnet_octets -join "."
		
			$cdot_ip_addresses_obj.ip_address 		= $ip_address_str
			$cdot_ip_addresses_obj.subnet 			= $_.subnet
			$cdot_ip_addresses_obj.netmask 			= $_.netmask
			$cdot_ip_addresses_obj.gateway 			= $_.gateway
			$cdot_ip_addresses_obj.vlan_id 			= $_.vlan_id
			$cdot_ip_addresses_obj.network_label	= $network_label
			$cdot_ip_addresses_obj.node_number		= $node_range_counter
			$cdot_ip_addresses_obj.lif_counter		= $lif_counter
		
			[Void]$cdot_ip_addresses.Add($cdot_ip_addresses_obj)
			$node_range_counter++
			$lif_counter++
			}
		}
	elseif ($cdot_ip_address_range -match "-")
		{
		
		[int]$ip_first 		= $cdot_ip_address_range.Split("-")[0]
		[int]$ip_last 		= $cdot_ip_address_range.Split("-")[1]
		$ip_counter 		= $ip_first
		$last_octets_arr 	= New-Object System.Collections.ArrayList
		while ($ip_counter -le $ip_last)
			{
			if ($node_range_counter -gt $node_range_end)
				{
				$node_range_counter = 1
				}
			$cdot_ip_addresses_obj = "" | select ip_address, subnet, netmask, gateway, vlan_id, network_label, node_number, lif_counter
			$subnet_octets[3] 	= $ip_counter
			$ip_address_str 	= $subnet_octets -join "."

			$cdot_ip_addresses_obj.ip_address 		= $ip_address_str
			$cdot_ip_addresses_obj.subnet 			= $_.subnet
			$cdot_ip_addresses_obj.netmask 			= $_.netmask
			$cdot_ip_addresses_obj.gateway 			= $_.gateway
			$cdot_ip_addresses_obj.vlan_id 			= $_.vlan_id
			$cdot_ip_addresses_obj.network_label	= $network_label
			$cdot_ip_addresses_obj.node_number		= $node_range_counter
			$cdot_ip_addresses_obj.lif_counter		= $lif_counter
			
			[Void]$cdot_ip_addresses.Add($cdot_ip_addresses_obj)
			$node_range_counter++
			$lif_counter++
			$ip_counter++
			}
		}
	elseif ($cdot_ip_address_range -ne "")
		{
		$cdot_ip_addresses_obj = "" | select ip_address, subnet, netmask, gateway, vlan_id, network_label, node_number, lif_counter
		$subnet_octets[3] 	= $cdot_ip_address_range
		$ip_address_str 	= $subnet_octets -join "."

		$cdot_ip_addresses_obj.ip_address 		= $ip_address_str
		$cdot_ip_addresses_obj.subnet 			= $_.subnet
		$cdot_ip_addresses_obj.netmask 			= $_.netmask
		$cdot_ip_addresses_obj.gateway 			= $_.gateway
		$cdot_ip_addresses_obj.vlan_id 			= $_.vlan_id
		$cdot_ip_addresses_obj.network_label	= $network_label
		$cdot_ip_addresses_obj.node_number		= 1
		$cdot_ip_addresses_obj.lif_counter		= $lif_counter
		[Void]$cdot_ip_addresses.Add($cdot_ip_addresses_obj)
		}
	}

# Synopsis:							Join cdot_ip_addresses, $cdot_data.cdot_lifs and $cdot_data.cdot_lif_attrs 
#									to collect all Properties required to build LIFs for each IP address
# Create an ArrayList: 				$cdot_network_labels
# of Custom Objects:				$cdot_network_labels_obj
# with Properties: 					ip_address, network_label, vlan_id, subnet, netmask, gateway, node_number, svm_number, svm_name, 
#									network_role, network_data_protocol, firewall_policy, failover_policy, node_range_start, node_range_end
# from ArrayList of Custom Objects: $cdot_ip_addresses
# that has Properties:				node_number, vlan_id, network_label, ip_address
# Unique Key(s):					ip_address
# Called by:						Used to build LIF Create commands

#$cdot_data.get_Item("cdot_lifs") | select network_label, svm_number
#exit

$cdot_merged_data 	= New-Object System.Collections.ArrayList
$cdot_ip_addresses | %{
	$cdot_merged_data_obj 						= "" | select ip_address, network_label, vlan_id, subnet, netmask, gateway, ip_address_partner, `
																node_number, node_def_name, node_aggr_mroot_def_name, node_aggr_mroot_name, node_name, node_name_nodash, software_licenses_str, `
																ten_gbe_ifgrp_name, ten_gbe_ifgrp_ports, ten_gbe_ifgrp_mode, ten_gbe_ifgrp_distr_func, one_gbe_ifgrp_name, one_gbe_ifgrp_ports, one_gbe_ifgrp_mode, one_gbe_ifgrp_distr_func, one_gbe_mgmt_port, `
																svm_number, svm_name, svm_name_nodash, `
																lif_role, lif_data_protocol, firewall_policy, failover_policy, lif_name, fg_name, fg_port, `
																autosupport_email_from
											
	$cdot_merged_data_obj.ip_address			= $_.ip_address
	$network_label 								= $_.network_label
	$network_label_str							= "cdot_" + $network_label 
	$cdot_merged_data_obj.network_label 		= $network_label
	$vlan_id									= $_.vlan_id
	$cdot_merged_data_obj.vlan_id     			= $vlan_id
	$cdot_merged_data_obj.subnet    			= $_.subnet
	$cdot_merged_data_obj.netmask     			= $_.netmask
	$cdot_merged_data_obj.gateway 				= $_.gateway
	$node_number								= $_.node_number
	$lif_counter								= $_.lif_counter

	if ($node_number % 2 -ne 0)
		{
		$node_number_partner 						= ($node_number + 1)
		$cdot_merged_data_obj.ip_address_partner 	= ($cdot_ip_addresses | where { ($_.network_label -eq "mgmt_node")-and ($_.node_number -eq $node_number_partner) } | select ip_address).ip_address
		}
	else
		{
		$node_number_partner 						= ($node_number - 1)
		$cdot_merged_data_obj.ip_address_partner 	= ($cdot_ip_addresses | where { ($_.network_label -eq "mgmt_node")-and ($_.node_number -eq $node_number_partner) } | select ip_address).ip_address
		}
		
	$cdot_merged_data_obj.node_number 			= $node_number
	$cdot_merged_data_obj.node_def_name			= $cluster_name + "-" + $node_number.ToString("00")
	$node_name_str 								= ($cdot_data.get_Item("cdot_nodes") | where { $_.node_number -eq $node_number } | select node_name).node_name
	$node_def_info  							= Get-NodeDefInfo -cluster_name $cluster_name -node_number $node_number
	if ($node_name_str -ne "use_default")
		{
		$node_name								= $node_name_str
		}
	else
		{
		$node_name								= $node_def_info[0]
		}

	if ($network_label -ne "mgmt_sp")
		{
		$node_name_nodash						= ($node_name).Replace("-", "_")
		}
	else
		{
		
		$node_name_nodash						= $node_name_str + "-sp"
		}

	$cdot_merged_data_obj.node_name 			= $node_name
	$cdot_merged_data_obj.node_name_nodash 		= $node_name_nodash
	$cdot_merged_data_obj.node_aggr_mroot_def_name 	= $node_def_info[1]
	$cdot_merged_data_obj.node_aggr_mroot_name		= $node_name_nodash + "_mroot"	
	$cdot_merged_data_obj.software_licenses_str = ($cdot_data.get_Item("cdot_nodes") | where { $_.node_number -eq $node_number } | select software_licenses).software_licenses
	$ten_gbe_ifgrp_name_str						= ($cdot_data.get_Item("cdot_controllers") | where { $_.node_model -eq $global_cdot_node_model} | select ten_gbe_ifgrp_name).ten_gbe_ifgrp_name
	$cdot_merged_data_obj.ten_gbe_ifgrp_ports	= ($cdot_data.get_Item("cdot_controllers") | where { $_.node_model -eq $global_cdot_node_model} | select ten_gbe_ifgrp_ports).ten_gbe_ifgrp_ports
	$cdot_merged_data_obj.ten_gbe_ifgrp_mode	= ($cdot_data.get_Item("cdot_ifgrps") | where { $_.build_id -eq $global_build_id } | select ten_gbe_ifgrp_mode).ten_gbe_ifgrp_mode
	$cdot_merged_data_obj.ten_gbe_ifgrp_distr_func = ($cdot_data.get_Item("cdot_ifgrps") | where { $_.build_id -eq $global_build_id } | select ten_gbe_ifgrp_distr_func).ten_gbe_ifgrp_distr_func
	$one_gbe_ifgrp_name_str						= ($cdot_data.get_Item("cdot_controllers") | where { $_.node_model -eq $global_cdot_node_model} | select one_gbe_ifgrp_name).one_gbe_ifgrp_name
	$cdot_merged_data_obj.one_gbe_ifgrp_ports	= ($cdot_data.get_Item("cdot_controllers") | where { $_.node_model -eq $global_cdot_node_model} | select one_gbe_ifgrp_ports).one_gbe_ifgrp_ports
	$cdot_merged_data_obj.one_gbe_ifgrp_mode	= ($cdot_data.get_Item("cdot_ifgrps") | where { $_.build_id -eq $global_build_id } | select one_gbe_ifgrp_mode).one_gbe_ifgrp_mode
	$cdot_merged_data_obj.one_gbe_ifgrp_distr_func = ($cdot_data.get_Item("cdot_ifgrps") | where { $_.build_id -eq $global_build_id } | select one_gbe_ifgrp_distr_func).one_gbe_ifgrp_distr_func
	$cdot_merged_data_obj.one_gbe_mgmt_port		= ($cdot_data.get_Item("cdot_controllers") | where { $_.node_model -eq $global_cdot_node_model } | select one_gbe_mgmt_port).one_gbe_mgmt_port
	$svm_number									= ($cdot_data.get_Item("cdot_lifs") | where { $_.network_label -eq $network_label_str } | select svm_number).svm_number
	$cdot_merged_data_obj.svm_number			= $svm_number
	$svm_name									= $cluster_name + "_svm" + $svm_number
	$cdot_merged_data_obj.svm_name				= $svm_name
	$svm_name_nodash							= ($svm_name).Replace("-", "_")
	$cdot_merged_data_obj.svm_name_nodash		= $svm_name_nodash
	$cdot_merged_data_obj.lif_role				= ($cdot_data.get_Item("cdot_lif_attrs") | where { $_.network_label -eq $network_label } | select lif_role).lif_role
	$cdot_merged_data_obj.lif_data_protocol		= ($cdot_data.get_Item("cdot_lif_attrs") | where { $_.network_label -eq $network_label } | select lif_data_protocol).lif_data_protocol
	$cdot_merged_data_obj.firewall_policy		= ($cdot_data.get_Item("cdot_lif_attrs") | where { $_.network_label -eq $network_label } | select firewall_policy).firewall_policy
	$cdot_merged_data_obj.failover_policy		= ($cdot_data.get_Item("cdot_lif_attrs") | where { $_.network_label -eq $network_label } | select failover_policy).failover_policy

	if ($vlan_id -ne "access")
		{
		$ten_gbe_ifgrp_name = $ten_gbe_ifgrp_name_str# + "-" + $vlan_id
		$one_gbe_ifgrp_name = $one_gbe_ifgrp_name_str# + "-" + $vlan_id
		$ten_gbe_ifgrp_vlan_port = $ten_gbe_ifgrp_name_str + "-" + $vlan_id
		$one_gbe_ifgrp_vlan_port = $one_gbe_ifgrp_name_str + "-" + $vlan_id
		$label_str 	= $network_label + "_" + $vlan_id + "_" 	
		if ($network_label -notmatch "iscsi")
			{
			if ($network_label -eq "intercluster")
				{
				$lif_name 	= $node_name_nodash + "_" + $label_str + "lif"
				$fg_name	= $node_name_nodash + "_" + $label_str + "fg"
				$fg_port	= $ten_gbe_ifgrp_vlan_port
				}			
			elseif ($network_label -eq "mgmt_svm")
				{
				$lif_name  	= $svm_name_nodash + "_" + $label_str + "lif"
				$fg_name	= $cluster_name_nodash + "_mgmt_cluster_" + $vlan_id + "_fg"
				$fg_port	= $one_gbe_ifgrp_vlan_port
				}
			elseif ($network_label -eq "mgmt_cluster")
				{
				$lif_name 	= $cluster_name_nodash + "_" + $label_str + "lif"
				$fg_name	= $cluster_name_nodash + "_" + $label_str + "fg"
				$fg_port	= $one_gbe_ifgrp_vlan_port
				}
			elseif ($network_label -eq "mgmt_node")
				{
				$lif_name 	= $node_name_nodash + "_" + $label_str + "lif"
				$fg_name	= $node_name_nodash + "_" + $label_str + "fg"
				$fg_port	= $one_gbe_ifgrp_vlan_port
				}
			elseif ($network_label -ne "mgmt_sp")
				{
				$lif_counter_str = $lif_counter.ToString("00")
				$lif_name  	= $label_str + "lif" + $lif_counter_str
				$fg_name 	= $label_str + "fg"
				$fg_port	= $ten_gbe_ifgrp_vlan_port
				
				}
			else
				{
				$lif_name  	= "not_used"
				$fg_name 	= "not_used"
				$fg_port	= "not_used"				
				}
			}
		elseif ($network_label -match "iscsi")
			{
			$lif_counter_str = $lif_counter.ToString("00")
			$lif_name  	= $label_str + "lif" + $lif_counter_str
			$fg_name		= $label_str + "fg"
			$fg_port		= $ten_gbe_ifgrp_vlan_port
			
			}
		}
	else
		{
		$ten_gbe_ifgrp_name = $ten_gbe_ifgrp_name_str
		$one_gbe_ifgrp_name = $one_gbe_ifgrp_name_str
		$label_str 			= $network_label + "_"
		if ($network_label -notmatch "iscsi")
			{
			if ($network_label -eq "intercluster")
				{
				$lif_name 	= $node_name_nodash + "_" + $label_str + "lif"
				$fg_name	= $node_name_nodash + "_" + $label_str + "fg"
				$fg_port	= $ten_gbe_ifgrp_name
				}			
			elseif ($network_label -eq "mgmt_svm")
				{
				$lif_name  	= $svm_name_nodash + "_" + $label_str + "lif"
				$fg_name	= $cluster_name_nodash + "_mgmt_cluster_" + "fg"
				$fg_port	= $one_gbe_ifgrp_name
				}
			elseif ($network_label -eq "mgmt_cluster")
				{
				$lif_name 	= $cluster_name_nodash + "_" + $label_str + "lif"
				$fg_name	= $cluster_name_nodash + "_" + $label_str + "fg"
				$fg_port	= $one_gbe_ifgrp_name
				}
			elseif ($network_label -eq "mgmt_node")
				{
				$lif_name 	= $node_name_nodash + "_" + $label_str + "lif"
				$fg_name	= $node_name_nodash + "_" + $label_str + "fg"
				$fg_port	= $one_gbe_ifgrp_name
				}
			elseif ($network_label -ne "mgmt_sp")
				{
				$lif_counter_str = $lif_counter.ToString("00")
				$lif_name  	= $label_str + "lif" + $lif_counter_str
				$fg_name 	= $label_str + "fg"
				$fg_port	= $ten_gbe_ifgrp_name
				
				}
			else
				{
				$lif_name  	= "not_used"
				$fg_name 	= "not_used"
				$fg_port	= "not_used"				
				}
			}
		elseif ($network_label -match "iscsi")
			{
			$lif_counter_str = $lif_counter.ToString("00")
			$lif_name  		= $label_str + "lif" + $lif_counter_str
			$fg_name		= $label_str + "fg"
			$fg_port		= $ten_gbe_ifgrp_name
			
			}
	}

	$cdot_merged_data_obj.lif_name				= $lif_name
	$cdot_merged_data_obj.fg_name				= $fg_name
	
	if ($global_cdot_node_model -ne $one_gbe_only_node_model_str)
		{
		$cdot_merged_data_obj.ten_gbe_ifgrp_name	= $ten_gbe_ifgrp_name
		$cdot_merged_data_obj.fg_port				= $fg_port
		}
	else
		{
		$cdot_merged_data_obj.ten_gbe_ifgrp_name	= $one_gbe_ifgrp_name
		$cdot_merged_data_obj.fg_port				= $one_gbe_ifgrp_vlan_port
		}
	$cdot_merged_data_obj.one_gbe_ifgrp_name	= $one_gbe_ifgrp_name
	$cdot_merged_data_obj.autosupport_email_from = $node_name + "@" + $domain_name
	[Void]$cdot_merged_data.Add($cdot_merged_data_obj)
	}

#$cdot_merged_data | select network_label, lif_name, fg_name, fg_port  | ft -Auto #Export-Csv -Path ".\cdot_merged_data.csv" -NoTypeInformation
#exit

Write-Host ""
Write-Host $section_break
Write-Host -ForegroundColor Cyan "Using the following parameters:"
Write-Host -ForegroundColor Cyan "Build ID:   $build_id"
Write-Host -ForegroundColor Cyan "Customer:   $customer_name_key"
Write-Host -ForegroundColor Cyan "Cluster:    $cluster_name"
Write-Host -ForegroundColor Cyan "Controller: $node_model"
Write-Host $section_break
Write-Host ""

$networks_vlans = $cdot_ip_addresses | select vlan_id | %{ $_.vlan_id } | sort -Unique
$ic_enabled 	= $cdot_merged_data | where { $_.network_label -eq "intercluster" }
$lifs_vlans 	= $cdot_merged_data | select vlan_id | %{ $_.vlan_id } | sort -Unique
$vlan_chk 		= compare $networks_vlans $lifs_vlans

if ($vlan_chk)
	{
	Write-Host ""
	Write-Host -ForegroundColor Yellow $section_break
	Write-Host -ForegroundColor Yellow "# + Network configuration information inconsistency detected"
	Write-Host -ForegroundColor Yellow "# +"

	$vlan_chk | %{
		$diff_vlan_id 	= $_.InputObject
		$side_indicator	= $_.SideIndicator
		if ($side_indicator -eq "<=")
			{
			Write-Host -ForegroundColor Yellow "# + $excel_workbook_name`:t_networks contains vlan_id $diff_vlan_id not found in $excel_workbook_name`:t_lifs"
			Write-Host -ForegroundColor Yellow "# + Please validate and update configuration, t_networks cannot reference a vlan_id that doesn't exist in t_lifs"
			Write-Host -ForegroundColor Yellow "# + Exiting..."
			Write-Host -ForegroundColor Yellow "# +"
			Write-Host -ForegroundColor Yellow $section_break
			exit
			}
		elseif ($side_indicator -eq "=>")
			{
			Write-Host -ForegroundColor Yellow "# + $excel_workbook_name`:t_lifs contains vlan_id $diff_vlan_id not found in $excel_workbook_name`:t_networks"
			Write-Host -ForegroundColor Yellow "# + Please validate and update configuration, t_lifs cannot reference a vlan_id that doesn't exist in t_networks"
			Write-Host -ForegroundColor Yellow "# + Exiting..."
			Write-Host -ForegroundColor Yellow "# +"
			Write-Host -ForegroundColor Yellow $section_break
			exit
			}
		}
		
	}


Get-TableDefs -datasource_name $datasource_name

$global_networks | where { $_.network_label -match "cdot_"  } | %{
	$as_built_networks_obj = "" | select network_label, vlan_id, subnet, netmask, gateway
	$as_built_networks_obj.network_label	= $_.network_label
	$as_built_networks_obj.vlan_id		= $_.vlan_id
	$as_built_networks_obj.subnet		= $_.subnet
	$as_built_networks_obj.netmask		= $_.netmask
	$as_built_networks_obj.gateway		= $_.gateway
	[Void]$as_built_networks.Add($as_built_networks_obj)
	}

#region Customer

#region Cluster

$cdot_data.get_Item("cdot_clusters") | %{			
	$base_license			= $_.base_license
	$autosupport_enable		= $_.autosupport_enable
	$autosupport_transport	= $_.autosupport_transport
	$autosupport_email_to	= $_.autosupport_email_to
	$ocum_user				= $_.ocum_user
	$ocopm_user				= $_.ocopm_user
	#$mgmt_ip_cluster = Get-MgmtIPs -network_label "mgmt_cluster"
	$mgmt_ip_cluster 		= $cdot_ip_addresses | where { ($_.network_label -eq "mgmt_cluster") } | select ip_address | %{ $_.ip_address }
	$mgmt_ip_node 			= $cdot_ip_addresses | where { ( ($_.network_label -eq "mgmt_node") -and ($_.node_number -eq 1)) } | select ip_address | %{ $_.ip_address }
	$mgmt_ip_sp 			= $cdot_ip_addresses | where { ( ($_.network_label -eq "mgmt_sp") -and ($_.node_number -eq 1)) } | select ip_address | %{ $_.ip_address }

	# Build as-built cluster object	
	$as_built_cluster_obj = "" | select cluster_attribute, cluster_value
	$as_built_cluster_obj.cluster_attribute 	= "cluster_name"
	$as_built_cluster_obj.cluster_value 		= $cluster_name
	[Void]$as_built_cluster.Add($as_built_cluster_obj)
	$as_built_cluster_obj = "" | select cluster_attribute, cluster_value
	$as_built_cluster_obj.cluster_attribute 	= "base_license"
	$as_built_cluster_obj.cluster_value 		= $_.base_license
	[Void]$as_built_cluster.Add($as_built_cluster_obj)
	$as_built_cluster_obj = "" | select cluster_attribute, cluster_value
	$as_built_cluster_obj.cluster_attribute 	= "autosupport_enable"
	$as_built_cluster_obj.cluster_value 		= $_.autosupport_enable
	[Void]$as_built_cluster.Add($as_built_cluster_obj)
	$as_built_cluster_obj = "" | select cluster_attribute, cluster_value
	$as_built_cluster_obj.cluster_attribute 	= "autosupport_transport"
	$as_built_cluster_obj.cluster_value 		= $_.autosupport_transport
	[Void]$as_built_cluster.Add($as_built_cluster_obj)
	$as_built_cluster_obj = "" | select cluster_attribute, cluster_value
	$as_built_cluster_obj.cluster_attribute 	= "autosupport_email_to"
	$as_built_cluster_obj.cluster_value 		= $_.autosupport_email_to
	[Void]$as_built_cluster.Add($as_built_cluster_obj)
	$as_built_cluster_obj = "" | select cluster_attribute, cluster_value
	$as_built_cluster_obj.cluster_attribute 	= "ocum_user"
	$as_built_cluster_obj.cluster_value 		= $_.ocum_user
	[Void]$as_built_cluster.Add($as_built_cluster_obj)
	$as_built_cluster_obj = "" | select cluster_attribute, cluster_value
	$as_built_cluster_obj.cluster_attribute 	= "ocopm_user"
	$as_built_cluster_obj.cluster_value 		= $_.ocopm_user
	[Void]$as_built_cluster.Add($as_built_cluster_obj)
	}

$mgmt_ip_cluster_vlan_id 	= ($cdot_ip_addresses | where { $_.network_label -eq "mgmt_cluster" } | select vlan_id).vlan_id
$mgmt_ip_cluster_subnet 	= ($cdot_ip_addresses | where { $_.network_label -eq "mgmt_cluster" } | select subnet).subnet
$mgmt_ip_cluster_netmask 	= ($cdot_ip_addresses | where { $_.network_label -eq "mgmt_cluster" } | select netmask).netmask
$mgmt_ip_cluster_gateway 	= ($cdot_ip_addresses | where { $_.network_label -eq "mgmt_cluster" } | select gateway).gateway
$mgmt_ip_node_vlan_id 		= ($cdot_ip_addresses | where { ($_.network_label -eq "mgmt_node") -and ($_.node_number -eq 1)} | select vlan_id).vlan_id
$mgmt_ip_node_subnet 		= ($cdot_ip_addresses | where { ($_.network_label -eq "mgmt_node") -and ($_.node_number -eq 1) } | select subnet).subnet
$mgmt_ip_node_netmask 		= ($cdot_ip_addresses | where { ($_.network_label -eq "mgmt_node") -and ($_.node_number -eq 1) } | select netmask).netmask
$mgmt_ip_node_gateway 		= ($cdot_ip_addresses | where { ($_.network_label -eq "mgmt_node") -and ($_.node_number -eq 1) } | select gateway).gateway
$mgmt_ip_sp_vlan_id 		= ($cdot_ip_addresses | where { ($_.network_label -eq "mgmt_sp") -and ($_.node_number -eq 1) } | select vlan_id).vlan_id
$mgmt_ip_sp_subnet 			= ($cdot_ip_addresses | where { ($_.network_label -eq "mgmt_sp") -and ($_.node_number -eq 1) } | select subnet).subnet
$mgmt_ip_sp_netmask 		= ($cdot_ip_addresses | where { ($_.network_label -eq "mgmt_sp") -and ($_.node_number -eq 1) } | select netmask).netmask
$mgmt_ip_sp_gateway 		= ($cdot_ip_addresses | where { ($_.network_label -eq "mgmt_sp") -and ($_.node_number -eq 1) } | select gateway).gateway
$ic_vlan_id 				= ($cdot_ip_addresses | where { ($_.network_label -eq "intercluster") -and ($_.node_number -eq 1) } | select vlan_id).vlan_id
$ic_subnet 					= ($cdot_ip_addresses | where { ($_.network_label -eq "intercluster") -and ($_.node_number -eq 1) } | select subnet).subnet
$ic_netmask 				= ($cdot_ip_addresses | where { ($_.network_label -eq "intercluster") -and ($_.node_number -eq 1) } | select netmask).netmask
$ic_gateway 				= ($cdot_ip_addresses | where { ($_.network_label -eq "intercluster") -and ($_.node_number -eq 1) } | select gateway).gateway
#endregion

#region One time CMD blocks

# header_summary_cmds
$header_summary1_cmds		= New-Object System.Collections.ArrayList
$header_summary1_brk_str	=	"Welcome to cDOT setup;" + `
								"Summary of installation parameters defined in:;" + `
								"$global_presite_name;" + `
								"$cdot_datasource_name"
[Void]$header_summary1_cmds.Add((Build-CmdHeader $header_summary1_brk_str))


$mgmt_ip_cluster_str	= $mgmt_ip_cluster + " | " + $mgmt_ip_cluster_subnet + " | " + $mgmt_ip_cluster_gateway
$mgmt_ip_node_str		= $mgmt_ip_node + " | " + $mgmt_ip_node_subnet + " | " + $mgmt_ip_node_gateway
$mgmt_ip_sp_str			= $mgmt_ip_sp + " | " + $mgmt_ip_sp_subnet + " | " + $mgmt_ip_sp_gateway
$location_str			= $customer_name + ", " + $system_install_address


$header_summary2_cmds		= New-Object System.Collections.ArrayList
$header_summary2_brk_str	=	"Cluster Name: $cluster_name;" + `
								"Cluster Base License: $base_license;" + `
								"Administrator Password (user: $default_admin_user): $default_admin_pass;" + `
								"Cluster Management Port (temporary for install): e0M;" + `
								"Cluster Management IP Info: $mgmt_ip_cluster_str;" + `
								"Domain Name: $domain_name;" + `
								"DNS Servers: $dns_server_ips_str;" + `
								"Location: $location_str;" + `
								"Node Management Port (temporary for install): e0M;" + `
								"Node Management IP Info: $mgmt_ip_node_str;" + `
								"Node SP Management IP Info: $mgmt_ip_sp_str"
[Void]$header_summary2_cmds.Add((Build-CmdHeader $header_summary2_brk_str))

# term_rows_0_cmds
$term_rows_0_cmds			= New-Object System.Collections.ArrayList
$term_rows_0_brk_str 		= "Set terminal to eliminate pause"
$term_rows_0_cmd_str 		= "rows 0;"
[Void]$term_rows_0_cmds.Add((Build-CmdHeader $term_rows_0_brk_str))
[Void]$term_rows_0_cmds.Add((Build-CmdContent $term_rows_0_cmd_str))

# system_initialize_cmds
$system_initialize_cmds		= New-Object System.Collections.ArrayList
$system_initialize_brk_str 	= 	"Set system to factory defaults"
$system_initialize_cmd_str 	= 	"set-defaults;" + `
								"setenv bootarg.init.boot_clustered true;" + `
								"saveenv;" + `
								"autoboot;" + `
								"ctrl-c (boot menu);" + `
								"5 (maintenance mode) to assign disks;" + `
								"reboot;" + `
								"ctrl-c (boot menu);" + `
								"4 (zero);"
[Void]$system_initialize_cmds.Add((Build-CmdHeader $system_initialize_brk_str))
[Void]$system_initialize_cmds.Add((Build-CmdContent $system_initialize_cmd_str))

# $network_interface_revert_cmds
$network_interface_revert_cmds 		= New-Object System.Collections.ArrayList
$network_interface_revert_brk_str 	= 	"Move network interfaces to their home nodes;" + `
										"Note: the following command will disconnect your current SSH session;" + `
										"Re-connect to $cluster_name cluster management interface via SSH ($mgmt_ip_cluster)"
$network_interface_revert_cmd_str 	= 	"network interface show -is-home false;" + `
										"network interface revert *;" + `
										"network interface show -is-home false;"
[Void]$network_interface_revert_cmds.Add((Build-CmdHeader $network_interface_revert_brk_str))
[Void]$network_interface_revert_cmds.Add((Build-CmdContent $network_interface_revert_cmd_str))

$timezone_cmds						= New-Object System.Collections.ArrayList
$timezone_brk_str					= 	"Configure current date, time and timezone"
$timezone_cmd_str					= 	"date;" + `
										"date [yymmdd]hhmm;" + `
										"timezone -timezone $timezone -version true;" + `
										"date;"
[Void]$timezone_cmds.Add((Build-CmdHeader $timezone_brk_str))
[Void]$timezone_cmds.Add((Build-CmdContent $timezone_cmd_str))

# snmp_cmds
$snmp_cmds							= New-Object System.Collections.ArrayList
$snmp_brk_str						= 	"Configure SNMP"
$snmp_cmd_str						= 	"system snmp community show;" + `
										"system snmp community add -type ro -community-name $snmp_community_string;" + `
										"system snmp community show;"
[Void]$timezone_cmds.Add((Build-CmdHeader $snmp_brk_str))
[Void]$timezone_cmds.Add((Build-CmdContent $snmp_cmd_str))

# create_mgmt_users_cmds
$create_mgmt_users_cmds				= New-Object System.Collections.ArrayList
$create_mgmt_users_brk_str			= 	"Create management application local accounts ( use password: $default_admin_pass )"
$create_mgmt_users_cmd_str			= 	"security login show;" + `
										"security login create -username $ocum_user -application http -authmethod password -role admin;" + `
										"security login create -username $ocum_user -application ontapi -authmethod password -role admin;" + `
										"security login create -username $ocum_user -application ssh -authmethod password -role admin;" + `
										"security login create -username $ocopm_user -application http -authmethod password -role admin;" + `
										"security login create -username $ocopm_user -application ontapi -authmethod password -role admin;" + `
										"security login create -username $ocopm_user -application ssh -authmethod password -role admin;" + `
										"security login show;"
[Void]$create_mgmt_users_cmds.Add((Build-CmdHeader $create_mgmt_users_brk_str))
[Void]$create_mgmt_users_cmds.Add((Build-CmdContent $create_mgmt_users_cmd_str))

# node_rename_cmds
$node_rename_cmds					= New-Object System.Collections.ArrayList
$node_rename_brk_str				= 	"Rename nodes"
$node_rename_cmd_str				= 	"system node show"
[Void]$node_rename_cmds.Add((Build-CmdHeader $node_rename_brk_str))
[Void]$node_rename_cmds.Add((Build-CmdContent $node_rename_cmd_str))

# aggr_rename_cmds
$aggr_rename_cmds					= New-Object System.Collections.ArrayList
$aggr_rename_brk_str				= 		"Rename mroot aggregates"
$aggr_rename_cmd_str				= 		"aggregate show"
[Void]$aggr_rename_cmds.Add((Build-CmdHeader $aggr_rename_brk_str))
[Void]$aggr_rename_cmds.Add((Build-CmdContent $aggr_rename_cmd_str))

if ($global_cdot_node_model -ne $one_gbe_only_node_model_str)
	{
	# network_create_ifgrp_ten_gbe_cmds
	$network_create_ifgrp_ten_gbe_cmds	  = New-Object System.Collections.ArrayList
	$network_create_ifgrp_ten_gbe_brk_str = 	"Create Data interface groups on each node"
	$network_create_ifgrp_ten_gbe_cmd_str = 	"network port ifgrp show"
	[Void]$network_create_ifgrp_ten_gbe_cmds.Add((Build-CmdHeader $network_create_ifgrp_ten_gbe_brk_str))
	[Void]$network_create_ifgrp_ten_gbe_cmds.Add((Build-CmdContent $network_create_ifgrp_ten_gbe_cmd_str))
	}

# network_create_ifgrp_one_gbe_cmds
$network_create_ifgrp_one_gbe_cmds	  = New-Object System.Collections.ArrayList
$network_create_ifgrp_one_gbe_brk_str = 	"Create Mgmt interface groups on each node"
$network_create_ifgrp_one_gbe_cmd_str = 	"network port ifgrp show"
[Void]$network_create_ifgrp_one_gbe_cmds.Add((Build-CmdHeader $network_create_ifgrp_one_gbe_brk_str))
[Void]$network_create_ifgrp_one_gbe_cmds.Add((Build-CmdContent $network_create_ifgrp_one_gbe_cmd_str))

# network_create_mgmt_node_fg_cmds
$network_create_mgmt_node_fg_cmds	  = New-Object System.Collections.ArrayList
$network_create_mgmt_node_fg_brk_str  = 	"Create node management failover groups"
$network_create_mgmt_node_fg_cmd_str  = 	"network interface failover-groups show;" + `
											"network interface failover show"
[Void]$network_create_mgmt_node_fg_cmds.Add((Build-CmdHeader $network_create_mgmt_node_fg_brk_str))
[Void]$network_create_mgmt_node_fg_cmds.Add((Build-CmdContent $network_create_mgmt_node_fg_cmd_str))

# network_create_mgmt_cluster_fg_cmds
$network_create_mgmt_cluster_fg_cmds	 = New-Object System.Collections.ArrayList
$network_create_mgmt_cluster_fg_brk_str  = 	"Create cluster/SVM management failover groups"
$network_create_mgmt_cluster_fg_cmd_str  = 	"network interface failover-groups show;" + `
											"network interface failover show"
[Void]$network_create_mgmt_cluster_fg_cmds.Add((Build-CmdHeader $network_create_mgmt_cluster_fg_brk_str))
[Void]$network_create_mgmt_cluster_fg_cmds.Add((Build-CmdContent $network_create_mgmt_cluster_fg_cmd_str))

# network_create_data_fg_cmds
$network_create_data_fg_cmds	 		= New-Object System.Collections.ArrayList
$network_create_data_fg_brk_str  		= 	"Create data failover groups"
$network_create_data_fg_cmd_str  		= 	"network interface failover-groups show;" + `
											"network interface failover show"
[Void]$network_create_data_fg_cmds.Add((Build-CmdHeader $network_create_data_fg_brk_str))
[Void]$network_create_data_fg_cmds.Add((Build-CmdContent $network_create_data_fg_cmd_str))

# network_create_cm_routing_group_cmds
$network_svm_create_routing_group_cmds	= New-Object System.Collections.ArrayList
$network_create_cm_routing_group_brk_str = 	"Create mgmt_cluster routing group default route"
$network_create_cm_routing_group_cmd_str = 	"network routing-groups route show"
[Void]$network_svm_create_routing_group_cmds.Add((Build-CmdHeader $network_create_cm_routing_group_brk_str))
[Void]$network_svm_create_routing_group_cmds.Add((Build-CmdContent $network_create_cm_routing_group_cmd_str))

if ($ic_enabled)
	{
	# network_create_ic_fg_cmds
	$network_create_ic_fg_cmds				= New-Object System.Collections.ArrayList
	$network_create_ic_fg_brk_str 			= 	"Create intercluster failover groups"
	$network_create_ic_fg_cmd_str 			= 	"network interface failover-groups show;" + `
												"network interface failover show"
	[Void]$network_create_ic_fg_cmds.Add((Build-CmdHeader $network_create_ic_fg_brk_str))
	[Void]$network_create_ic_fg_cmds.Add((Build-CmdContent $network_create_ic_fg_cmd_str))

	# network_create_ic_lif_cmds
	$network_create_ic_lif_cmds				= New-Object System.Collections.ArrayList
	$network_create_ic_lif_brk_str 			= 	"Create intercluster LIFs on each node"
	$network_create_ic_lif_cmd_str 			= 	"network interface show"
	[Void]$network_create_ic_lif_cmds.Add((Build-CmdHeader $network_create_ic_lif_brk_str))
	[Void]$network_create_ic_lif_cmds.Add((Build-CmdContent $network_create_ic_lif_cmd_str))
	
	# network_create_ic_routing_group_cmds
	$network_create_ic_routing_group_cmds	 = New-Object System.Collections.ArrayList
	$network_create_ic_routing_group_brk_str = 	"Create intercluster routing groups on each node"
	$network_create_ic_routing_group_cmd_str = 	"network interface failover-groups show;" + `
												"network interface failover show"
	[Void]$network_create_ic_routing_group_cmds.Add((Build-CmdHeader $network_create_ic_routing_group_brk_str))
	[Void]$network_create_ic_routing_group_cmds.Add((Build-CmdContent $network_create_ic_routing_group_cmd_str))
	}

# network_rename_mgmt_lif_cmds
$network_rename_mgmt_lif_cmds				= New-Object System.Collections.ArrayList
$network_rename_mgmt_lif_brk_str 			= 	"Rename mgmt_cluster, mgmt_node LIFs"
$network_rename_mgmt_lif_cmd_str 			= 	"network interface show"
[Void]$network_rename_mgmt_lif_cmds.Add((Build-CmdHeader $network_rename_mgmt_lif_brk_str))
[Void]$network_rename_mgmt_lif_cmds.Add((Build-CmdContent $network_rename_mgmt_lif_cmd_str))

# network_modify_mgmt_lif_cmds
$network_modify_mgmt_lif_cmds	 			= New-Object System.Collections.ArrayList
$network_modify_mgmt_lif_brk_str 			= 	"Modify mgmt_cluster, mgmt_node LIFs"
$network_modify_mgmt_lif_cmd_str 			= 	"network interface show;" + `
												"network interface failover show"
[Void]$network_modify_mgmt_lif_cmds.Add((Build-CmdHeader $network_modify_mgmt_lif_brk_str))
[Void]$network_modify_mgmt_lif_cmds.Add((Build-CmdContent $network_modify_mgmt_lif_cmd_str))

# ntp_cmds
$ntp_cmds	 								= New-Object System.Collections.ArrayList
$ntp_brk_str 								= 	"Configure NTP"
$ntp_cmd_str 								= 	"ntp server show"
[Void]$ntp_cmds.Add((Build-CmdHeader $ntp_brk_str))
[Void]$ntp_cmds.Add((Build-CmdContent $ntp_cmd_str))

# cdp_cmds
$cdp_cmds	 								= New-Object System.Collections.ArrayList
$cdp_brk_str 								= "Enable CDP on each node"
[Void]$cdp_cmds.Add((Build-CmdHeader $cdp_brk_str))

# hardware_assist_cmds
$hardware_assist_cmds	 					= New-Object System.Collections.ArrayList
$hardware_assist_brk_str 					= 	"Verify that hardware assist is configured"
$hardware_assist_cmd_str 					= 	"storage failover hwassist show"
[Void]$hardware_assist_cmds.Add((Build-CmdHeader $hardware_assist_brk_str))
[Void]$hardware_assist_cmds.Add((Build-CmdContent $hardware_assist_cmd_str))

# ping cmds
$mgmt_cluster_lif_name		= ($cdot_merged_data | where { $_.network_label -eq "mgmt_cluster" } | select lif_name).lif_name
$mgmt_ip_cluster_gateway	= ($cdot_merged_data | where { $_.network_label -eq "mgmt_cluster" } | select gateway).gateway
$ping_cmds									= New-Object System.Collections.ArrayList
$ping_brk_str								= "Perform connectivity tests"
$ping_cmd_str								= "network ping -lif-owner $cluster_name -lif $mgmt_cluster_lif_name -destination $mgmt_ip_cluster_gateway"
[Void]$ping_cmds.Add((Build-CmdHeader $ping_brk_str))
[Void]$ping_cmds.Add((Build-CmdContent $ping_cmd_str))

# license cmds
$license_cmds								= New-Object System.Collections.ArrayList
$license_brk_str							= "Add cluster software licenses"
$license_cmd_str							= "system license show"
[Void]$license_cmds.Add((Build-CmdHeader $license_brk_str))
[Void]$license_cmds.Add((Build-CmdContent $license_cmd_str))

# aggregate cmds
$aggr_create_cmds		= New-Object System.Collections.ArrayList
$aggr_create_brk_str 	= "Create data aggregates on each node"
$aggr_create_cmd_str 	= "storage aggregate show"
[Void]$aggr_create_cmds.Add((Build-CmdHeader $aggr_create_brk_str))
[Void]$aggr_create_cmds.Add((Build-CmdContent $aggr_create_cmd_str))

$aggr_no_snap_cmds		= New-Object System.Collections.ArrayList
$aggr_no_snap_brk_str	= "Disable aggregate snapshots on each node"
$aggr_no_snap_cmd_str	= "system node run -node * -command snap list -A;" + `
							"system node run -node * -command snap sched -A;"
[Void]$aggr_no_snap_cmds.Add((Build-CmdHeader $aggr_create_brk_str))
[Void]$aggr_no_snap_cmds.Add((Build-CmdContent $aggr_create_cmd_str))

# auto giveback cmds
$auto_giveback_cmds		= New-Object System.Collections.ArrayList
$auto_giveback_brk_str	= "Enable auto-giveback"
$auto_giveback_cmd_str	= "storage failover show -fields auto-giveback;" + `
							"storage failover show -fields auto-giveback-after-panic;" + `
							"storage failover modify -node * -auto-giveback true;" + `
							"storage failover modify -node * -auto-giveback-after-panic true;" + `
							"storage failover show -fields auto-giveback;" + `
							"storage failover show -fields auto-giveback-after-panic;"

[Void]$auto_giveback_cmds.Add((Build-CmdHeader $auto_giveback_brk_str))
[Void]$auto_giveback_cmds.Add((Build-CmdContent $auto_giveback_cmd_str))

if ($node_count -eq 2)
	{
	# failover ha cmds
	$two_node_failover_ha_verify_cmds		= New-Object System.Collections.ArrayList
	$two_node_failover_ha_verify_brk_str	= "Verify Storage Failover/Cluster HA are enabled in a 2-node cluster"
	$two_node_failover_ha_verify_cmd_str	= "set -privilege diagnostic;" + `
												"storage failover show;" + `
												"cluster ha show;" + `
												"## If the 'Takeover Possible' is 'false' issue the following, otherwise issue 'set -privilege admin' only;" + `
												"cluster ha modify -configured true;" + `
												"cluster ha show;" + `
												"storage failover show;" + `
												"set -privilege admin;"										
	[Void]$two_node_failover_ha_verify_cmds.Add((Build-CmdHeader $two_node_failover_ha_verify_brk_str))
	[Void]$two_node_failover_ha_verify_cmds.Add((Build-CmdContent $two_node_failover_ha_verify_cmd_str))
	
	$two_node_failover_ha_disable_enable_cmds		= New-Object System.Collections.ArrayList
	$two_node_failover_ha_disable_enable_brk_str	= "Disable/Enable Failover/Cluster HA on a 2-node cluster"
	$two_node_failover_ha_disable_enable_cmd_str	= "storage ha show;" + `
														"cluster ha show;" + `
														"cluster ha modify -configured false;" + `
														"storage failover modify -node * -enabled false;" + `
														"## Wait a couple minutes prior to issuing following command;" + `
														"cluster ha modify -configured true;" + `
														"storage ha show;" + `
														"cluster ha show;"

	[Void]$two_node_failover_ha_disable_enable_cmds.Add((Build-CmdHeader $two_node_failover_ha_disable_enable_brk_str))
	[Void]$two_node_failover_ha_disable_enable_cmds.Add((Build-CmdContent $two_node_failover_ha_disable_enable_cmd_str))
	}
	
$svm_create_cmds	= New-Object System.Collections.ArrayList
$svm_create_brk_str	= "Create storage virtual machine (SVM)"
[Void]$svm_create_cmds.Add((Build-CmdHeader $svm_create_brk_str))

$svm_lif_create_cmds		= New-Object System.Collections.ArrayList
$svm_lif_create_brk_str 	= "Create SVM logical networking interfaces (LIFs - data & mgmt_svm)"
[Void]$svm_lif_create_cmds.Add((Build-CmdHeader $svm_lif_create_brk_str))


$autosupport_cmds		= New-Object System.Collections.ArrayList
$autosupport_brk_str	= "Configure autosupport and send test message"
$autosupport_cmd_str	= "system autosupport show"
[Void]$autosupport_cmds.Add((Build-CmdHeader $autosupport_brk_str))
[Void]$autosupport_cmds.Add((Build-CmdContent $autosupport_cmd_str))

if ($fc_tgt_ports -is [System.Array]) 
	{
	$onboard_tgt_disable_cmds		= New-Object System.Collections.ArrayList
	$onboard_tgt_disable_brk_str	= "Disable onboard UTA2 / CNA / FC ports to prepare for port mode change"
	$onboard_tgt_disable_cmd_str	= "ucadmin show"
	[Void]$onboard_tgt_disable_cmds.Add((Build-CmdHeader $onboard_tgt_disable_brk_str))
	[Void]$onboard_tgt_disable_cmds.Add((Build-CmdContent $onboard_tgt_disable_cmd_str))

	$onboard_tgt_modify_cmds	= New-Object System.Collections.ArrayList
	$onboard_tgt_modify_brk_str	= "Convert onboard UTA2 / CNA / FC ports to target mode"
	$onboard_tgt_modify_cmd_str	= "ucadmin show"

	[Void]$onboard_tgt_modify_cmds.Add((Build-CmdHeader $onboard_tgt_modify_brk_str))
	[Void]$onboard_tgt_modify_cmds.Add((Build-CmdContent $onboard_tgt_modify_cmd_str))
	}

#endregion

#region Merged Data Cmds
#$header_summary3_cmds = New-Object System.Collections.ArrayList
#$header_summary3_brk_str = "Mgmt IP address summary for cluster setup for: $cluster_name"
#$header_summary3_cmds.Add((Build-CmdHeader $header_summary3_brk_str))
#$header_summary3_brk = $cdot_merged_data | select ip_address, lif_name, network_label, vlan_id, netmask, gateway | where { $_.network_label -match "mgmt" } | sort -Property ip_address | ft -AutoSize
#[Void]$header_summary3_cmds.Add($header_summary3_brk)

#$cdot_merged_data | select lif_role, lif_data_protocol, ten_gbe_ifgrp_name
#exit

$node_names 	= $cdot_merged_data | select node_name -Unique
$ten_gbe_vlans 	= $cdot_merged_data | where { $_.network_label -notmatch "mgmt_node|mgmt_sp|mgmt_cluster|mgmt_svm|access" } | select vlan_id -Unique | %{ $_.vlan_id }
$one_gbe_vlans 	= $cdot_merged_data | where { $_.network_label -match "mgmt_node|mgmt_sp|mgmt_cluster"  } | select vlan_id -Unique | %{ $_.vlan_id }

$mroot_aggrs = New-Object System.Collections.ArrayList

foreach ($item in $cdot_merged_data) 
	{		
	$network_label				= $item.network_label
	$node_def_name				= $item.node_def_name
	$node_number				= $item.node_number
	$node_name					= $item.node_name
	$node_name_sp				= $item.node_name_sp
	$node_aggr_mroot_def_name	= $item.node_aggr_mroot_def_name
	$node_aggr_mroot_name 		= $item.node_aggr_mroot_name
	$software_licenses_str 		= $item.software_licenses_str
	$ten_gbe_ifgrp_name 		= $item.ten_gbe_ifgrp_name
	$ten_gbe_ifgrp_mode 		= $item.ten_gbe_ifgrp_mode
	$ten_gbe_ifgrp_distr_func 	= $item.ten_gbe_ifgrp_distr_func
	$ten_gbe_ifgrp_ports_str 	= $item.ten_gbe_ifgrp_ports
	$one_gbe_ifgrp_name 		= $item.one_gbe_ifgrp_name
	$one_gbe_ifgrp_mode 		= $item.one_gbe_ifgrp_mode
	$one_gbe_ifgrp_distr_func 	= $item.one_gbe_ifgrp_distr_func
	$one_gbe_ifgrp_ports_str 	= $item.one_gbe_ifgrp_ports
	$one_gbe_mgmt_port			= $item.one_gbe_mgmt_port
	$ip_address					= $item.ip_address
	$ip_address_partner			= $item.ip_address_partner	
	$subnet						= $item.subnet
	$netmask					= $item.netmask
	$gateway					= $item.gateway
	$vlan_id					= $item.vlan_id
	$svm_name					= $item.svm_name
	$lif_name					= $item.lif_name
	$lif_role					= $item.lif_role
	$lif_data_protocol			= $item.lif_data_protocol
	$fg_name					= $item.fg_name
	$fg_port					= $item.fg_port
	$failover_policy 			= $item.failover_policy
	$firewall_policy 			= $item.firewall_policy
	$autosupport_email_from		= $item.autosupport_email_from

	if ($network_label -eq "mgmt_cluster")
		{
		$as_built_lifs_obj					= "" | select ip_address, lif_name, lif_network_label, vlan_id,  fg_port, fg_name
		
		$as_built_lifs_obj.lif_name			= $lif_name
		$as_built_lifs_obj.lif_network_label	= $network_label
		$as_built_lifs_obj.vlan_id			= $vlan_id
		$as_built_lifs_obj.ip_address		= $ip_address
		$as_built_lifs_obj.fg_port			= $fg_port
		$as_built_lifs_obj.fg_name			= $fg_name
		[Void]$as_built_lifs.Add($as_built_lifs_obj)
		
		$as_built_ifgrps_obj = "" | select node_name, node_phys_ports, ifgrp_name, vlan_ids
		$as_built_ifgrps_obj.node_name 			= $node_name
		$as_built_ifgrps_obj.node_phys_ports 	= $ten_gbe_ifgrp_ports_str
		$as_built_ifgrps_obj.ifgrp_name 		= $ten_gbe_ifgrp_name
		$as_built_ifgrps_obj.vlan_ids 			= $ten_gbe_vlans -join ","
		[Void]$as_built_ifgrps.Add($as_built_ifgrps_obj)
		[Void]$network_create_mgmt_cluster_fg_cmds.Add("network interface failover-groups create -failover-group $fg_name -node $node_name -port $fg_port")
		[Void]$network_rename_mgmt_lif_cmds.Add("network interface rename -vserver $cluster_name -lif cluster_mgmt -newname $lif_name")
		[Void]$network_modify_mgmt_lif_cmds.Add("network interface modify -vserver $cluster_name -lif $lif_name -home-node $node_name -home-port $fg_port -failover-group $fg_name -auto-revert true")
		}
	elseif ($network_label -eq "mgmt_node")
		{
		$as_built_lifs_obj					= "" | select ip_address, lif_name, lif_network_label, vlan_id,  fg_port, fg_name
		
		$as_built_lifs_obj.lif_name			= $lif_name
		$as_built_lifs_obj.lif_network_label	= $network_label
		$as_built_lifs_obj.vlan_id			= $vlan_id
		$as_built_lifs_obj.ip_address		= $ip_address
		$as_built_lifs_obj.fg_port			= $fg_port
		$as_built_lifs_obj.fg_name			= $fg_name
		[Void]$as_built_lifs.Add($as_built_lifs_obj)
		[Void]$node_rename_cmds.Add("system node rename $node_def_name -newname $node_name")
		[Void]$aggr_rename_cmds.Add("storage aggregate rename -aggregate $node_aggr_mroot_def_name -newname $node_aggr_mroot_name")

		$software_licenses	= $software_licenses_str.Split(";")
		foreach ($software_license in $software_licenses)
			{
			[Void]$license_cmds.Add("system license add -license-code $software_license")
			}
		foreach ($ntp_server in $ntp_server_names)
			{
			[Void]$ntp_cmds.Add("ntp server create -node $node_name -server $ntp_server -version max")
			}
		
		$cdp_cmd_str = "system node run -node $node_name -command options cdpd.enable on"
		[Void]$cdp_cmds.Add((Build-CmdContent $cdp_cmd_str))
		
		if ($autosupport_enable -eq "enable")
			{
			[Void]$autosupport_cmds.Add("system autosupport modify -node $node_name -state enable -mail-hosts $smtp_server_name -from $autosupport_email_from -support enable -transport $autosupport_transport -to $autosupport_email_to")
			}
		if ($global_cdot_node_model -ne $one_gbe_only_node_model_str)
			{
			[Void]$network_create_ifgrp_ten_gbe_cmds.Add("network port ifgrp create -node $node_name -ifgrp $ten_gbe_ifgrp_name -mode $ten_gbe_ifgrp_mode -distr-func $ten_gbe_ifgrp_distr_func")
				
			$ten_gbe_ifgrp_ports = $ten_gbe_ifgrp_ports_str.Split(";")
			
			foreach ($ten_gbe_ifgrp_port in $ten_gbe_ifgrp_ports)
				{
				[Void]$network_create_ifgrp_ten_gbe_cmds.Add("network port ifgrp add-port -node $node_name -ifgrp $ten_gbe_ifgrp_name -port $ten_gbe_ifgrp_port")
				}
			}
			
		[Void]$network_create_ifgrp_one_gbe_cmds.Add("network port ifgrp create -node $node_name -ifgrp $one_gbe_ifgrp_name -mode $one_gbe_ifgrp_mode -distr-func $one_gbe_ifgrp_distr_func")
		
		$one_gbe_ifgrp_ports = $one_gbe_ifgrp_ports_str.Split(";")
		
		foreach ($one_gbe_ifgrp_port in $one_gbe_ifgrp_ports)
			{
			[Void]$network_create_ifgrp_one_gbe_cmds.Add("network port ifgrp add-port -node $node_name -ifgrp $one_gbe_ifgrp_name -port $one_gbe_ifgrp_port")
			}
		
		$as_built_ifgrps_obj = "" | select node_name, node_phys_ports, ifgrp_name, vlan_ids
		$as_built_ifgrps_obj.node_name 			= $node_name
		$as_built_ifgrps_obj.node_phys_ports 	= $one_gbe_ifgrp_ports_str
		$as_built_ifgrps_obj.ifgrp_name 		= $one_gbe_ifgrp_name
		$as_built_ifgrps_obj.vlan_ids 			= $one_gbe_vlans -join ","
		[Void]$as_built_ifgrps.Add($as_built_ifgrps_obj)
		
		[Void]$network_create_mgmt_node_fg_cmds.Add("network interface failover-groups create -failover-group $fg_name -node $node_name -port $fg_port")
		[Void]$network_create_mgmt_node_fg_cmds.Add("network interface failover-groups create -failover-group $fg_name -node $node_name -port $one_gbe_mgmt_port")

		[Void]$hardware_assist_cmds.Add("storage failover modify -hwassist-partner-ip $ip_address_partner -node $node_name -enabled true")
		[Void]$network_rename_mgmt_lif_cmds.Add("network interface rename -vserver $node_name -lif mgmt1 -newname $lif_name")
		[Void]$network_modify_mgmt_lif_cmds.Add("network interface modify -vserver $node_name -lif $lif_name -home-node $node_name -home-port $fg_port -failover-group $fg_name -auto-revert true")
		
		
		[int]$aggr_counter = 0
		$cdot_data.get_Item("cdot_aggrs") | where { $_.node_number -eq $node_number } | %{		
			$aggr_action			= $_.action
			if ($aggr_action -ne "not_used")
				{
				$node_name_nodash		= ($cdot_merged_data | where { $_.node_number -eq $node_number } | sort -Unique | select node_name_nodash).node_name_nodash
				$aggr_flashpool_enable	= $_.flash_pool_enable
				$aggr_disk_size_str		= $_.disk_size_str
				$aggr_disk_type			= $_.disk_type
				$aggr_disk_num			= $_.disk_count
				$aggr_raid_size			= $_.raid_size
				$aggr_raid_type			= $_.raid_type

				$aggr_disk_size_gb 	= ($cdot_data.get_Item("cdot_disks") | where { $_.disk_size_str -eq $aggr_disk_size_str } | select disk_size_gb).disk_size_gb
				$aggr_disk_type_lc 	= $aggr_disk_type.ToLower()

				if ($aggr_action -eq "create")
					{		
					if ($aggr_flashpool_enable -eq "enable") 
						{
						$aggr_name = $node_name_nodash + "_" + $aggr_disk_size_str + "_" + $aggr_disk_type_lc + "_fp_aggr" + $aggr_counter
		
						[Void]$non_mroot_aggrs.Add($aggr_name)

						[Void]$aggr_create_cmds.Add("storage aggregate create -aggregate $aggr_name -node $node_name -diskcount $aggr_disk_num -disktype $aggr_disk_type -disksize $aggr_disk_size_gb -maxraidsize $aggr_raid_size -raidtype $aggr_raid_type")
						[Void]$aggr_create_cmds.Add("storage aggregate modify -aggregate $aggr_name -node $node_name -hybrid-enabled true")

						$aggr_no_snap_cmd_str = "system node run -node $node_name -command snap sched -A $aggr_name 0 0 0;" + `
												"system node run -node $node_name -command aggr options $aggr_name on;" + `
												"system node run -node $node_name -command snap delete -A -a -f $aggr_name;" + `
												"system node run -node $node_name -command snap reserve -A $aggr_name 0"
					[Void]$aggr_no_snap_cmds.Add((Build-CmdContent $aggr_no_snap_cmd_str))
						$aggr_counter++
						}
					elseif ($aggr_flashpool_enable -eq "disable")
						{
						$aggr_name = $node_name_nodash + "_" + $aggr_disk_size_str + "_" + $aggr_disk_type_lc + "_aggr" + $aggr_counter
						[Void]$non_mroot_aggrs.Add($aggr_name)
						[Void]$aggr_create_cmds.Add("storage aggregate create -aggregate $aggr_name -node $node_name -diskcount $aggr_disk_num -disktype $aggr_disk_type -disksize $aggr_disk_size_gb -maxraidsize $aggr_raid_size -raidtype $aggr_raid_type")

						$aggr_no_snap_cmd_str = "system node run -node $node_name -command snap sched -A $aggr_name 0 0 0;" + `
												"system node run -node $node_name -command aggr options $aggr_name nosnap on;" + `
												"system node run -node $node_name -command snap delete -A -a -f $aggr_name;" + `
												"system node run -node $node_name -command snap reserve -A $aggr_name 0"
						[Void]$aggr_no_snap_cmds.Add((Build-CmdContent $aggr_no_snap_cmd_str))
						}
					}
				elseif ($aggr_action -eq "add")
					{
					[Void]$aggr_create_cmds.Add("storage aggregate add -aggregate $aggr_name -diskcount $aggr_disk_num -disktype $aggr_disk_type -disksize $aggr_disk_size_gb -raidtype $aggr_raid_type")
					}
				$as_built_aggrs_obj = "" | select node_name, aggr_name
				$as_built_aggrs_obj.node_name 	= $node_name
				$as_built_aggrs_obj.aggr_name 	= $aggr_name
				[Void]$as_built_aggrs.Add($as_built_aggrs_obj)
				}
				$aggr_no_snap_cmd_str = "system node run -node $node_name -command snap sched -A $node_aggr_mroot_name 0 0 0;" + `
								"system node run -node $node_name -command aggr options $node_aggr_mroot_name nosnap on;" + `
								"system node run -node $node_name -command snap delete -A -a -f $node_aggr_mroot_name;" + `
								"system node run -node $node_name -command snap reserve -A $node_aggr_mroot_name 0"
				[Void]$aggr_no_snap_cmds.Add((Build-CmdContent $aggr_no_snap_cmd_str))
				[Void]$mroot_aggrs.Add($node_aggr_mroot_name)
			}
		}
	elseif ($network_label -eq "mgmt_svm")
		{
		$as_built_lifs_obj					= "" | select ip_address, lif_name, lif_network_label, vlan_id,  fg_port, fg_name		
		$as_built_lifs_obj.lif_name			= $lif_name
		$as_built_lifs_obj.lif_network_label	= $network_label
		$as_built_lifs_obj.vlan_id			= $vlan_id
		$as_built_lifs_obj.ip_address		= $ip_address
		$as_built_lifs_obj.fg_port			= $fg_port
		$as_built_lifs_obj.fg_name			= $fg_name
		[Void]$as_built_lifs.Add($as_built_lifs_obj)
		[Void]$svm_lif_create_cmds.Add("network interface create -vserver $svm_name -lif $lif_name -role $lif_role -data-protocol $lif_data_protocol -home-node $node_name -home-port $fg_port -address $ip_address -netmask $netmask -status-admin up -failover-policy $failover_policy -firewall-policy $firewall_policy -auto-revert true -failover-group $fg_name")
		}	
	elseif ($network_label -eq "intercluster")
		{
		$as_built_lifs_obj					= "" | select ip_address, lif_name, lif_network_label, vlan_id,  fg_port, fg_name		
		$as_built_lifs_obj.lif_name			= $lif_name
		$as_built_lifs_obj.lif_network_label	= $network_label
		$as_built_lifs_obj.vlan_id			= $vlan_id
		$as_built_lifs_obj.ip_address		= $ip_address
		$as_built_lifs_obj.fg_port			= $fg_port
		$as_built_lifs_obj.fg_name			= $fg_name
		[Void]$as_built_lifs.Add($as_built_lifs_obj)
		[Void]$network_create_ic_fg_cmds.Add("network interface failover-groups create -failover-group $fg_name -node $node_name -port $fg_port")
		[Void]$network_create_ic_routing_group_cmds.Add("network routing-groups route create -vserver $node_name -routing-group $node_routing_group_name -destination 0.0.0.0/0 -gateway $gateway")
		[Void]$network_create_ic_lif_cmds.Add("network interface create -vserver $node_name -lif $lif_name -role $lif_role -home-node $node_name -home-port $fg_port -address $ip_address -netmask $netmask -status-admin up -failover-policy $failover_policy -firewall-policy $firewall_policy -failover-group $fg_name")
		[Void]$ping_cmds.Add("network ping -lif-owner $node_name -lif $lif_name -destination $gateway")
		}
	elseif ($network_label -match "iscsi")
		{
		$as_built_lifs_obj					= "" | select ip_address, lif_name, lif_network_label, vlan_id,  fg_port, fg_name		
		$as_built_lifs_obj.lif_name			= $lif_name
		$as_built_lifs_obj.lif_network_label	= $network_label
		$as_built_lifs_obj.vlan_id			= $vlan_id
		$as_built_lifs_obj.ip_address		= $ip_address
		$as_built_lifs_obj.fg_port			= $fg_port
		$as_built_lifs_obj.fg_name			= $fg_name
		[Void]$as_built_lifs.Add($as_built_lifs_obj)
		[Void]$svm_lif_create_cmds.Add("network interface create -vserver $svm_name -lif $lif_name -role $lif_role -data-protocol $lif_data_protocol -home-node $node_name -home-port $fg_port -address $ip_address -netmask $netmask -status-admin up -firewall-policy $firewall_policy")
		if (!($svm_data_protocol_lookup.ContainsKey($network_label)))
			{
			$svm_data_protocol_lookup.Add($network_label, "")
			}
		}
	elseif ($network_label -notmatch "mgmt_sp")
		{
		$as_built_lifs_obj					= "" | select ip_address, lif_name, lif_network_label, vlan_id,  fg_port, fg_name
		$as_built_lifs_obj.lif_name			= $lif_name
		$as_built_lifs_obj.lif_network_label = $network_label
		$as_built_lifs_obj.vlan_id			= $vlan_id
		$as_built_lifs_obj.ip_address		= $ip_address
		$as_built_lifs_obj.fg_port			= $fg_port
		$as_built_lifs_obj.fg_name			= $fg_name
		[Void]$as_built_lifs.Add($as_built_lifs_obj)
		if ($node_number -eq 1)
			{
			foreach ($name in $node_names)
				{
				$node_name_str = $name.node_name
				[Void]$network_create_data_fg_cmds.Add("network interface failover-groups create -failover-group $fg_name -node $node_name_str -port $fg_port")
				}
			}
		[Void]$svm_lif_create_cmds.Add("network interface create -vserver $svm_name -lif $lif_name -role $lif_role -data-protocol $lif_data_protocol -home-node $node_name -home-port $fg_port -address $ip_address -netmask $netmask -status-admin up -failover-policy $failover_policy -firewall-policy $firewall_policy -auto-revert true -failover-group $fg_name")
		if (!($svm_data_protocol_lookup.ContainsKey($network_label)))
			{
			$svm_data_protocol_lookup.Add($network_label, "")
			}
		}						
	}
#$as_built_lifs | ft -AutoSize
#exit

[Void]$cdp_cmds.Add("")
[Void]$ping_cmds.Add("")

if ($global_cdot_node_model -ne $one_gbe_only_node_model_str)
	{
	$network_create_ifgrp_ten_gbe_cmd_str = "network port ifgrp show;"
	[Void]$network_create_ifgrp_ten_gbe_cmds.Add((Build-CmdContent $network_create_ifgrp_ten_gbe_cmd_str))
	}


$network_create_ifgrp_one_gbe_cmd_str = "network port ifgrp show;"
[Void]$network_create_ifgrp_one_gbe_cmds.Add((Build-CmdContent $network_create_ifgrp_one_gbe_cmd_str))

$node1_name		= ($cdot_merged_data | where { ($_.node_number -eq 1) -and ($_.network_label -eq "mgmt_node") } | select node_name).node_name
$mgmt_node1_ip 	= ($cdot_merged_data | where { ($_.node_number -eq 1) -and ($_.network_label -eq "mgmt_node") } | select ip_address).ip_address

$header_summary4_cmds		= New-Object System.Collections.ArrayList

$header_summary4_brk_str	= 	"Connect to first node: $node1_name ($mgmt_node1_ip);" + `
							"Node management interface via SSH to issue remaining;" + `
							"commands as console can have issues with line ends"
[Void]$header_summary4_cmds.Add((Build-CmdHeader $header_summary4_brk_str))

# VLAN CMDS

if ($one_gbe_ifgrp_ports -ne $one_gbe_ifgrp_ports)
	{
	$network_create_vlan_ten_gbe_cmds	  = New-Object System.Collections.ArrayList
	$network_create_vlan_ten_gbe_brk_str  = "Create VLANs on Data interface groups on each node"
	$network_create_vlan_ten_gbe_cmd_str  = "network port show"
	[Void]$network_create_vlan_ten_gbe_cmds.Add((Build-CmdHeader $network_create_vlan_ten_gbe_brk_str))
	[Void]$network_create_vlan_ten_gbe_cmds.Add((Build-CmdContent $network_create_vlan_ten_gbe_cmd_str))

	foreach ($node in $node_names)
		{
		$node_name = $node.node_name
		foreach ($ten_gbe_vlan_id in $ten_gbe_vlans)
			{
			if ($ten_gbe_vlan_id -ne "access")
				{
				$ten_gbe_ifgrp_name = ($cdot_merged_data | where { $_.vlan_id -eq $ten_gbe_vlan_id } | select ten_gbe_ifgrp_name -Unique).ten_gbe_ifgrp_name
				[Void]$network_create_vlan_ten_gbe_cmds.Add("network port vlan create -node $node_name -vlan-id $ten_gbe_vlan_id -port $ten_gbe_ifgrp_name")
				}
			}
		}
	$network_create_vlan_ten_gbe_cmd_str = "network port show;"
	[Void]$network_create_vlan_ten_gbe_cmds.Add((Build-CmdContent $network_create_vlan_ten_gbe_cmd_str))
	}

# network_create_vlan_one_gbe_cmds
$network_create_vlan_one_gbe_cmds	  = New-Object System.Collections.ArrayList
$network_create_vlan_one_gbe_brk_str  = "Create VLANs on Mgmt interface groups on each node"
$network_create_vlan_one_gbe_cmd_str  = "network port show"
[Void]$network_create_vlan_one_gbe_cmds.Add((Build-CmdHeader $network_create_vlan_one_gbe_brk_str))
[Void]$network_create_vlan_one_gbe_cmds.Add((Build-CmdContent $network_create_vlan_one_gbe_cmd_str))

foreach ($node in $node_names)
	{
	$node_name = $node.node_name
	foreach ($one_gbe_vlan_id in $one_gbe_vlans)
		{
		if ($one_gbe_vlan_id -ne "access")
			{
			$one_gbe_ifgrp_name = ($cdot_merged_data | where { $_.vlan_id -eq $one_gbe_vlan_id } | select one_gbe_ifgrp_name -Unique).one_gbe_ifgrp_name
			$network_create_vlan_one_gbe_cmd_str = "network port vlan create -node $node_name -vlan-id $one_gbe_vlan_id -port $one_gbe_ifgrp_name"
			[Void]$network_create_vlan_one_gbe_cmds.Add((Build-CmdContent $network_create_vlan_one_gbe_cmd_str))
			}
		}
	}

$network_create_vlan_one_gbe_cmd_str  = "network port show;"
[Void]$network_create_vlan_one_gbe_cmds.Add((Build-CmdContent $network_create_vlan_one_gbe_cmd_str))
	
# hwassist cmds

$node_attrs | %{
	[int]$node_number	= $_.node_number
	$node_name			= $_.node_name
	if ($node_number % 2 -ne 0)
		{
		$node_number_partner 		= ($node_number + 1)
		$mgmt_node_ip_node_partner	= ($node_attrs | where {  $_.node_number -eq $node_number_partner } | select mgmt_ip_node).mgmt_ip_node
		}
	else
		{
		$node_number_partner 		= ($node_number - 1)
		$mgmt_node_ip_node_partner	= ($node_attrs | where {  $_.node_number -eq $node_number_partner } | select mgmt_ip_node).mgmt_ip_node
		}
	$hardware_assist_cmd_str = "storage failover modify -hwassist-partner-ip $mgmt_node_ip_node_partner -node $node_name -enabled true"
	[Void]$hardware_assist_cmds.Add((Build-CmdContent $hardware_assist_cmd_str))
	}

#endregion

#region SVM

foreach ($svm_protocol in $all_svm_protocols)
	{
	if (!($svm_data_protocol_lookup.ContainsKey($svm_protocol)))
		{
		[Void]$svm_disallowed_protocols.Add($svm_protocol)
		}
	}

if ($svm_data_protocol_lookup.ContainsKey("nfs"))
	{
	$svm_nfs_enabled = $true
	}
else
	{
	$svm_nfs_enabled = $false
	}
if ($svm_data_protocol_lookup.ContainsKey("cifs"))
	{
	$svm_cifs_enabled = $true
	}
else
	{
	$svm_cifs_enabled = $false
	}
$non_mroot_aggr_list_str 	= $non_mroot_aggrs -join ","
$mroot_aggr_list_str 		= $mroot_aggrs -join ","
$svm_root_aggr_name = $non_mroot_aggrs[0]
$svm_disallowed_protocols_str = $svm_disallowed_protocols -join ","
$cdot_data.get_Item("cdot_svms")
$svm_root_vol_name = "vol_" + $svm_name

$cdot_data.get_Item("cdot_svms") | %{
	$svm_number						= $_.svm_number
	$svm_name						= ($cdot_merged_data | where { $_.network_label -eq "mgmt_svm" } | select svm_name).svm_name	
	$svm_ls_mir_sched_mins			= $_.ls_mir_sched_mins
	$svm_ls_mir_sched_name			= $_.ls_mir_sched_name
	$svm_network_attrs				= $cdot_merged_data | where { $_.network_label -eq "mgmt_cluster" }
	$svm_mgmt_lif_vlan_id_str		= $svm_network_attrs.vlan_id
	$mgmt_ip_cluster_subnet			= $svm_network_attrs.subnet
	$mgmt_ip_cluster_netmask		= $svm_network_attrs.netmask
	$mgmt_cluster_gateway			= $svm_network_attrs.gateway
	$mgmt_cluster_routing_group		= Get-RoutingGroup -role "cluster-mgmt" -subnet $mgmt_ip_cluster_subnet -netmask $mgmt_ip_cluster_netmask
	$mgmt_cluster_fg_vlan_port		= $ten_gbe_node_ifgrp + "-" + $svm_mgmt_lif_vlan_id_str

	$svm_create_cmd_str = "vserver create -vserver $svm_name -rootvolume $svm_root_vol_name -aggregate $svm_root_aggr_name -ns-switch file -rootvolume-security-style unix -language en_US -quota-policy default;" + `
							"vserver modify -vserver $svm_name -disallowed-protocols $svm_disallowed_protocols_str;"
	if (($svm_nfs_enabled) -or ($svm_cifs_enabled))
		{
		$svm_create_cmd_str = $svm_create_cmd_str + `
								"export-policy rule create -vserver $svm_name -policyname default -clientmatch 0.0.0.0/0 -rorule none -rwrule none -superuser none;" + `
								"vserver nfs create -vserver $svm_name -v3 enabled -v4.0 enabled;"
		}
	$svm_create_cmd_str = $svm_create_cmd_str + `
							"vserver show -vserver $svm_name;"
	[Void]$svm_create_cmds.Add((Build-CmdContent $svm_create_cmd_str))
	
	$network_create_cm_routing_group_cmd_str = "network routing-groups route create -vserver $svm_name -routing-group $mgmt_cluster_routing_group -gateway $mgmt_cluster_gateway -metric 20"
	[Void]$network_svm_create_routing_group_cmds.Add((Build-CmdContent $network_create_cm_routing_group_cmd_str))	
	}

#endregion

#region Footer

$aggr_rename_cmd_str = "aggregate show;"
[Void]$aggr_rename_cmds.Add((Build-CmdContent $aggr_rename_cmd_str))

$node_rename_cmd_str = "system node show;"
[Void]$node_rename_cmds.Add((Build-CmdContent $node_rename_cmd_str))

if ($fc_tgt_ports -is [System.Array])
	{
	$onboard_tgt_disable_cmd_str = "ucadmin show"
	[Void]$onboard_tgt_disable_cmds.Add((Build-CmdContent $onboard_tgt_disable_cmd_str))

	$onboard_tgt_modify_cmd_str = "ucadmin show"
	[Void]$onboard_tgt_modify_cmds.Add((Build-CmdContent $onboard_tgt_modify_cmd_str))
	}

$network_create_mgmt_node_fg_cmd_str = "network interface failover-groups show;" +
										"network interface failover show;"
[Void]$network_create_mgmt_node_fg_cmds.Add((Build-CmdContent $network_create_mgmt_node_fg_cmd_str))

$network_create_mgmt_cluster_fg_cmd_str = "network interface failover-groups show;" +
											"network interface failover show;"	
[Void]$network_create_mgmt_cluster_fg_cmds.Add((Build-CmdContent $network_create_mgmt_cluster_fg_cmd_str))

$network_create_data_fg_cmd_str = "network interface failover-groups show;" +
									"network interface failover show"
[Void]$network_create_data_fg_cmds.Add((Build-CmdContent $network_create_mgmt_cluster_fg_cmd_str))

if ($ic_enabled)
	{
	$network_create_ic_fg_cmd_str = "network interface failover-groups show;" + `
									"network interface failover show;"
	[Void]$network_create_ic_fg_cmds.Add((Build-CmdContent $network_create_ic_fg_cmd_str))
	
	$network_create_ic_routing_group_cmd_str = "network interface failover-groups show;" + `
												"network interface failover show;"
	[Void]$network_create_ic_routing_group_cmds.Add((Build-CmdContent $network_create_ic_fg_cmd_str))
	
	$network_create_ic_lif_cmd_str = "network interface show;"
	[Void]$network_create_ic_lif_cmds.Add((Build-CmdContent $network_create_ic_lif_cmd_str))
	}

$network_rename_mgmt_lif_cmd_str = "network interface show;"
[Void]$network_rename_mgmt_lif_cmds.Add((Build-CmdContent $network_rename_mgmt_lif_cmd_str))

$network_modify_mgmt_lif_cmd_str = "network interface show;" + `
									"network interface show -failover;"
[Void]$network_modify_mgmt_lif_cmds.Add((Build-CmdContent $network_modify_mgmt_lif_cmd_str))

$network_create_cm_routing_group_cmd_str = "network routing-groups route show;"
[Void]$network_svm_create_routing_group_cmds.Add((Build-CmdContent $network_create_cm_routing_group_cmd_str))

$svm_lif_create_cmd_str = "network interface show;"
[Void]$svm_lif_create_cmds.Add((Build-CmdContent $svm_lif_create_cmd_str))

$ntp_cmd_str = "set -privilege diagnostic;" + `
				"system services ntp config show;" + `
				"system services ntp config modify -enabled true;" + `
				"system services ntp config show;" + `
				"set -privilege admin;"				
[Void]$ntp_cmds.Add((Build-CmdContent $ntp_cmd_str))

$hardware_assist_cmd_str = "storage failover hwassist show;"
[Void]$hardware_assist_cmds.Add((Build-CmdContent $hardware_assist_cmd_str))

$aggr_create_cmd_str = "storage aggregate show;"
[Void]$aggr_create_cmds.Add((Build-CmdContent $aggr_create_cmd_str))

$aggr_no_snap_cmd_str = "system node run -node * -command snap list -A;" + `
						"system node run -node * -command snap sched -A;"					
[Void]$aggr_no_snap_cmds.Add((Build-CmdContent $aggr_no_snap_cmd_str))

$svm_remove_mroot_brk_str = "Remove mroot aggrs from SVM List"
$svm_remove_mroot_cmd_str = "vserver show -vserver $svm_name -fields aggr-list;" + `
							"vserver modify -vserver $svm_name -aggr-list $non_mroot_aggr_list_str;" + `
							"vserver show -vserver $svm_name -fields aggr-list;"
							
[Void]$svm_remove_mroot_cmds.Add((Build-CmdHeader $svm_remove_mroot_brk_str))							
[Void]$svm_remove_mroot_cmds.Add((Build-CmdContent $svm_remove_mroot_cmd_str))


$svm_ls_mir_counter = 1
$aggr_count = ($non_mroot_aggrs | measure).Count

if ($aggr_count -gt 1)
	{
	$svm_create_ls_mir_brk_str = "Create SVM root/loadshare mirror schedule"
	$svm_create_ls_mir_cmd_str = "schedule interval create -name $svm_ls_mir_sched_name -minutes $svm_ls_mir_sched_mins;"
	[Void]$svm_create_ls_mir_cmds.Add((Build-CmdHeader $svm_create_ls_mir_brk_str))
	[Void]$svm_create_ls_mir_cmds.Add((Build-CmdContent $svm_create_ls_mir_cmd_str))

	$svm_create_ls_mir_brk_str = "Create SVM root/loadshare mirrors"
	[Void]$svm_create_ls_mir_cmds.Add((Build-CmdHeader $svm_create_ls_mir_brk_str))

	$svm_snapmirror_ls_mir_cmd_str = " Create root/loadshare mirror snapmirror relationship"
	[Void]$svm_snapmirror_ls_mir_cmds.Add((Build-CmdHeader $svm_snapmirror_ls_mir_cmd_str))

#	$svm_ls_mir_counter_str		= $svm_ls_mir_counter.ToString("00")
#	$svm_ls_mir_name			= $svm_root_vol_name + "_ls" + $svm_ls_mir_counter_str
#	$svm_ls_mir_src				= $cluster_name + "://" + $svm_name + "/" + $svm_root_vol_name
#	$svm_ls_mir_dst				= $cluster_name + "://" + $svm_name + "/" + $svm_ls_mir_name
#	$svm_create_ls_mir_cmd_str 	= "volume create -vserver $svm_name -volume $svm_ls_mir_name -aggregate $non_mroot_aggrs -size 1GB -state online -type DP;"
#	[Void]$svm_create_ls_mir_cmds.Add((Build-CmdContent $svm_create_ls_mir_cmd_str))
#	$svm_snapmirror_ls_mir_cmd_str = "snapmirror create -source-path $svm_ls_mir_src -destination-path $svm_ls_mir_dst -type LS -tries 8 -vserver $svm_name -schedule $svm_ls_mir_sched_name;"
#	[Void]$svm_snapmirror_ls_mir_cmds.Add((Build-CmdContent $svm_snapmirror_ls_mir_cmd_str))			

	foreach ($non_mroot_aggr_line in $non_mroot_aggrs)
		{
		if ($non_mroot_aggr_line -ne $svm_root_aggr_name) 
			{
			$svm_ls_mir_counter_str		= $svm_ls_mir_counter.ToString("00")
			$svm_ls_mir_name			= $svm_root_vol_name + "_ls" + $svm_ls_mir_counter_str
			$svm_ls_mir_src				= $cluster_name + "://" + $svm_name + "/" + $svm_root_vol_name
			$svm_ls_mir_dst				= $cluster_name + "://" + $svm_name + "/" + $svm_ls_mir_name
			$svm_create_ls_mir_cmd_str 	= "volume create -vserver $svm_name -volume $svm_ls_mir_name -aggregate $non_mroot_aggr_line -size 1GB -state online -type DP;"
			[Void]$svm_create_ls_mir_cmds.Add((Build-CmdContent $svm_create_ls_mir_cmd_str))
			$svm_snapmirror_ls_mir_cmd_str = "snapmirror create -source-path $svm_ls_mir_src -destination-path $svm_ls_mir_dst -type LS -tries 8 -vserver $svm_name -schedule $svm_ls_mir_sched_name;"
			[Void]$svm_snapmirror_ls_mir_cmds.Add((Build-CmdContent $svm_snapmirror_ls_mir_cmd_str))
			$svm_ls_mir_counter++
			}
		}
		
	$svm_snapmirror_ls_mir_brk_str = "Start root/loadshare snapmirror"
	$svm_snapmirror_ls_mir_cmd_str = "snapmirror initialize-ls-set -source-path $svm_ls_mir_src -foreground true;" + `
										"snapmirror show -type LS;"
	[Void]$svm_snapmirror_ls_mir_cmds.Add((Build-CmdHeader $svm_snapmirror_ls_mir_brk_str))
	[Void]$svm_snapmirror_ls_mir_cmds.Add((Build-CmdContent $svm_snapmirror_ls_mir_cmd_str))
	}
	
$license_cmd_str = "system license show;"
[Void]$license_cmds.Add((Build-CmdContent $license_cmd_str))

if ($autosupport_enable -eq "enable")
	{
	$autosupport_cmd_str = "system autosupport invoke -node * -type test -message test;" + `
							"system autosupport show;"
	[Void]$autosupport_cmds.Add((Build-CmdContent $autosupport_cmd_str))
	}
else {
	$autosupport_cmd_str = "Autosupport not enabled, to enable, set variable 'cdot_autosupport_enable' equal to 'enable'"
	[Void]$autosupport_cmds.Add((Build-CmdContent $autosupport_cmd_str))
	}

$all_cmds = $header_summary1_cmds + `
			$system_initialize_cmds + `
			$header_summary2_cmds + `
			$header_summary4_cmds + `
			$term_rows_0_cmds + `
			$two_node_failover_ha_verify_cmds + `
			$node_rename_cmds + `
			$aggr_rename_cmds + `
			$onboard_tgt_disable_cmds + `
			$onboard_tgt_modify_cmds + `
			$reboot_all_cmds + `
			$ucadmin_show_cmds + `
			$network_create_ifgrp_ten_gbe_cmds + `
			$network_create_ifgrp_one_gbe_cmds + `
			$network_create_vlan_ten_gbe_cmds + `
			$network_create_vlan_one_gbe_cmds + `
			$network_create_mgmt_node_fg_cmds + `
			$network_create_mgmt_cluster_fg_cmds + `
			$network_create_ic_fg_cmds + `
			$network_create_ic_lif_cmds + `
			$network_create_ic_routing_group_cmds + `
			$network_create_data_fg_cmds + `
			$network_rename_mgmt_lif_cmds + `
			$network_modify_mgmt_lif_cmds + `
			$timezone_cmds + `
			$ntp_cmds + `
			$snmp_cmds + `
			$cdp_cmds + `
			$hardware_assist_cmds + `
			$network_interface_revert_cmds + `
			$ping_cmds + `
			$license_cmds + `
			$aggr_create_cmds + `
			$aggr_no_snap_cmds + `
			$auto_giveback_cmds + `
			$two_node_failover_ha_disable_enable_cmds + `
			$svm_create_cmds + `
			$network_svm_create_routing_group_cmds + `
			$svm_lif_create_cmds + `
			$svm_create_ls_mir_cmds + `
			$svm_snapmirror_ls_mir_cmds + `
			$svm_remove_mroot_cmds + `
			$create_mgmt_users_cmds + `
			$autosupport_cmds
			
Write-Host ""
Write-Host "$section_break"
Write-Host "# + Writing cdot_cmd_output_file:"
Write-Host "$section_break"
Write-Host ""
Write-Host -ForegroundColor Green "$cmd_output_file"
Write-Host ""

if ($build_cmds -eq "y")
	{
	$all_cmds | Out-File $cmd_output_file -Encoding ASCII
	}

#endregion

#region Presite/As-Built Documentation

$word_template_name = "cdot_as-built_template.docx"
$word_template_file = Join-Path -Path $cdot_templates_path -ChildPath $word_template_name

if ($build_docs -eq "y")
	{
	$word_rpt_name	= $customer_name_key + "_" + $cluster_name + "_as-built.docx"
	$word_rpt_file	= Join-Path -Path $cdot_reports_path -ChildPath $word_rpt_name
	}
else
	{
	Write-Host "No build_docs specified, exiting..."
	exit
	}

$tmpl_xml_file		= Join-Path -Path $cdot_templates_path "cdot_as-built_template_body_contents.xml"
[xml]$tmpl_xml_contents 	= Get-Content -Path $tmpl_xml_file

$tmpl_introduction_body = $tmpl_xml_contents.template.introduction
$tmpl_customer_body 	= $tmpl_xml_contents.template.customer
$tmpl_cluster_body 		= $tmpl_xml_contents.template.cluster
$tmpl_nodes_body 		= $tmpl_xml_contents.template.nodes
$tmpl_aggrs_body 		= $tmpl_xml_contents.template.aggrs
$tmpl_networks_body 	= $tmpl_xml_contents.template.networks
$tmpl_ifgrps_body 		= $tmpl_xml_contents.template.ifgrps
$tmpl_lifs_body 		= $tmpl_xml_contents.template.lifs

# Instantiate a new Word object

$word 				= New-Object -ComObject word.application
$word.visible 		= $true
$document 			= $word.documents.add($word_template_file)
$selection			= $word.Selection

SearchAWord $document "DOCUMENT NAME" "cDOT Cluster As-Built"
SearchAWord $document "CUSTOMER NAME" $customer_name
 
$MoveCursorToEnd = $selection.EndKey(6, 0) 
$selection.TypeParagraph() 
$selection.TypeParagraph() 

$selection.Style="Heading 1"
$selection.TypeText("cDOT Cluster As-Built")
$selection.TypeParagraph()

$selection.Style="Heading 2"
$selection.TypeText("Introduction")
$selection.TypeParagraph()
$selection.Style="Normal"
$selection.TypeText("$tmpl_introduction_body")
$selection.TypeParagraph()
$selection.TypeParagraph()

$selection.Style="Heading 2"
$selection.TypeText("Customer Information")
$selection.TypeParagraph()
$selection.Style="Normal"
$selection.TypeText("$tmpl_customer_body")
$selection.TypeParagraph()
$selection.TypeParagraph()
$selection.Style="Normal"
Create-AbWordTables "Customer" $as_built_customer

$selection.Style="Heading 2"
$selection.TypeText("Cluster")
$selection.TypeParagraph()
$selection.Style="Normal"
$selection.TypeText("$tmpl_cluster_body")
$selection.TypeParagraph()
$selection.TypeParagraph()
$selection.Style="Normal"
Create-AbWordTables "Cluster" $as_built_cluster

$selection.Style="Heading 2"
$selection.TypeText("Aggregates")
$selection.TypeParagraph()
$selection.Style="Normal"
$selection.TypeText("$tmpl_aggrs_body")
$selection.TypeParagraph()
$selection.TypeParagraph()
$selection.Style="Normal"
Create-AbWordTables "Aggregates" $as_built_aggrs

$selection.Style="Heading 2"
$selection.TypeText("Networks")
$selection.TypeParagraph()
$selection.Style="Normal"
$selection.TypeText("$tmpl_networks_body")
$selection.TypeParagraph()
$selection.TypeParagraph()
$selection.Style="Normal"
Create-AbWordTables "Networks" $as_built_networks

$selection.Style="Heading 2"
$selection.TypeText("Network Interface Groups (IFGRPs)")
$selection.TypeParagraph()
$selection.Style="Normal"
$selection.TypeText("$tmpl_ifgrps_body")
$selection.TypeParagraph()
$selection.TypeParagraph()
$selection.Style="Normal"
Create-AbWordTables "IFGRPs" $as_built_ifgrps

$selection.Style="Heading 2"
$selection.TypeText("Network Logical Interfaces (LIFs)")
$selection.TypeParagraph()
$selection.Style="Normal"
$selection.TypeText("$tmpl_lifs_body")
$selection.TypeParagraph()
$selection.TypeParagraph()
$selection.Style="Normal"
Create-AbWordTables "LIFs" $as_built_lifs

#$table_name = "cDOT Variable Glossary"
#Write-Host "Working on $table_name"
#$selection.ParagraphFormat.Alignment = 1
## Get table column headers
#$t_obj					= $cdot_data.get_Item("cdot_glossary").GetEnumerator() | %{ $_ } | select var_name, var_definition
#$table_hdrs				= $t_obj[0].psobject.properties | select Name | %{ $_.Name }
#$rows					= 1
#$columns				= $table_hdrs.Count
#$range 					= $selection.Range
#$table					= $document.Tables.add($range,$rows,$columns)
#$table.Borders.Enable	= $true	
#$selection.InsertCaption(-2, " $table_name") 

## Build table column headers
#$y = 1
#foreach ($table_hdr in $table_hdrs)
#	{
##	$Table.Cell(1,$y).Shading.BackgroundPatternColor = 0
#	$table.cell(1,$y).Borders.Enable	= $true
#	$table.cell(1,$y).Range.Font.Bold 	= $true
#	$table.cell(1,$y).Range.Text 	= $table_hdr
#	$y++
#	}	
#	
## Build table body
#$x = 2
#foreach ($table_row in $t_obj)
#	{
#	$y = 1
#	[Void]$table.rows.Add()
#	foreach ($table_hdr in $table_hdrs)
#		{
#		if ($table_row | select -ExpandProperty $table_hdr)
#			{
#			$cell_value = $table_row | select -ExpandProperty $table_hdr
#			$table.cell($x,$y).Range.Font.Bold 	= $false
#			$table.cell($x,$y).range.text 		= $cell_value
#			$y++
#			}
#		else
#			{
#			$cell_value = "not_specified"
#			$table.cell($x,$y).Range.Font.Bold 	= $false
#			$table.cell($x,$y).range.text 		= $cell_value
#			$y++				
#			}
#		}
#	$x++
#	}
$selection.EndKey(6)
$selection.TypeParagraph()
	
if (Test-Path -Path $word_rpt_file)
	{
	Remove-Item $word_rpt_file
	}
#save word document
$saveFormat = [Enum]::Parse([Microsoft.Office.Interop.Word.WdSaveFormat], "wdFormatDocumentDefault");
$document.SaveAs([ref] $word_rpt_file,[ref] 16)
$document.Close()
$word.quit()

#endregion

