Param(
  [Parameter(Mandatory=$True,Position=1)]
  [string]$datasource_name,
  [Parameter(Mandatory=$True,Position=2)]
  [string]$build_cmds,
  [Parameter(Mandatory=$True,Position=3)]
  [string]$build_docs,
  [Parameter(Mandatory=$True,Position=4)]
  [string]$build_id
  
)

$this_script = ($MyInvocation.MyCommand).Name

#region Header

$node_mgmt_ip_lookup				= @{}
$sp_mgmt_ip_lookup					= @{}
$svm_data_protocol_lookup			= @{}
$node_attrs							= New-Object System.Collections.ArrayList
$svm_disallowed_protocols			= New-Object System.Collections.ArrayList

$abt_build_id						= New-Object System.Collections.ArrayList
$abt_customer						= New-Object System.Collections.ArrayList
$abt_networks						= New-Object System.Collections.ArrayList
$abt_cluster						= New-Object System.Collections.ArrayList
$abt_nodes							= New-Object System.Collections.ArrayList
$abt_aggrs							= New-Object System.Collections.ArrayList
$abt_ifgrps							= New-Object System.Collections.ArrayList
$abt_svms							= New-Object System.Collections.ArrayList
$abt_lifs							= New-Object System.Collections.ArrayList

#$aggr_rename_cmds					= New-Object System.Collections.ArrayList
#$network_create_ifgrp_ten_gbe_cmds	= New-Object System.Collections.ArrayList
#$network_create_vlan_ten_gbe_cmds	= New-Object System.Collections.ArrayList
#$network_create_vlan_one_gbe_cmds	= New-Object System.Collections.ArrayList
#$network_create_mgmt_node_fg_cmds	= New-Object System.Collections.ArrayList
#$network_create_mgmt_cluster_fg_cmds = New-Object System.Collections.ArrayList
#$network_create_data_fg_cmds		= New-Object System.Collections.ArrayList
#$network_create_ic_fg_cmds			= New-Object System.Collections.ArrayList
#$network_rename_mgmt_lif_cmds		= New-Object System.Collections.ArrayList
#$network_modify_mgmt_lif_cmds		= New-Object System.Collections.ArrayList
#$network_create_ic_lif_cmds			= New-Object System.Collections.ArrayList
#$network_create_cm_routing_group_cmds = New-Object System.Collections.ArrayList
#$network_create_ic_routing_group_cmds = New-Object System.Collections.ArrayList
#$network_revert_interface_cmds		= New-Object System.Collections.ArrayList
#$cdp_cmds							= New-Object System.Collections.ArrayList
#$hardware_assist_cmds				= New-Object System.Collections.ArrayList
#$ping_cmds							= New-Object System.Collections.ArrayList
#$license_cmds						= New-Object System.Collections.ArrayList
#$aggr_create_cmds					= New-Object System.Collections.ArrayList
#$aggr_no_snap_cmds					= New-Object System.Collections.ArrayList
#$auto_giveback_cmds					= New-Object System.Collections.ArrayList
#$two_node_failover_ha_disable_enable_cmds	= New-Object System.Collections.ArrayList
#$two_node_failover_ha_verify_cmds	= New-Object System.Collections.ArrayList
#$svm_create_cmds					= New-Object System.Collections.ArrayList
#$autosupport_cmds					= New-Object System.Collections.ArrayList
#$onboard_tgt_disable_cmds 			= New-Object System.Collections.ArrayList
#$onboard_tgt_modify_cmds			= New-Object System.Collections.ArrayList
#$reboot_all_cmds					= New-Object System.Collections.ArrayList
#$ucadmin_show_cmds					= New-Object System.Collections.ArrayList
#$disable_flow_ctl_cmds				= New-Object System.Collections.ArrayList
#$network_create_ifgrp_ten_gbe_cmds	= New-Object System.Collections.ArrayList
#$ntp_cmds							= New-Object System.Collections.ArrayList
$non_mroot_aggrs					= New-Object System.Collections.ArrayList
$svm_remove_mroot_cmds				= New-Object System.Collections.ArrayList
$svm_snapmirror_ls_mir_cmds			= New-Object System.Collections.ArrayList
$svm_lif_create_cmds				= New-Object System.Collections.ArrayList
$svm_create_ls_mir_cmds				= New-Object System.Collections.ArrayList
$all_cmds							= @()

[int]$node_count		= $cdot_data.get_Item("cdot_nodes").Count
$customer_name_key		= ((($global_customer | select customer_name).customer_name).Replace(" ","")).ToLower()
$cluster_name			= $global_cdot_cluster_name
$cluster_name_nodash 	= $cluster_name.Replace("-","_")
$node_model				= $global_cdot_node_model

$datasource_basename	= Split-Path -Path $datasource_name -Leaf
$cmd_output_name 		= $customer_name_key + "_" + $cluster_name + "_cdot_build_cmds.txt"
$cmd_output_file 		= Join-Path -Path $cdot_cmds_path -ChildPath $cmd_output_name
$table_defs_name 		= "cdot_table_defs_" + $datasource_name + ".txt"
$table_defs_file 		= Join-Path -Path $cdot_reports_path -ChildPath $table_defs_name

# Synopsis:							Filter out "cdot_" network labels from $global_networks 
#									Trim "cdot_" prefix from network_label for use going forward
# Create an ArrayList: 				$cdot_networks
# of Custom Objects:				$cdot_networks_obj
# with Properties: 					build_id, network_label, vlan_id, subnet, ip_address_range
# from ArrayList of Custom Objects: $global_networks
# Unique key:						network_label
# Used by:							$cdot_ip_addresses ArrayList

#$cdot_networks 			= New-Object System.Collections.ArrayList
#$global_networks | where { $_.network_label -match "cdot_" } | %{
#	$cdot_networks_obj 			= "" | select build_id, network_label, vlan_id, subnet, gateway, ip_address_range
#	$cdot_networks_obj.build_id	= $_.build_id
#	$network_label_str  		= $_.network_label
#	if ($network_label_str -match "cdot_")
#		{
#		$cdot_networks_obj.network_label = $network_label_str.Substring(5)
#		}
#	else
#		{
#		$cdot_networks_obj.network_label = $network_label_str
#		}
#	$cdot_networks_obj.vlan_id     		= $_.vlan_id
#	$cdot_networks_obj.subnet    		= $_.subnet
#	$cdot_networks_obj.gateway    		= $_.gateway
#	$cdot_networks_obj.ip_address_range = $_.ip_address_range
#	[Void]$cdot_networks.Add($cdot_networks_obj)
#	}


#region Get global data
$global_customer | %{			
	$customer_name			= $_.customer_name
	$contact_name			= $_.contact_name
	$contact_email			= $_.contact_email
	$contact_phone			= $_.contact_phone
	}
$global_location | %{
	$customer_address		= $_.customer_address
	$system_install_address	= $_.system_install_address
	$rma_address			= $_.rma_address
	$rma_attn_to_name		= $_.rma_attn_to_name
	}
$global_datacenter | %{
	$domain_name			= $_.domain_name
	$dns_server_ips_str		= $_.dns_server_ips
	$ntp_server_names_str	= $_.ntp_server_names
	$smtp_server_name		= $_.smtp_server_name
	$snmp_server_name		= $_.snmp_server_name
	$snmp_community_string	= $_.snmp_community_string
	$timezone				= $_.timezone
	$default_admin_user		= $_.default_admin_user
	$default_admin_pass		= $_.default_admin_pass
	
	if ($dns_server_ips_str -match ",")
		{
		$dns_server_ips		= $dns_server_ips_str.Split(",")
		}
	else
		{
		$dns_server_ips		= @($dns_server_ips_str)
		}
	if ($ntp_server_names_str -match ",")
		{
		$ntp_server_names	= $ntp_server_names_str.Split(",")
		}
	else
		{
		$ntp_server_names	= @($ntp_server_names_str)
		}
	}
	
$abt_customer_obj = "" | select customer_attribute, customer_value
$abt_customer_obj.customer_attribute	= "contact_name"
$abt_customer_obj.customer_value	= $contact_name
[Void]$abt_customer.Add($abt_customer_obj)
$abt_customer_obj = "" | select customer_attribute, customer_value
$abt_customer_obj.customer_attribute	= "contact_email"
$abt_customer_obj.customer_value	= $contact_email
[Void]$abt_customer.Add($abt_customer_obj)
$abt_customer_obj = "" | select customer_attribute, customer_value
$abt_customer_obj.customer_attribute	= "contact_phone"
$abt_customer_obj.customer_value	= $contact_phone
[Void]$abt_customer.Add($abt_customer_obj)
$abt_customer_obj = "" | select customer_attribute, customer_value
$abt_customer_obj.customer_attribute	= "customer_address"
$abt_customer_obj.customer_value	= $customer_address
[Void]$abt_customer.Add($abt_customer_obj)
$abt_customer_obj = "" | select customer_attribute, customer_value
$abt_customer_obj.customer_attribute	= "system_install_address"
$abt_customer_obj.customer_value	= $system_install_address	
[Void]$abt_customer.Add($abt_customer_obj)
$abt_customer_obj = "" | select customer_attribute, customer_value
$abt_customer_obj.customer_attribute	= "rma_address"
$abt_customer_obj.customer_value	= $rma_address	
[Void]$abt_customer.Add($abt_customer_obj)
$abt_customer_obj = "" | select customer_attribute, customer_value
$abt_customer_obj.customer_attribute	= "rma_attn_to_name"
$abt_customer_obj.customer_value	= $rma_attn_to_name	
[Void]$abt_customer.Add($abt_customer_obj)
$abt_customer_obj = "" | select customer_attribute, customer_value
$abt_customer_obj.customer_attribute	= "domain_name"
$abt_customer_obj.customer_value	= $domain_name	
[Void]$abt_customer.Add($abt_customer_obj)
$abt_customer_obj = "" | select customer_attribute, customer_value
$abt_customer_obj.customer_attribute	= "dns_server_ips"
$abt_customer_obj.customer_value	= $dns_server_ips
[Void]$abt_customer.Add($abt_customer_obj)
$abt_customer_obj = "" | select customer_attribute, customer_value
$abt_customer_obj.customer_attribute	= "ntp_server_names"
$abt_customer_obj.customer_value	= $ntp_server_names	
[Void]$abt_customer.Add($abt_customer_obj)
$abt_customer_obj = "" | select customer_attribute, customer_value
$abt_customer_obj.customer_attribute	= "smtp_server_name"
$abt_customer_obj.customer_value	= $smtp_server_name	
[Void]$abt_customer.Add($abt_customer_obj)
$abt_customer_obj = "" | select customer_attribute, customer_value
$abt_customer_obj.customer_attribute	= "snmp_server_name"
$abt_customer_obj.customer_value	= $snmp_server_name	
[Void]$abt_customer.Add($abt_customer_obj)
$abt_customer_obj = "" | select customer_attribute, customer_value
$abt_customer_obj.customer_attribute	= "snmp_community_string"
$abt_customer_obj.customer_value	= $snmp_community_string
[Void]$abt_customer.Add($abt_customer_obj)
$abt_customer_obj = "" | select customer_attribute, customer_value
$abt_customer_obj.customer_attribute	= "timezone"
$abt_customer_obj.customer_value	= $timezone	
[Void]$abt_customer.Add($abt_customer_obj)
$abt_customer_obj = "" | select customer_attribute, customer_value
$abt_customer_obj.customer_attribute	= "default_admin_user"
$abt_customer_obj.customer_value	= $default_admin_user	
[Void]$abt_customer.Add($abt_customer_obj)
$abt_customer_obj = "" | select customer_attribute, customer_value
$abt_customer_obj.customer_attribute	= "default_admin_pass"
$abt_customer_obj.customer_value	= $default_admin_pass	
[Void]$abt_customer.Add($abt_customer_obj)

#endregion


# Synopsis:							Calcuate each LIF ip_address based on specified ip_address_range 
#									and calculate LIF node_id based on specified node_range
# Create an ArrayList: 				$cdot_ip_addresses
# of Custom Objects:				$cdot_ip_addresses_obj
# with Properties: 					ip_address, subnet, netmask, gateway, vlan_id, network_label, node_id
# from ArrayList of Custom Objects: $cdot_networks
# Unique key:						ip_address
# Called by:						$cdot_network_labels ArrayList

$cdot_ip_addresses = New-Object System.Collections.ArrayList
$global_networks | where { $_.network_label -match "cdot_" } | %{
	$network_label_str		= $_.network_label
	$network_label 			= ($network_label_str).Substring(5)
	$node_number_range		= (($cdot_data.get_Item("cdot_lifs") | where { $_.network_label -eq $network_label_str } | select node_number_range).node_number_range).Split("-")
	[int]$node_range_start	= $node_number_range[0]
	[int]$node_range_end	= $node_number_range[1]
	[int]$lif_counter		= 1
	$node_range_counter 	= $node_range_start
	$cdot_ip_addresses_obj 	= "" | select ip_address, subnet, netmask, gateway, vlan_id, network_label, node_number, lif_counter
	$cdot_ip_address_range	= $_.ip_address_range
	$subnet_octets 			= ($_.subnet).Split(".")
	if ($cdot_ip_address_range -match ",")
		{
		$last_octets = $cdot_ip_address_range.Split(",")
		foreach ($octet in $last_octets)
			{
			if ($node_range_counter -gt $node_range_end)
				{
				$node_range_counter = 1
				}
			$cdot_ip_addresses_obj = "" | select ip_address, subnet, netmask, gateway, vlan_id, network_label, node_number, lif_counter
			$subnet_octets[3] = $octet
			$ip_address_str = $subnet_octets -join "."
		
			$cdot_ip_addresses_obj.ip_address 		= $ip_address_str
			$cdot_ip_addresses_obj.subnet 			= $_.subnet
			$cdot_ip_addresses_obj.netmask 			= $_.netmask
			$cdot_ip_addresses_obj.gateway 			= $_.gateway
			$cdot_ip_addresses_obj.vlan_id 			= $_.vlan_id
			$cdot_ip_addresses_obj.network_label	= $network_label
			$cdot_ip_addresses_obj.node_number		= $node_range_counter
			$cdot_ip_addresses_obj.lif_counter		= $lif_counter
		
			[Void]$cdot_ip_addresses.Add($cdot_ip_addresses_obj)
			$node_range_counter++
			$lif_counter++
			}
		}
	elseif ($cdot_ip_address_range -match "-")
		{
		
		[int]$ip_first 		= $cdot_ip_address_range.Split("-")[0]
		[int]$ip_last 		= $cdot_ip_address_range.Split("-")[1]
		$ip_counter 		= $ip_first
		$last_octets_arr 	= New-Object System.Collections.ArrayList
		while ($ip_counter -le $ip_last)
			{
			if ($node_range_counter -gt $node_range_end)
				{
				$node_range_counter = 1
				}
			$cdot_ip_addresses_obj = "" | select ip_address, subnet, netmask, gateway, vlan_id, network_label, node_number, lif_counter
			$subnet_octets[3] 	= $ip_counter
			$ip_address_str 	= $subnet_octets -join "."

			$cdot_ip_addresses_obj.ip_address 		= $ip_address_str
			$cdot_ip_addresses_obj.subnet 			= $_.subnet
			$cdot_ip_addresses_obj.netmask 			= $_.netmask
			$cdot_ip_addresses_obj.gateway 			= $_.gateway
			$cdot_ip_addresses_obj.vlan_id 			= $_.vlan_id
			$cdot_ip_addresses_obj.network_label	= $network_label
			$cdot_ip_addresses_obj.node_number		= $node_range_counter
			$cdot_ip_addresses_obj.lif_counter		= $lif_counter
			
			[Void]$cdot_ip_addresses.Add($cdot_ip_addresses_obj)
			$node_range_counter++
			$lif_counter++
			$ip_counter++
			}
		}
	elseif ($cdot_ip_address_range -ne "")
		{
		$cdot_ip_addresses_obj = "" | select ip_address, subnet, netmask, gateway, vlan_id, network_label, node_number, lif_counter
		$subnet_octets[3] 	= $cdot_ip_address_range
		$ip_address_str 	= $subnet_octets -join "."

		$cdot_ip_addresses_obj.ip_address 		= $ip_address_str
		$cdot_ip_addresses_obj.subnet 			= $_.subnet
		$cdot_ip_addresses_obj.netmask 			= $_.netmask
		$cdot_ip_addresses_obj.gateway 			= $_.gateway
		$cdot_ip_addresses_obj.vlan_id 			= $_.vlan_id
		$cdot_ip_addresses_obj.network_label	= $network_label
		$cdot_ip_addresses_obj.node_number		= 1
		$cdot_ip_addresses_obj.lif_counter		= $lif_counter
		[Void]$cdot_ip_addresses.Add($cdot_ip_addresses_obj)
		}
	}

# Synopsis:							Join cdot_ip_addresses, $cdot_data.cdot_lifs and $cdot_data.cdot_lif_attrs 
#									to collect all Properties required to build LIFs for each IP address
# Create an ArrayList: 				$cdot_network_labels
# of Custom Objects:				$cdot_network_labels_obj
# with Properties: 					ip_address, network_label, vlan_id, subnet, netmask, gateway, node_number, svm_number, svm_name, 
#									network_role, network_data_protocol, firewall_policy, failover_policy, node_range_start, node_range_end
# from ArrayList of Custom Objects: $cdot_ip_addresses
# that has Properties:				node_number, vlan_id, network_label, ip_address
# Unique Key(s):					ip_address
# Called by:						Used to build LIF Create commands

#$cdot_data.get_Item("cdot_lifs") | select network_label, svm_number
#exit

$cdot_merged_data 	= New-Object System.Collections.ArrayList
$cdot_ip_addresses | %{
	$cdot_merged_data_obj 						= "" | select ip_address, network_label, vlan_id, subnet, netmask, gateway, ip_address_partner, `
																node_number, node_def_name, node_aggr_mroot_def_name, node_aggr_mroot_name, node_name, node_name_nodash, software_licenses_str, `
																ten_gbe_ifgrp_name, ten_gbe_ifgrp_ports, ten_gbe_ifgrp_mode, ten_gbe_ifgrp_distr_func, one_gbe_ifgrp_name, one_gbe_ifgrp_ports, one_gbe_ifgrp_mode, one_gbe_ifgrp_distr_func, one_gbe_mgmt_port, `
																svm_number, svm_name, svm_name_nodash, `
																lif_role, lif_data_protocol, firewall_policy, failover_policy, lif_name, fg_name, fg_port, `
																autosupport_email_from
											
	$cdot_merged_data_obj.ip_address			= $_.ip_address
	$network_label 								= $_.network_label
	$network_label_str							= "cdot_" + $network_label 
	$cdot_merged_data_obj.network_label 		= $network_label
	$vlan_id									= $_.vlan_id
	$cdot_merged_data_obj.vlan_id     			= $vlan_id
	$cdot_merged_data_obj.subnet    			= $_.subnet
	$cdot_merged_data_obj.netmask     			= $_.netmask
	$cdot_merged_data_obj.gateway 				= $_.gateway
	$node_number								= $_.node_number

	if ($node_number % 2 -ne 0)
		{
		$node_number_partner 						= ($node_number + 1)
		$cdot_merged_data_obj.ip_address_partner 	= ($cdot_ip_addresses | where { ($_.network_label -eq "mgmt_node")-and ($_.node_number -eq $node_number_partner) } | select ip_address).ip_address
		}
	else
		{
		$node_number_partner 						= ($node_number - 1)
		$cdot_merged_data_obj.ip_address_partner 	= ($cdot_ip_addresses | where { ($_.network_label -eq "mgmt_node")-and ($_.node_number -eq $node_number_partner) } | select ip_address).ip_address
		}
		
	$cdot_merged_data_obj.node_number 			= $node_number
	$cdot_merged_data_obj.node_def_name			= $cluster_name + "-" + $node_number.ToString("00")
	$node_name_str 								= ($cdot_data.get_Item("cdot_nodes") | where { $_.node_number -eq $node_number } | select node_name).node_name
	$node_def_info  							= Get-NodeDefInfo -cluster_name $cluster_name -node_number $node_number
	if ($node_name_str -ne "use_default")
		{
		$node_name								= $node_name_str
		}
	else
		{
		$node_name								= $node_def_info[0]
		}

	if ($network_label -ne "mgmt_sp")
		{
		$node_name_nodash						= ($node_name).Replace("-", "_")
		}
	else
		{
		
		$node_name_nodash						= $node_name_str + "-sp"
		}

	$cdot_merged_data_obj.node_name 			= $node_name
	$cdot_merged_data_obj.node_name_nodash 		= $node_name_nodash
	$cdot_merged_data_obj.node_aggr_mroot_def_name 	= $node_def_info[1]
	$cdot_merged_data_obj.node_aggr_mroot_name		= $node_name_nodash + "_mroot"	
	$cdot_merged_data_obj.software_licenses_str = ($cdot_data.get_Item("cdot_nodes") | where { $_.node_number -eq $node_number } | select software_licenses).software_licenses
	$ten_gbe_ifgrp_name_str						= ($cdot_data.get_Item("cdot_controllers") | where { $_.node_model -eq $global_cdot_node_model} | select ten_gbe_ifgrp_name).ten_gbe_ifgrp_name
	$cdot_merged_data_obj.ten_gbe_ifgrp_ports	= ($cdot_data.get_Item("cdot_controllers") | where { $_.node_model -eq $global_cdot_node_model} | select ten_gbe_ifgrp_ports).ten_gbe_ifgrp_ports
	$cdot_merged_data_obj.ten_gbe_ifgrp_mode	= ($cdot_data.get_Item("cdot_ifgrps") | where { $_.build_id -eq $global_build_id } | select ten_gbe_ifgrp_mode).ten_gbe_ifgrp_mode
	$cdot_merged_data_obj.ten_gbe_ifgrp_distr_func = ($cdot_data.get_Item("cdot_ifgrps") | where { $_.build_id -eq $global_build_id } | select ten_gbe_ifgrp_distr_func).ten_gbe_ifgrp_distr_func
	$one_gbe_ifgrp_name_str						= ($cdot_data.get_Item("cdot_controllers") | where { $_.node_model -eq $global_cdot_node_model} | select one_gbe_ifgrp_name).one_gbe_ifgrp_name
	$cdot_merged_data_obj.one_gbe_ifgrp_ports	= ($cdot_data.get_Item("cdot_controllers") | where { $_.node_model -eq $global_cdot_node_model} | select one_gbe_ifgrp_ports).one_gbe_ifgrp_ports
	$cdot_merged_data_obj.one_gbe_ifgrp_mode	= ($cdot_data.get_Item("cdot_ifgrps") | where { $_.build_id -eq $global_build_id } | select one_gbe_ifgrp_mode).one_gbe_ifgrp_mode
	$cdot_merged_data_obj.one_gbe_ifgrp_distr_func = ($cdot_data.get_Item("cdot_ifgrps") | where { $_.build_id -eq $global_build_id } | select one_gbe_ifgrp_distr_func).one_gbe_ifgrp_distr_func
	$cdot_merged_data_obj.one_gbe_mgmt_port		= ($cdot_data.get_Item("cdot_controllers") | where { $_.node_model -eq $global_cdot_node_model } | select one_gbe_mgmt_port).one_gbe_mgmt_port
	$svm_number									= ($cdot_data.get_Item("cdot_lifs") | where { $_.network_label -eq $network_label_str } | select svm_number).svm_number
	$cdot_merged_data_obj.svm_number			= $svm_number
	$svm_name									= $cluster_name + "_svm" + $svm_number
	$cdot_merged_data_obj.svm_name				= $svm_name
	$svm_name_nodash							= ($svm_name).Replace("-", "_")
	$cdot_merged_data_obj.svm_name_nodash		= $svm_name_nodash
	$cdot_merged_data_obj.lif_role				= ($cdot_data.get_Item("cdot_lif_attrs") | where { $_.network_label -eq $network_label } | select lif_role).lif_role
	$cdot_merged_data_obj.lif_data_protocol		= ($cdot_data.get_Item("cdot_lif_attrs") | where { $_.network_label -eq $network_label } | select lif_data_protocol).lif_data_protocol
	$cdot_merged_data_obj.firewall_policy		= ($cdot_data.get_Item("cdot_lif_attrs") | where { $_.network_label -eq $network_label } | select firewall_policy).firewall_policy
	$cdot_merged_data_obj.failover_policy		= ($cdot_data.get_Item("cdot_lif_attrs") | where { $_.network_label -eq $network_label } | select failover_policy).failover_policy

	if ($vlan_id -ne "access")
		{
		$ten_gbe_ifgrp_name = $ten_gbe_ifgrp_name_str# + "-" + $vlan_id
		$one_gbe_ifgrp_name = $one_gbe_ifgrp_name_str# + "-" + $vlan_id
		$ten_gbe_ifgrp_vlan_port = $ten_gbe_ifgrp_name_str + "-" + $vlan_id
		$one_gbe_ifgrp_vlan_port = $one_gbe_ifgrp_name_str + "-" + $vlan_id
		$label_str 	= $network_label + "_" + $vlan_id + "_" 	
		if ($network_label -notmatch "iscsi")
			{
			if ($network_label -eq "intercluster")
				{
				$lif_name 	= $node_name_nodash + "_" + $label_str + "lif"
				$fg_name	= $node_name_nodash + "_" + $label_str + "fg"
				$fg_port	= $ten_gbe_ifgrp_vlan_port
				}			
			elseif ($network_label -eq "mgmt_svm")
				{
				$lif_name  	= $svm_name_nodash + "_" + $label_str + "lif"
				$fg_name	= $cluster_name_nodash + "_mgmt_cluster_" + $vlan_id + "_fg"
				$fg_port	= $one_gbe_ifgrp_vlan_port
				}
			elseif ($network_label -eq "mgmt_cluster")
				{
				$lif_name 	= $cluster_name_nodash + "_" + $label_str + "lif"
				$fg_name	= $cluster_name_nodash + "_" + $label_str + "fg"
				$fg_port	= $one_gbe_ifgrp_vlan_port
				}
			elseif ($network_label -eq "mgmt_node")
				{
				$lif_name 	= $node_name_nodash + "_" + $label_str + "lif"
				$fg_name	= $node_name_nodash + "_" + $label_str + "fg"
				$fg_port	= $one_gbe_ifgrp_vlan_port
				}
			elseif ($network_label -ne "mgmt_sp")
				{
				$lif_name  	= $label_str + "lif" + $lif_number
				$fg_name 	= $label_str + "fg"
				$fg_port	= $ten_gbe_ifgrp_vlan_port
				}
			else
				{
				$lif_name  	= "not_used"
				$fg_name 	= "not_used"
				$fg_port	= "not_used"				
				}
			}
		elseif ($network_label -match "iscsi")
			{
			$lif_name  		= $label_str + "lif" + $lif_number
			$fg_name		= $label_str + "fg"
			$fg_port		= $ten_gbe_ifgrp_name
			}
		}
	else
		{
		$ten_gbe_ifgrp_name = $ten_gbe_ifgrp_name_str
		$one_gbe_ifgrp_name = $one_gbe_ifgrp_name_str
		$label_str 			= $network_label + "_"
		if ($network_label -notmatch "iscsi")
			{
			if ($network_label -eq "intercluster")
				{
				$lif_name 	= $node_name_nodash + "_" + $label_str + "lif"
				$fg_name	= $node_name_nodash + "_" + $label_str + "fg"
				$fg_port	= $ten_gbe_ifgrp_name
				}			
			elseif ($network_label -eq "mgmt_svm")
				{
				$lif_name  	= $svm_name_nodash + "_" + $label_str + "lif"
				$fg_name	= $cluster_name_nodash + "_mgmt_cluster_" + "fg"
				$fg_port	= $one_gbe_ifgrp_name
				}
			elseif ($network_label -eq "mgmt_cluster")
				{
				$lif_name 	= $cluster_name_nodash + "_" + $label_str + "lif"
				$fg_name	= $cluster_name_nodash + "_" + $label_str + "fg"
				$fg_port	= $one_gbe_ifgrp_name
				}
			elseif ($network_label -eq "mgmt_node")
				{
				$lif_name 	= $node_name_nodash + "_" + $label_str + "lif"
				$fg_name	= $node_name_nodash + "_" + $label_str + "fg"
				$fg_port	= $one_gbe_ifgrp_name
				}
			elseif ($network_label -ne "mgmt_sp")
				{
				$lif_name  	= $label_str + "lif" + $lif_number
				$fg_name 	= $label_str + "fg"
				$fg_port	= $ten_gbe_ifgrp_name
				}
			else
				{
				$lif_name  	= "not_used"
				$fg_name 	= "not_used"
				$fg_port	= "not_used"				
				}
			}
		elseif ($network_label -match "iscsi")
			{
			$lif_name  		= $label_str + "lif" + $lif_number
			$fg_name		= $label_str + "fg"
			$fg_port		= $ten_gbe_ifgrp_name
			}
	}

	$cdot_merged_data_obj.lif_name				= $lif_name
	$cdot_merged_data_obj.fg_name				= $fg_name
	$cdot_merged_data_obj.fg_port				= $fg_port
	$cdot_merged_data_obj.ten_gbe_ifgrp_name	= $ten_gbe_ifgrp_name
	$cdot_merged_data_obj.one_gbe_ifgrp_name	= $one_gbe_ifgrp_name
	$cdot_merged_data_obj.autosupport_email_from = $node_name + "@" + $domain_name
	[Void]$cdot_merged_data.Add($cdot_merged_data_obj)
	}

#$cdot_merged_data | select lif_name, svm_name, svm_number  | ft -Auto #Export-Csv -Path ".\cdot_merged_data.csv" -NoTypeInformation
#exit

Write-Host ""
Write-Host $section_break
Write-Host -ForegroundColor Cyan "Using the following parameters:"
Write-Host -ForegroundColor Cyan "Build ID:   $build_id"
Write-Host -ForegroundColor Cyan "Customer:   $customer_name_key"
Write-Host -ForegroundColor Cyan "Cluster:    $cluster_name"
Write-Host -ForegroundColor Cyan "Controller: $node_model"
Write-Host $section_break
Write-Host ""

$networks_vlans = $cdot_ip_addresses | select vlan_id | %{ $_.vlan_id } | sort -Unique
$ic_enabled 	= $cdot_merged_data | where { $_.network_label -eq "intercluster" }
$lifs_vlans 	= $cdot_merged_data | select vlan_id | %{ $_.vlan_id } | sort -Unique
$vlan_chk 		= compare $networks_vlans $lifs_vlans

if ($vlan_chk)
	{
	Write-Host ""
	Write-Host -ForegroundColor Yellow $section_break
	Write-Host -ForegroundColor Yellow "### Network configuration information inconsistency detected"
	Write-Host -ForegroundColor Yellow "###"

	$vlan_chk | %{
		$diff_vlan_id 	= $_.InputObject
		$side_indicator	= $_.SideIndicator
		if ($side_indicator -eq "<=")
			{
			Write-Host -ForegroundColor Yellow "### $excel_workbook_name`:t_networks contains vlan_id $diff_vlan_id not found in $excel_workbook_name`:t_lifs"
			Write-Host -ForegroundColor Yellow "### Please validate and update configuration, t_networks cannot reference a vlan_id that doesn't exist in t_lifs"
			Write-Host -ForegroundColor Yellow "### Exiting..."
			Write-Host -ForegroundColor Yellow "###"
			Write-Host -ForegroundColor Yellow $section_break
			exit
			}
		elseif ($side_indicator -eq "=>")
			{
			Write-Host -ForegroundColor Yellow "### $excel_workbook_name`:t_lifs contains vlan_id $diff_vlan_id not found in $excel_workbook_name`:t_networks"
			Write-Host -ForegroundColor Yellow "### Please validate and update configuration, t_lifs cannot reference a vlan_id that doesn't exist in t_networks"
			Write-Host -ForegroundColor Yellow "### Exiting..."
			Write-Host -ForegroundColor Yellow "###"
			Write-Host -ForegroundColor Yellow $section_break
			exit
			}
		}
		
	}


Get-TableDefs -datasource_name $datasource_name
#region Customer

#$cdot_data.get_Item("cdot_controllers") | where { $_.node_model -eq $node_model } | %{
#	$fc_tgt_ports_chk			= $_.fc_tgt_ports
#	$non_fcoe_ten_gbe_ports		= $_.non_fcoe_ten_gbe_ports
#	$ten_gbe_ifgrp_name			= $_.ten_gbe_ifgrp_name
#	$one_gbe_ifgrp_name			= $_.one_gbe_ifgrp_name
#	$ten_gbe_ifgrp_ports_chk 	= $_.ten_gbe_ifgrp_ports
#	$one_gbe_ifgrp_ports_chk 	= $_.one_gbe_ifgrp_ports
#	$ten_gbe_ifgrp_mode			= ($cdot_data.get_Item("cdot_ifgrps") | where { $_.ten_gbe_ifgrp_name -eq $ten_gbe_ifgrp_name } | select ten_gbe_ifgrp_mode).ten_gbe_ifgrp_mode
#	$one_gbe_ifgrp_mode			= ($cdot_data.get_Item("cdot_ifgrps") | where { $_.one_gbe_ifgrp_name -eq $one_gbe_ifgrp_name } | select one_gbe_ifgrp_mode).one_gbe_ifgrp_mode
#	$ten_gbe_ifgrp_distr_func	= ($cdot_data.get_Item("cdot_ifgrps") | where { $_.ten_gbe_ifgrp_name -eq $ten_gbe_ifgrp_name } | select ten_gbe_ifgrp_distr_func).ten_gbe_ifgrp_distr_func
#	$one_gbe_ifgrp_distr_func	= ($cdot_data.get_Item("cdot_ifgrps") | where { $_.one_gbe_ifgrp_name -eq $one_gbe_ifgrp_name } | select one_gbe_ifgrp_distr_func).one_gbe_ifgrp_distr_func
#	$one_gbe_mgmt_port			= $_.one_gbe_mgmt_port
#
#	if ($fc_tgt_ports_chk -ne "not_used")
#		{
#		$fc_tgt_ports			= $fc_tgt_ports_chk.Split(";")
#		}
#	else
#		{
#		$fc_tgt_ports			= $fc_tgt_ports_chk
#		}
#		
#	if ($ten_gbe_ifgrp_ports_chk  -ne "not_used")
#		{
#		$ten_gbe_ifgrp_ports	= $ten_gbe_ifgrp_ports_chk.Split(";")
#		}
#	else
#		{
#		$ten_gbe_ifgrp_ports	= $ten_gbe_ifgrp_ports_chk
#		}
#		
#	if ($one_gbe_ifgrp_ports_chk  -ne "not_used")
#		{
#		$one_gbe_ifgrp_ports	= $one_gbe_ifgrp_ports_chk.Split(";")
#		}
#	else
#		{
#		$one_gbe_ifgrp_ports	= $one_gbe_ifgrp_ports_chk
#		}
#	}

#region Cluster


#$cdot_networks | %{
#	$abt_networks_obj = "" | select network_label, vlan_id, subnet, netmask, gateway
#	$abt_networks_obj.network_label	= $_.network_label
#	$abt_networks_obj.vlan_id		= $_.vlan_id
#	$abt_networks_obj.subnet		= $_.subnet
#	$abt_networks_obj.netmask		= $_.netmask
#	$abt_networks_obj.gateway		= $_.gateway
#	[Void]$abt_networks.Add($abt_networks_obj)
#	}


$cdot_data.get_Item("cdot_clusters") | %{			
	$base_license			= $_.base_license
	$autosupport_enable		= $_.autosupport_enable
	$autosupport_transport	= $_.autosupport_transport
	$autosupport_email_to	= $_.autosupport_email_to
	$ocum_user				= $_.ocum_user
	$ocopm_user				= $_.ocopm_user
	#$mgmt_ip_cluster = Get-MgmtIPs -network_label "mgmt_cluster"
	$mgmt_ip_cluster 		= $cdot_ip_addresses | where { ($_.network_label -eq "mgmt_cluster") } | select ip_address | %{ $_.ip_address }
	# Build as-built cluster object	
	$abt_cluster_obj = "" | select cluster_attribute, cluster_value
	$abt_cluster_obj.cluster_attribute 	= "cluster_name"
	$abt_cluster_obj.cluster_value 		= $cluster_name
	[Void]$abt_cluster.Add($abt_cluster_obj)
	$abt_cluster_obj = "" | select cluster_attribute, cluster_value
	$abt_cluster_obj.cluster_attribute 	= "base_license"
	$abt_cluster_obj.cluster_value 		= $_.base_license
	[Void]$abt_cluster.Add($abt_cluster_obj)
	$abt_cluster_obj = "" | select cluster_attribute, cluster_value
	$abt_cluster_obj.cluster_attribute 	= "autosupport_enable"
	$abt_cluster_obj.cluster_value 		= $_.autosupport_enable
	[Void]$abt_cluster.Add($abt_cluster_obj)
	$abt_cluster_obj = "" | select cluster_attribute, cluster_value
	$abt_cluster_obj.cluster_attribute 	= "autosupport_transport"
	$abt_cluster_obj.cluster_value 		= $_.autosupport_transport
	[Void]$abt_cluster.Add($abt_cluster_obj)
	$abt_cluster_obj = "" | select cluster_attribute, cluster_value
	$abt_cluster_obj.cluster_attribute 	= "autosupport_email_to"
	$abt_cluster_obj.cluster_value 		= $_.autosupport_email_to
	[Void]$abt_cluster.Add($abt_cluster_obj)
	$abt_cluster_obj = "" | select cluster_attribute, cluster_value
	$abt_cluster_obj.cluster_attribute 	= "ocum_user"
	$abt_cluster_obj.cluster_value 		= $_.ocum_user
	[Void]$abt_cluster.Add($abt_cluster_obj)
	$abt_cluster_obj = "" | select cluster_attribute, cluster_value
	$abt_cluster_obj.cluster_attribute 	= "ocopm_user"
	$abt_cluster_obj.cluster_value 		= $_.ocopm_user
	[Void]$abt_cluster.Add($abt_cluster_obj)
	}

$mgmt_ip_cluster_vlan_id 	= ($cdot_ip_addresses | where { $_.network_label -eq "mgmt_cluster" } | select vlan_id).vlan_id
$mgmt_ip_cluster_subnet 	= ($cdot_ip_addresses | where { $_.network_label -eq "mgmt_cluster" } | select subnet).subnet
$mgmt_ip_cluster_netmask 	= ($cdot_ip_addresses | where { $_.network_label -eq "mgmt_cluster" } | select netmask).netmask
$mgmt_ip_cluster_gateway 	= ($cdot_ip_addresses | where { $_.network_label -eq "mgmt_cluster" } | select gateway).gateway
$mgmt_ip_node_vlan_id 		= ($cdot_ip_addresses | where { $_.network_label -eq "mgmt_node" } | select vlan_id).vlan_id
$mgmt_ip_node_subnet 		= ($cdot_ip_addresses | where { $_.network_label -eq "mgmt_node" } | select subnet).subnet
$mgmt_ip_node_netmask 		= ($cdot_ip_addresses | where { $_.network_label -eq "mgmt_node" } | select netmask).netmask
$mgmt_ip_node_gateway 		= ($cdot_ip_addresses | where { $_.network_label -eq "mgmt_node" } | select gateway).gateway
$mgmt_ip_sp_vlan_id 		= ($cdot_ip_addresses | where { $_.network_label -eq "mgmt_sp" } | select vlan_id).vlan_id
$mgmt_ip_sp_subnet 			= ($cdot_ip_addresses | where { $_.network_label -eq "mgmt_sp" } | select subnet).subnet
$mgmt_ip_sp_netmask 		= ($cdot_ip_addresses | where { $_.network_label -eq "mgmt_sp" } | select netmask).netmask
$mgmt_ip_sp_gateway 		= ($cdot_ip_addresses | where { $_.network_label -eq "mgmt_sp" } | select gateway).gateway
$ic_vlan_id 				= ($cdot_ip_addresses | where { $_.network_label -eq "intercluster" } | select vlan_id).vlan_id
$ic_subnet 					= ($cdot_ip_addresses | where { $_.network_label -eq "intercluster" } | select subnet).subnet
$ic_netmask 				= ($cdot_ip_addresses | where { $_.network_label -eq "intercluster" } | select netmask).netmask
$ic_gateway 				= ($cdot_ip_addresses | where { $_.network_label -eq "intercluster" } | select gateway).gateway
#endregion

#region One time CMD blocks

# header_summary_cmds
$header_summary_cmds		= New-Object System.Collections.ArrayList
$header_summary_brk_str		=	"Welcome to cDOT setup;" + `
								"Summary of installation parameters defined in: $datasource_name;" + `
								"Customer Name: $customer_name;" + `
								"Cluster Name: $cluster_name;" + `
								"Cluster Base License: $base_license;" + `
								"Administrator Password (user: $default_admin_user): $default_admin_pass;" + `
								"Domain Name: $domain_name;" + `
								"DNS Servers: $dns_server_ips_str;" + `
								"Location: $system_install_address;" + `
								"Node Management Port (temporary for install): e0M"							
[Void]$header_summary_cmds.Add((Build-CmdHeader $header_summary_brk_str))

# term_rows_0_cmds
$term_rows_0_cmds			= New-Object System.Collections.ArrayList
$term_rows_0_brk_str 		= "Set terminal to eliminate pause"
$term_rows_0_cmd_str 		= "rows 0;"
[Void]$term_rows_0_cmds.Add((Build-CmdHeader $term_rows_0_brk_str))
[Void]$term_rows_0_cmds.Add((Build-CmdContent $term_rows_0_cmd_str))

# system_initialize_cmds
$system_initialize_cmds		= New-Object System.Collections.ArrayList
$system_initialize_brk_str 	= 	"Set system to factory defaults"
$system_initialize_cmd_str 	= 	"set-defaults;" + `
								"setenv bootarg.init.boot_clustered true;" + `
								"saveenv;" + `
								"autoboot;" + `
								"ctrl-c (boot menu);" + `
								"5 (maintenance mode) to assign disks;" + `
								"reboot;" + `
								"ctrl-c (boot menu);" + `
								"4 (zero);"
[Void]$system_initialize_cmds.Add((Build-CmdHeader $system_initialize_brk_str))
[Void]$system_initialize_cmds.Add((Build-CmdContent $system_initialize_cmd_str))

# $network_interface_revert_cmds
$network_interface_revert_cmds 		= New-Object System.Collections.ArrayList
$network_interface_revert_brk_str 	= 	"Move network interfaces to their home nodes;" + `
										"Note: the following command will disconnect your current SSH session;" + `
										"Re-connect to $cluster_name cluster management interface via SSH ($mgmt_ip_cluster)"
$network_interface_revert_cmd_str 	= 	"network interface show -is-home false;" + `
										"network interface revert *;" + `
										"network interface show -is-home false;"
[Void]$network_interface_revert_cmds.Add((Build-CmdHeader $network_interface_revert_brk_str))
[Void]$network_interface_revert_cmds.Add((Build-CmdContent $network_interface_revert_cmd_str))

$timezone_cmds						= New-Object System.Collections.ArrayList
$timezone_brk_str					= 	"Configure current date, time and timezone"
$timezone_cmd_str					= 	"date;" + `
										"date [yymmdd]hhmm;" + `
										"timezone -timezone $timezone -version true;" + `
										"date;"
[Void]$timezone_cmds.Add((Build-CmdHeader $timezone_brk_str))
[Void]$timezone_cmds.Add((Build-CmdContent $timezone_cmd_str))

# snmp_cmds
$snmp_cmds							= New-Object System.Collections.ArrayList
$snmp_brk_str						= 	"Configure SNMP"
$snmp_cmd_str						= 	"system snmp community show;" + `
										"system snmp community add -type ro -community-name $snmp_community_string;" + `
										"system snmp community show;"
[Void]$timezone_cmds.Add((Build-CmdHeader $snmp_brk_str))
[Void]$timezone_cmds.Add((Build-CmdContent $snmp_cmd_str))

# create_mgmt_users_cmds
$create_mgmt_users_cmds				= New-Object System.Collections.ArrayList
$create_mgmt_users_brk_str			= 	"Create management application local accounts ( use password: $default_admin_pass )"
$create_mgmt_users_cmd_str			= 	"security login show;" + `
										"security login create -username $ocum_user -application http -authmethod password -role admin;" + `
										"security login create -username $ocum_user -application ontapi -authmethod password -role admin;" + `
										"security login create -username $ocum_user -application ssh -authmethod password -role admin;" + `
										"security login create -username $ocopm_user -application http -authmethod password -role admin;" + `
										"security login create -username $ocopm_user -application ontapi -authmethod password -role admin;" + `
										"security login create -username $ocopm_user -application ssh -authmethod password -role admin;" + `
										"security login show"
[Void]$create_mgmt_users_cmds.Add((Build-CmdHeader $create_mgmt_users_brk_str))
[Void]$create_mgmt_users_cmds.Add((Build-CmdContent $create_mgmt_users_cmd_str))

# node_rename_cmds
$node_rename_cmds					= New-Object System.Collections.ArrayList
$node_rename_brk_str				= 	"Rename nodes"
$node_rename_cmd_str				= 	"system node show"
[Void]$node_rename_cmds.Add((Build-CmdHeader $node_rename_brk_str))
[Void]$node_rename_cmds.Add((Build-CmdContent $node_rename_cmd_str))

# aggr_rename_cmds
$aggr_rename_cmds					= New-Object System.Collections.ArrayList
$aggr_rename_brk_str				= 		"Rename mroot aggregates"
$aggr_rename_cmd_str				= 		"aggregate show"
[Void]$aggr_rename_cmds.Add((Build-CmdHeader $aggr_rename_brk_str))
[Void]$aggr_rename_cmds.Add((Build-CmdContent $aggr_rename_cmd_str))

# network_create_ifgrp_ten_gbe_cmds
$network_create_ifgrp_ten_gbe_cmds	  = New-Object System.Collections.ArrayList
$network_create_ifgrp_ten_gbe_brk_str = 	"Create 10GbE interface groups on each node"
$network_create_ifgrp_ten_gbe_cmd_str = 	"network port ifgrp show"
[Void]$network_create_ifgrp_ten_gbe_cmds.Add((Build-CmdHeader $network_create_ifgrp_ten_gbe_brk_str))
[Void]$network_create_ifgrp_ten_gbe_cmds.Add((Build-CmdContent $network_create_ifgrp_ten_gbe_cmd_str))

# network_create_ifgrp_one_gbe_cmds
$network_create_ifgrp_one_gbe_cmds	  = New-Object System.Collections.ArrayList
$network_create_ifgrp_one_gbe_brk_str = 	"Create 1GbE interface groups on each node"
$network_create_ifgrp_one_gbe_cmd_str = 	"network port ifgrp show"
[Void]$network_create_ifgrp_one_gbe_cmds.Add((Build-CmdHeader $network_create_ifgrp_one_gbe_brk_str))
[Void]$network_create_ifgrp_one_gbe_cmds.Add((Build-CmdContent $network_create_ifgrp_one_gbe_cmd_str))

# network_create_mgmt_node_fg_cmds
$network_create_mgmt_node_fg_cmds	  = New-Object System.Collections.ArrayList
$network_create_mgmt_node_fg_brk_str  = 	"Create node management failover groups"
$network_create_mgmt_node_fg_cmd_str  = 	"network interface failover-groups show;" + `
											"network interface failover show"
[Void]$network_create_mgmt_node_fg_cmds.Add((Build-CmdHeader $network_create_mgmt_node_fg_brk_str))
[Void]$network_create_mgmt_node_fg_cmds.Add((Build-CmdContent $network_create_mgmt_node_fg_cmd_str))

# network_create_mgmt_cluster_fg_cmds
$network_create_mgmt_cluster_fg_cmds	 = New-Object System.Collections.ArrayList
$network_create_mgmt_cluster_fg_brk_str  = 	"Create cluster/SVM management failover groups"
$network_create_mgmt_cluster_fg_cmd_str  = 	"network interface failover-groups show;" + `
											"network interface failover show"
[Void]$network_create_mgmt_cluster_fg_cmds.Add((Build-CmdHeader $network_create_mgmt_cluster_fg_brk_str))
[Void]$network_create_mgmt_cluster_fg_cmds.Add((Build-CmdContent $network_create_mgmt_cluster_fg_cmd_str))

# network_create_data_fg_cmds
$network_create_data_fg_cmds	 		= New-Object System.Collections.ArrayList
$network_create_data_fg_brk_str  		= 	"Create data failover groups"
$network_create_data_fg_cmd_str  		= 	"network interface failover-groups show;" + `
											"network interface failover show"
[Void]$network_create_data_fg_cmds.Add((Build-CmdHeader $network_create_data_fg_brk_str))
[Void]$network_create_data_fg_cmds.Add((Build-CmdContent $network_create_data_fg_cmd_str))

# network_create_cm_routing_group_cmds
$network_create_cm_routing_group_cmds	= New-Object System.Collections.ArrayList
$network_create_cm_routing_group_brk_str = 	"Create mgmt_cluster routing group default route"
$network_create_cm_routing_group_cmd_str = 	"network routing-groups route show"
[Void]$network_create_cm_routing_group_cmds.Add((Build-CmdHeader $network_create_cm_routing_group_brk_str))
[Void]$network_create_cm_routing_group_cmds.Add((Build-CmdContent $network_create_cm_routing_group_cmd_str))

if ($ic_enabled)
	{
	# network_create_ic_fg_cmds
	$network_create_ic_fg_cmds				= New-Object System.Collections.ArrayList
	$network_create_ic_fg_brk_str 			= 	"Create intercluster failover groups"
	$network_create_ic_fg_cmd_str 			= 	"network interface failover-groups show;" + `
												"network interface failover show"
	[Void]$network_create_ic_fg_cmds.Add((Build-CmdHeader $network_create_ic_fg_brk_str))
	[Void]$network_create_ic_fg_cmds.Add((Build-CmdContent $network_create_ic_fg_cmd_str))

	# network_create_ic_lif_cmds
	$network_create_ic_lif_cmds				= New-Object System.Collections.ArrayList
	$network_create_ic_lif_brk_str 			= 	"Create intercluster LIFs on each node"
	$network_create_ic_lif_cmd_str 			= 	"network interface show"
	[Void]$network_create_ic_lif_cmds.Add((Build-CmdHeader $network_create_ic_lif_brk_str))
	[Void]$network_create_ic_lif_cmds.Add((Build-CmdContent $network_create_ic_lif_cmd_str))
	
	# network_create_ic_routing_group_cmds
	$network_create_ic_routing_group_cmds	 = New-Object System.Collections.ArrayList
	$network_create_ic_routing_group_brk_str = 	"Create intercluster routing groups on each node"
	$network_create_ic_routing_group_cmd_str = 	"network interface failover-groups show;" + `
												"network interface failover show"
	[Void]$network_create_ic_routing_group_cmds.Add((Build-CmdHeader $network_create_ic_routing_group_brk_str))
	[Void]$network_create_ic_routing_group_cmds.Add((Build-CmdContent $network_create_ic_routing_group_cmd_str))
	}

# network_rename_mgmt_lif_cmds
$network_rename_mgmt_lif_cmds				= New-Object System.Collections.ArrayList
$network_rename_mgmt_lif_brk_str 			= 	"Rename mgmt_cluster, mgmt_node LIFs"
$network_rename_mgmt_lif_cmd_str 			= 	"network interface show"
[Void]$network_rename_mgmt_lif_cmds.Add((Build-CmdHeader $network_rename_mgmt_lif_brk_str))
[Void]$network_rename_mgmt_lif_cmds.Add((Build-CmdContent $network_rename_mgmt_lif_cmd_str))

# network_modify_mgmt_lif_cmds
$network_modify_mgmt_lif_cmds	 			= New-Object System.Collections.ArrayList
$network_modify_mgmt_lif_brk_str 			= 	"Modify mgmt_cluster, mgmt_node LIFs"
$network_modify_mgmt_lif_cmd_str 			= 	"network interface show;" + `
												"network interface failover show"
[Void]$network_modify_mgmt_lif_cmds.Add((Build-CmdHeader $network_modify_mgmt_lif_brk_str))
[Void]$network_modify_mgmt_lif_cmds.Add((Build-CmdContent $network_modify_mgmt_lif_cmd_str))

# ntp_cmds
$ntp_cmds	 								= New-Object System.Collections.ArrayList
$ntp_brk_str 								= 	"Configure NTP"
$ntp_cmd_str 								= 	"ntp server show"
[Void]$ntp_cmds.Add((Build-CmdHeader $ntp_brk_str))
[Void]$ntp_cmds.Add((Build-CmdContent $ntp_cmd_str))

# cdp_cmds
$cdp_cmds	 								= New-Object System.Collections.ArrayList
$cdp_brk_str 								= 	"Enable CDP on each node"
[Void]$cdp_cmds.Add((Build-CmdHeader $cdp_brk_str))

# hardware_assist_cmds
$hardware_assist_cmds	 					= New-Object System.Collections.ArrayList
$hardware_assist_brk_str 					= 	"Verify that hardware assist is configured"
$hardware_assist_cmd_str 					= 	"storage failover hwassist show"
[Void]$hardware_assist_cmds.Add((Build-CmdHeader $hardware_assist_brk_str))
[Void]$hardware_assist_cmds.Add((Build-CmdContent $hardware_assist_cmd_str))

# ping cmds
$mgmt_cluster_lif_name		= ($cdot_merged_data | where { $_.network_label -eq "mgmt_cluster" } | select lif_name).lif_name
$mgmt_ip_cluster_gateway	= ($cdot_merged_data | where { $_.network_label -eq "mgmt_cluster" } | select gateway).gateway
$ping_cmds									= New-Object System.Collections.ArrayList
$ping_brk_str								= "Perform connectivity tests"
$ping_cmd_str								= "network ping -lif-owner $cluster_name -lif $mgmt_cluster_lif_name -destination $mgmt_ip_cluster_gateway"
[Void]$ping_cmds.Add((Build-CmdHeader $ping_brk_str))
[Void]$ping_cmds.Add((Build-CmdContent $ping_cmd_str))

# license cmds
$license_cmds								= New-Object System.Collections.ArrayList
$license_brk_str							= "Add cluster software licenses"
$license_cmd_str							= "system license show"
[Void]$license_cmds.Add((Build-CmdHeader $license_brk_str))
[Void]$license_cmds.Add((Build-CmdContent $license_cmd_str))

# aggregate cmds
$aggr_create_cmds		= New-Object System.Collections.ArrayList
$aggr_create_brk_str 	= "Create data aggregates on each node"
$aggr_create_cmd_str 	= "storage aggregate show"
[Void]$aggr_create_cmds.Add((Build-CmdHeader $aggr_create_brk_str))
[Void]$aggr_create_cmds.Add((Build-CmdContent $aggr_create_cmd_str))

$aggr_no_snap_cmds		= New-Object System.Collections.ArrayList
$aggr_no_snap_brk_str	= "Disable aggregate snapshots on each node"
$aggr_no_snap_cmd_str	= "system node run -node * -command snap list -A;" + `
							"system node run -node * -command snap sched -A"
[Void]$aggr_no_snap_cmds.Add((Build-CmdHeader $aggr_create_brk_str))
[Void]$aggr_no_snap_cmds.Add((Build-CmdContent $aggr_create_cmd_str))

# auto giveback cmds
$auto_giveback_cmds		= New-Object System.Collections.ArrayList
$auto_giveback_brk_str	= "Enable auto-giveback"
$auto_giveback_cmd_str	= "storage failover show -fields auto-giveback;" + `
							"storage failover show -fields auto-giveback-after-panic;" + `
							"storage failover modify -node * -auto-giveback true;" + `
							"storage failover modify -node * -auto-giveback-after-panic true;" + `
							"storage failover show -fields auto-giveback;" + `
							"storage failover show -fields auto-giveback-after-panic;"

[Void]$auto_giveback_cmds.Add((Build-CmdHeader $auto_giveback_brk_str))
[Void]$auto_giveback_cmds.Add((Build-CmdContent $auto_giveback_cmd_str))

if ($node_count -eq 2)
	{
	# failover ha cmds
	$two_node_failover_ha_verify_cmds		= New-Object System.Collections.ArrayList
	$two_node_failover_ha_verify_brk_str	= "Verify Storage Failover/Cluster HA are enabled in a 2-node cluster"
	$two_node_failover_ha_verify_cmd_str	= "set -privilege diagnostic;" + `
												"storage failover show;" + `
												"cluster ha show;" + `
												"## If the 'Takeover Possible' is 'false' issue the following, otherwise issue 'set -privilege admin' only;" + `
												"cluster ha modify -configured true;" + `
												"cluster ha show;" + `
												"storage failover show;" + `
												"set -privilege admin;"										
	[Void]$two_node_failover_ha_verify_cmds.Add((Build-CmdHeader $two_node_failover_ha_verify_brk_str))
	[Void]$two_node_failover_ha_verify_cmds.Add((Build-CmdContent $two_node_failover_ha_verify_cmd_str))
	
	$two_node_failover_ha_disable_enable_cmds		= New-Object System.Collections.ArrayList
	$two_node_failover_ha_disable_enable_brk_str	= "Disable/Enable Failover/Cluster HA on a 2-node cluster"
	$two_node_failover_ha_disable_enable_cmd_str	= "storage ha show;" + `
														"cluster ha show;" + `
														"cluster ha modify -configured false;" + `
														"storage failover modify -node * -enabled false;" + `
														"## Wait a couple minutes prior to issuing following command;" + `
														"cluster ha modify -configured true;" + `
														"storage ha show;" + `
														"cluster ha show;"

	[Void]$two_node_failover_ha_disable_enable_cmds.Add((Build-CmdHeader $two_node_failover_ha_disable_enable_brk_str))
	[Void]$two_node_failover_ha_disable_enable_cmds.Add((Build-CmdContent $two_node_failover_ha_disable_enable_cmd_str))
	}
	
$svm_create_cmds	= New-Object System.Collections.ArrayList
$svm_create_brk_str	= "Create storage virtual machine (SVM)"
[Void]$svm_create_cmds.Add((Build-CmdHeader $svm_create_brk_str))

$svm_lif_create_cmds		= New-Object System.Collections.ArrayList
$svm_lif_create_brk_str 	= "Create SVM logical networking interfaces (LIFs - data & mgmt_svm)"
[Void]$svm_lif_create_cmds.Add((Build-CmdHeader $svm_lif_create_brk_str))


$autosupport_cmds		= New-Object System.Collections.ArrayList
$autosupport_brk_str	= "Configure autosupport and send test message"
$autosupport_cmd_str	= "system autosupport show"
[Void]$autosupport_cmds.Add((Build-CmdHeader $autosupport_brk_str))
[Void]$autosupport_cmds.Add((Build-CmdContent $autosupport_cmd_str))

if ($fc_tgt_ports -is [System.Array]) 
	{
	$onboard_tgt_disable_cmds		= New-Object System.Collections.ArrayList
	$onboard_tgt_disable_brk_str	= "Disable onboard UTA2 / CNA / FC ports to prepare for port mode change"
	$onboard_tgt_disable_cmd_str	= "ucadmin show"
	[Void]$onboard_tgt_disable_cmds.Add((Build-CmdHeader $onboard_tgt_disable_brk_str))
	[Void]$onboard_tgt_disable_cmds.Add((Build-CmdContent $onboard_tgt_disable_cmd_str))

	$onboard_tgt_modify_cmds	= New-Object System.Collections.ArrayList
	$onboard_tgt_modify_brk_str	= "Convert onboard UTA2 / CNA / FC ports to target mode"
	$onboard_tgt_modify_cmd_str	= "ucadmin show"

	[Void]$onboard_tgt_modify_cmds.Add((Build-CmdHeader $onboard_tgt_modify_brk_str))
	[Void]$onboard_tgt_modify_cmds.Add((Build-CmdContent $onboard_tgt_modify_cmd_str))
	}

#endregion

#region Merged Data Cmds

$header_summary_brk_str = "IP address summary for cluster: $cluster_name"
$header_summary_cmds.Add((Build-CmdHeader $header_summary_brk_str))
$header_summary_brk = $cdot_merged_data | select ip_address, lif_name, network_label, netmask, gateway | sort -Property network_label | ft -AutoSize
[Void]$header_summary_cmds.Add($header_summary_brk)

$cdot_merged_data | select lif_role, lif_data_protocol
exit
foreach ($item in $cdot_merged_data) 
	{		
	$network_label				= $item.network_label
	$node_def_name				= $item.node_def_name
	$node_number				= $item.node_number
	$node_name					= $item.node_name
	$node_name_sp				= $item.node_name_sp
	$node_aggr_mroot_def_name	= $item.node_aggr_mroot_def_name
	$node_aggr_mroot_name 		= $item.node_aggr_mroot_name
	$software_licenses_str 		= $item.software_licenses_str
	$ten_gbe_ifgrp_name 		= $item.ten_gbe_ifgrp_name
	$ten_gbe_ifgrp_mode 		= $item.ten_gbe_ifgrp_mode
	$ten_gbe_ifgrp_distr_func 	= $item.ten_gbe_ifgrp_distr_func
	$ten_gbe_ifgrp_ports_str 	= $item.ten_gbe_ifgrp_ports
	$one_gbe_ifgrp_name 		= $item.one_gbe_ifgrp_name
	$one_gbe_ifgrp_mode 		= $item.one_gbe_ifgrp_mode
	$one_gbe_ifgrp_distr_func 	= $item.one_gbe_ifgrp_distr_func
	$one_gbe_ifgrp_ports_str 	= $item.one_gbe_ifgrp_ports
	$one_gbe_mgmt_port			= $item.one_gbe_mgmt_port
	$ip_address					= $item.ip_address
	$ip_address_partner			= $item.ip_address_partner	
	$subnet						= $item.subnet
	$netmask					= $item.netmask
	$gateway					= $item.gateway
	$vlan_id					= $item.vlan_id
	$svm_name					= $item.svm_name
	$lif_name					= $item.lif_name
	$lif_role					= $item.lif_role
	$lif_data_protocol			= $item.lif_data_protocol
	$fg_name					= $item.fg_name
	$fg_port					= $item.fg_port
	$failover_policy 			= $item.failover_policy
	$firewall_policy 			= $item.firewall_policy

	if ($network_label -eq "mgmt_cluster")
		{
		[Void]$network_create_mgmt_cluster_fg_cmds.Add("network interface failover-groups create -failover-group $fg_name -node $node_name -port $fg_port")
		[Void]$network_rename_mgmt_lif_cmds.Add("network interface rename -vserver $cluster_name -lif cluster_mgmt -newname $lif_name")
		[Void]$network_modify_mgmt_lif_cmds.Add("network interface modify -vserver $cluster_name -lif $lif_name -home-node $node_name -home-port $fg_port -failover-group $fg_name -auto-revert true")
		}
	elseif ($network_label -eq "mgmt_node")
		{													
		[Void]$node_rename_cmds.Add("system node rename $node_def_name -newname $node_name")
		[Void]$aggr_rename_cmds.Add("storage aggregate rename -aggregate $node_aggr_mroot_def_name -newname $node_aggr_mroot_name")

		$software_licenses	= $software_licenses_str.Split(";")
		foreach ($software_license in $software_licenses)
			{
			[Void]$license_cmds.Add("system license add -license-code $software_license")
			}
		foreach ($ntp_server in $ntp_server_names)
			{
			[Void]$ntp_cmds.Add("ntp server create -node $node_name -server $ntp_server -version max")
			}
		[Void]$cdp_cmds.Add("system node run -node $node_name -command options cdpd.enable on")
		if ($autosupport_enable -eq "enable")
			{
			[Void]$autosupport_cmds.Add("system autosupport modify -node $node_name -state enable -mail-hosts $smtp_server_name -from $autosupport_email_from -support enable -transport $autosupport_transport -to $autosupport_email_to")
			}
		[Void]$network_create_ifgrp_ten_gbe_cmds.Add("network port ifgrp create -node $node_name -ifgrp $ten_gbe_ifgrp_name -mode $ten_gbe_ifgrp_mode -distr-func $ten_gbe_ifgrp_distr_func")
		$ten_gbe_ifgrp_ports = $ten_gbe_ifgrp_ports_str.Split(";")
		foreach ($ten_gbe_ifgrp_port in $ten_gbe_ifgrp_ports)
			{
			[Void]$network_create_ifgrp_ten_gbe_cmds.Add("network port ifgrp add-port -node $node_name -ifgrp $ten_gbe_ifgrp_name -port $ten_gbe_ifgrp_port")
			}
		[Void]$network_create_ifgrp_one_gbe_cmds.Add("network port ifgrp create -node $node_name -ifgrp $one_gbe_ifgrp_name -mode $one_gbe_ifgrp_mode -distr-func $one_gbe_ifgrp_distr_func")
		$one_gbe_ifgrp_ports = $one_gbe_ifgrp_ports_str.Split(";")
		foreach ($one_gbe_ifgrp_port in $one_gbe_ifgrp_ports)
			{
			[Void]$network_create_ifgrp_one_gbe_cmds.Add("network port ifgrp add-port -node $node_name -ifgrp $one_gbe_ifgrp_name -port $one_gbe_ifgrp_port")
			}
		[Void]$network_create_mgmt_node_fg_cmds.Add("network interface failover-groups create -failover-group $fg_name -node $node_name -port $fg_port")
		[Void]$network_create_mgmt_node_fg_cmds.Add("network interface failover-groups create -failover-group $fg_name -node $node_name -port $one_gbe_mgmt_port")

		[Void]$hardware_assist_cmds.Add("storage failover modify -hwassist-partner-ip $ip_address_partner -node $node_name -enabled true")
		[Void]$network_rename_mgmt_lif_cmds.Add("network interface rename -vserver $node_name -lif mgmt1 -newname $lif_name")
		[Void]$network_modify_mgmt_lif_cmds.Add("network interface modify -vserver $node_name -lif $lif_name -home-node $node_name -home-port $fg_port -failover-group $fg_name -auto-revert true")
		
		[int]$aggr_counter = 0
		$cdot_data.get_Item("cdot_aggrs") | where { $_.node_number -eq $node_number } | %{		
			$aggr_action			= $_.action
			$aggr_flashpool_enable	= $_.flash_pool_enable
			$aggr_disk_size_str		= $_.disk_size_str
			$aggr_disk_type			= $_.disk_type
			$aggr_disk_num			= $_.disk_count
			$aggr_raid_size			= $_.raid_size
			$aggr_raid_type			= $_.raid_type

			$aggr_disk_size_gb 	= ($cdot_data.get_Item("cdot_disks") | where { $_.disk_size_str -eq $aggr_disk_size_str } | select disk_size_gb).disk_size_gb
			$aggr_disk_type_lc 	= $aggr_disk_type.ToLower()

			if ($aggr_action -eq "create")
				{						
				[Void]$aggr_no_snap_cmds.Add("system node run -node $node_name -command snap sched -A $node_aggr_mroot_name 0 0 0")
				[Void]$aggr_no_snap_cmds.Add("system node run -node $node_name -command aggr options $node_aggr_mroot_name nosnap on")
				[Void]$aggr_no_snap_cmds.Add("system node run -node $node_name -command snap delete -A -a -f $node_aggr_mroot_name")
				[Void]$aggr_no_snap_cmds.Add("system node run -node $node_name -command snap reserve -A $node_aggr_mroot_name 0")						
				if ($aggr_flashpool_enable -eq "enable") 
					{
					$aggr_name = $node_name_nodash + "_" + $aggr_disk_size_str + "_" + $aggr_disk_type_lc + "_fp_aggr" + $aggr_counter
	
					[Void]$non_mroot_aggrs.Add($aggr_name)

					[Void]$aggr_create_cmds.Add("storage aggregate create -aggregate $aggr_name -node $node_name -diskcount $aggr_disk_num -disktype $aggr_disk_type -disksize $aggr_disk_size_gb -maxraidsize $aggr_raid_size -raidtype $aggr_raid_type")
					[Void]$aggr_create_cmds.Add("storage aggregate modify -aggregate $aggr_name -node $node_name -hybrid-enabled true")

					[Void]$aggr_no_snap_cmds.Add("system node run -node $node_name -command snap sched -A $aggr_name 0 0 0")
					[Void]$aggr_no_snap_cmds.Add("system node run -node $node_name -command aggr options $aggr_name nosnap on")
					[Void]$aggr_no_snap_cmds.Add("system node run -node $node_name -command snap delete -A -a -f $aggr_name")
					[Void]$aggr_no_snap_cmds.Add("system node run -node $node_name -command snap reserve -A $aggr_name 0")
					$aggr_counter++
					}
				elseif ($aggr_flashpool_enable -eq "disable")
					{
					$aggr_name = $node_name_nodash + "_" + $aggr_disk_size_str + "_" + $aggr_disk_type_lc + "_aggr" + $aggr_counter
					[Void]$non_mroot_aggrs.Add($aggr_name)

					[Void]$aggr_create_cmds.Add("storage aggregate create -aggregate $aggr_name -node $node_name -diskcount $aggr_disk_num -disktype $aggr_disk_type -disksize $aggr_disk_size_gb -maxraidsize $aggr_raid_size -raidtype $aggr_raid_type")

					[Void]$aggr_no_snap_cmds.Add("system node run -node $node_name -command snap sched -A $aggr_name 0 0 0")
					[Void]$aggr_no_snap_cmds.Add("system node run -node $node_name -command aggr options $aggr_name nosnap on")
					[Void]$aggr_no_snap_cmds.Add("system node run -node $node_name -command snap delete -A -a -f $aggr_name")
					[Void]$aggr_no_snap_cmds.Add("system node run -node $node_name -command snap reserve -A $aggr_name 0")
					}
				}
			elseif ($aggr_action -eq "add")
				{
				[Void]$aggr_create_cmds.Add("storage aggregate add -aggregate $aggr_name -diskcount $aggr_disk_num -disktype $aggr_disk_type -disksize $aggr_disk_size_gb -raidtype $aggr_raid_type")
				}
			}		
		}
	elseif ($network_label -eq "mgmt_svm")
		{
		[Void]$svm_lif_create_cmds.Add("network interface create -vserver $svm_name -lif $lif_name -role $lif_role -data-protocol $lif_data_protocol -home-node $node_name -home-port $fg_port -address $ip_address -netmask $netmask -status-admin up -failover-policy $failover_policy -firewall-policy $firewall_policy -auto-revert true -failover-group $fg_name")
		}	
	elseif ($network_label -eq "intercluster")
		{
		$node_routing_group_name = Get-RoutingGroup -role "intercluster" -subnet $subnet -netmask $netmask
		[Void]$network_create_ic_fg_cmds.Add("network interface failover-groups create -failover-group $fg_name -node $node_name -port $fg_port")
		[Void]$network_create_ic_routing_group_cmds.Add("network routing-groups route create -vserver $node_name -routing-group $node_routing_group_name -destination 0.0.0.0/0 -gateway $gateway")
		[Void]$network_create_ic_lif_cmds.Add("network interface create -vserver $node_name -lif $lif_name -role $lif_role -home-node $node_name -home-port $fg_port -address $ip_address -netmask $netmask -status-admin up -failover-policy $failover_policy -firewall-policy $firewall_policy -failover-group $fg_name")
		[Void]$ping_cmds.Add("network ping -lif-owner $node_name -lif $lif_name -destination $gateway")
		}
	elseif ($network_label -match "iscsi")
		{
		"lif_role: $lif_role"
		[Void]$svm_lif_create_cmds.Add("network interface create -vserver $svm_name -lif $lif_name -role $lif_role -data-protocol $lif_data_protocol -home-node $node_name -home-port $fg_port -address $ip_address -netmask $netmask -status-admin up -firewall-policy $firewall_policy")
		if (!($svm_data_protocol_lookup.ContainsKey($network_label)))
			{
			$svm_data_protocol_lookup.Add($network_label, "")
			}
		}
	elseif ($network_label -notmatch "mgmt_sp|mgmt_svm")
		{
		[Void]$network_create_data_fg_cmds.Add("network interface failover-groups create -failover-group $fg_name -node $node_name -port $fg_port")
		[Void]$svm_lif_create_cmds.Add("network interface create -vserver $svm_name -lif $lif_name -role $lif_role -data-protocol $lif_data_protocol -home-node $node_name -home-port $fg_port -address $ip_address -netmask $netmask -status-admin up -failover-policy $failover_policy -firewall-policy $firewall_policy -auto-revert true -failover-group $fg_name")
		if (!($svm_data_protocol_lookup.ContainsKey($network_label)))
			{
			$svm_data_protocol_lookup.Add($network_label, "")
			}
		}						
	}
$network_create_ifgrp_ten_gbe_cmd_str = "network port ifgrp show;"
[Void]$network_create_ifgrp_ten_gbe_cmds.Add((Build-CmdContent $network_create_ifgrp_ten_gbe_cmd_str))
$network_create_ifgrp_one_gbe_cmd_str = "network port ifgrp show;"
[Void]$network_create_ifgrp_one_gbe_cmds.Add((Build-CmdContent $network_create_ifgrp_one_gbe_cmd_str))

$node1_name		= ($cdot_merged_data | where { ($_.node_number -eq 1) -and ($_.network_label -eq "mgmt_node") } | select node_name).node_name
$mgmt_node1_ip 	= ($cdot_merged_data | where { ($_.node_number -eq 1) -and ($_.network_label -eq "mgmt_node") } | select ip_address).ip_address

$header_summary_brk_str	= 	"Connect to first node: $node1_name ($mgmt_node1_ip);" + `
							"Node management interface via SSH to issue remaining;" + `
							"commands as console can have issues with line ends"
[Void]$header_summary_cmds.Add((Build-CmdHeader $header_summary_brk_str))

# VLAN CMDS
$node_names 	= $cdot_merged_data | select node_name -Unique
$ten_gbe_vlans 	= $cdot_merged_data | where { $_.network_label -notmatch "mgmt_node|mgmt_sp|mgmt_cluster|access" } | select vlan_id -Unique | %{ $_.vlan_id }
$one_gbe_vlans 	= $cdot_merged_data | where { $_.network_label -match "mgmt_node|mgmt_sp|mgmt_cluster"  } | select vlan_id -Unique | %{ $_.vlan_id }

$network_create_vlan_ten_gbe_cmds	  = New-Object System.Collections.ArrayList
$network_create_vlan_ten_gbe_brk_str  = "Create VLANs on 10GbE interface groups on each node"
$network_create_vlan_ten_gbe_cmd_str  = "network port show"
[Void]$network_create_vlan_ten_gbe_cmds.Add((Build-CmdHeader $network_create_vlan_ten_gbe_brk_str))
[Void]$network_create_vlan_ten_gbe_cmds.Add((Build-CmdContent $network_create_vlan_ten_gbe_cmd_str))

foreach ($node in $node_names)
	{
	$node_name = $node.node_name
	foreach ($ten_gbe_vlan_id in $ten_gbe_vlans)
		{
		if ($ten_gbe_vlan_id -ne "access")
			{
			$ten_gbe_ifgrp_name = ($cdot_merged_data | where { $_.vlan_id -eq $ten_gbe_vlan_id } | select ten_gbe_ifgrp_name -Unique).ten_gbe_ifgrp_name
			[Void]$network_create_vlan_ten_gbe_cmds.Add("network port vlan create -node $node_name -vlan-id $ten_gbe_vlan_id -port $ten_gbe_ifgrp_name")
			}
		}
	}
$network_create_vlan_ten_gbe_cmd_str = "network port show;"
[Void]$network_create_vlan_ten_gbe_cmds.Add((Build-CmdContent $network_create_vlan_ten_gbe_cmd_str))

# network_create_vlan_one_gbe_cmds
$network_create_vlan_one_gbe_cmds	  = New-Object System.Collections.ArrayList
$network_create_vlan_one_gbe_brk_str  = "Create VLANs on 1GbE interface groups on each node"
$network_create_vlan_one_gbe_cmd_str  = "network port show"
[Void]$network_create_vlan_one_gbe_cmds.Add((Build-CmdHeader $network_create_vlan_one_gbe_brk_str))
[Void]$network_create_vlan_one_gbe_cmds.Add((Build-CmdContent $network_create_vlan_one_gbe_cmd_str))

foreach ($node in $node_names)
	{
	$node_name = $node.node_name
	foreach ($one_gbe_vlan_id in $one_gbe_vlans)
		{
		if ($one_gbe_vlan_id -ne "access")
			{
			$one_gbe_ifgrp_name = ($cdot_merged_data | where { $_.vlan_id -eq $one_gbe_vlan_id } | select one_gbe_ifgrp_name -Unique).one_gbe_ifgrp_name
			$network_create_vlan_one_gbe_cmd_str = "network port vlan create -node $node_name -vlan-id $one_gbe_vlan_id -port $one_gbe_ifgrp_name"
			[Void]$network_create_vlan_one_gbe_cmds.Add((Build-CmdContent $network_create_vlan_one_gbe_cmd_str))
			}
		}
	}

$network_create_vlan_one_gbe_cmd_str  = "network port show;"
[Void]$network_create_vlan_one_gbe_cmds.Add((Build-CmdContent $network_create_vlan_one_gbe_cmd_str))

#$header_summary_brk_str	= "Connect to first node: $node1_name ($node1_ip_address);" + `
#							"Node management interface via SSH to issue remaining;" + `
#							"commands as console can have issues with line ends"
#[Void]$header_summary_cmds.Add((Build-CmdHeader $header_summary_brk_str))

## Set lif network values
#$cdot_network_lifs | %{
#	$svm_number						= $_.svm_name
#	$svm_lif_network_label			= $_.network_label
#	$svm_lif_role					= $_.network_role
#	$svm_lif_data_protocol			= $_.network_data_protocol
#	$svm_lif_vlan_id				= $_.vlan_id
#	$svm_lif_ip_address				= $_.ip_address
#	$svm_lif_netmask				= $_.netmask	
#	$svm_lif_firewall_policy		= $_.firewall_policy
#	$svm_lif_failover_policy		= $_.failover_policy
#	$svm_lif_node_id				= $_.node_id
#	$svm_lif_subnet					= $_.subnet
#	$svm_lif_number 				= $_.lif_counter.ToString("000")
#	$home_node_name 				= ($node_attrs | where { $_.node_number -eq $node_range_counter } | select node_name).node_name
#	$home_node_name_nodash 			= $home_node_name.Replace("-","_")
#		if ($svm_lif_network_label -notmatch "iscsi")
#			{
#			if ($svm_lif_network_label -eq "intercluster")
#				{
#				$ic_lif_name 	= $home_node_name_nodash + "_" + $svm_lif_network_label + "_lif"
#				$ic_home_port	= $ten_gbe_ifgrp_name + "-" + $svm_lif_vlan_id
#				$ic_fg_name		= $home_node_name_nodash + "_intercluster_" + $ic_vlan_id + "_fg"
#				
#				$abt_lifs_obj					= "" | select lif_owner, lif_name, lif_network_label, vlan_id, ip_address, fg_port, fg_name
#				$abt_lifs_obj.lif_owner			= $home_node_name
#				$abt_lifs_obj.lif_name			= $ic_lif_name
#				$abt_lifs_obj.lif_network_label	= $svm_lif_network_label
#				$abt_lifs_obj.vlan_id			= $svm_lif_vlan_id
#				$abt_lifs_obj.ip_address		= $svm_lif_ip
#				$abt_lifs_obj.fg_port		= $ic_home_port
#				$abt_lifs_obj.fg_name			= $ic_fg_name
#				[Void]$abt_lifs.Add($abt_lifs_obj)
#				[Void]$network_create_ic_lif_cmds.Add("network interface create -vserver $home_node_name -lif $ic_lif_name -role $svm_lif_role -home-node $home_node_name -home-port $ic_home_port -address $svm_lif_ip_address -netmask $svm_lif_netmask -status-admin up -failover-policy $svm_lif_failover_policy -firewall-policy $svm_lif_firewall_policy -failover-group $ic_fg_name")
#				[Void]$ping_cmds.Add("network ping -lif-owner $home_node_name -lif $ic_lif_name -destination $ic_gateway")
#				}			
#			elseif ($svm_lif_network_label -eq "mgmt_svm")
#				{
#				$svm_lif_name  			= $svm_name + "_" + $svm_lif_network_label + "_" + $svm_lif_vlan_id + "_lif"
#				$svm_ifgrp_vlan_port 	= $ten_gbe_ifgrp_name + "-" + $svm_lif_vlan_id
#				$svm_fg_name 			= $svm_lif_network_label + "_" + $svm_lif_vlan_id + "_fg"
#
#				$abt_lifs_obj					= "" | select lif_owner, lif_name, lif_network_label, vlan_id, ip_address, fg_port, fg_name
#				$abt_lifs_obj.lif_owner			= $svm_name
#				$abt_lifs_obj.lif_name			= $svm_lif_name
#				$abt_lifs_obj.lif_network_label	= $svm_lif_network_label
#				$abt_lifs_obj.vlan_id			= $svm_lif_vlan_id
#				$abt_lifs_obj.ip_address		= $svm_lif_ip_address
#				$abt_lifs_obj.fg_port			= $svm_ifgrp_vlan_port
#				$abt_lifs_obj.fg_name			= $mgmt_cluster_fg_name
#				[Void]$abt_lifs.Add($abt_lifs_obj)
#
#				[Void]$svm_lif_create_cmds.Add("network interface create -vserver $svm_name -lif $svm_lif_name -role $svm_lif_role -data-protocol $svm_lif_data_protocol -home-node $home_node_name -home-port $svm_ifgrp_vlan_port -address $svm_lif_ip_address -netmask $mgmt_ip_cluster_netmask -status-admin up -failover-policy $svm_lif_failover_policy -firewall-policy $svm_lif_firewall_policy -auto-revert true -failover-group $mgmt_cluster_fg_name")
#				}
#			elseif ($svm_lif_network_label -notmatch "mgmt|intercluster")
#				{
#				$svm_lif_name  			= $svm_name + "_" + $svm_lif_network_label + "_" + $svm_lif_vlan_id + "_lif" + $lif_number
#				$svm_ifgrp_vlan_port 	= $ten_gbe_ifgrp_name + "-" + $svm_lif_vlan_id
#				$svm_fg_name 			= $svm_lif_network_label + "_" + $svm_lif_vlan_id + "_fg"
#
#				$abt_lifs_obj					= "" | select lif_owner, lif_name, lif_network_label, vlan_id, ip_address, fg_port, fg_name
#				$abt_lifs_obj.lif_owner			= $svm_name
#				$abt_lifs_obj.lif_name			= $svm_lif_name
#				$abt_lifs_obj.lif_network_label	= $svm_lif_network_label
#				$abt_lifs_obj.vlan_id			= $svm_lif_vlan_id
#				$abt_lifs_obj.ip_address		= $svm_lif_ip_address
#				$abt_lifs_obj.fg_port			= $svm_ifgrp_vlan_port
#				$abt_lifs_obj.fg_name			= $svm_fg_name
#				[Void]$abt_lifs.Add($abt_lifs_obj)
#								
#				[Void]$svm_lif_create_cmds.Add("network interface create -vserver $svm_name -lif $svm_lif_name -role $svm_lif_role -data-protocol $svm_lif_data_protocol -home-node $home_node_name -home-port $svm_ifgrp_vlan_port -address $svm_lif_ip_address -netmask $svm_lif_netmask -status-admin up -failover-policy $svm_lif_failover_policy -firewall-policy $svm_lif_firewall_policy -auto-revert true -failover-group $svm_fg_name")
#				}
#	
#			$svm_lif_ip_counter++
#			$svm_lif_counter++
#			$node_range_counter++
#			}
#		elseif ($svm_lif_network_label -match "iscsi")
#			{
#			$svm_lif_name  			= $svm_name + "_" + $svm_lif_network_label + "_" + $svm_lif_vlan_id + "_lif" + $lif_number
#			$svm_ifgrp_vlan_port 	= $ten_gbe_ifgrp_name + "-" + $svm_lif_vlan_id
#			$svm_fg_name 			= $svm_lif_network_label + "_" + $svm_lif_vlan_id + "_fg"
#			$svm_lif_netmask		= ($cdot_networks | where { ($_.network_label -eq $svm_lif_network_label) -and ($_.vlan_id -eq $svm_lif_vlan_id) } | select netmask).netmask
#
#			$abt_lifs_obj					= "" | select lif_owner, lif_name, lif_network_label, vlan_id, ip_address, fg_port, fg_name
#			$abt_lifs_obj.lif_owner			= $svm_name
#			$abt_lifs_obj.lif_name			= $svm_lif_name
#			$abt_lifs_obj.lif_network_label	= $svm_lif_network_label
#			$abt_lifs_obj.vlan_id			= $svm_lif_vlan_id
#			$abt_lifs_obj.ip_address		= $svm_lif_ip
#			$abt_lifs_obj.fg_port		= $svm_ifgrp_vlan_port
#			$abt_lifs_obj.fg_name			= "not_used"
#			[Void]$abt_lifs.Add($abt_lifs_obj)
#						
#			[Void]$svm_lif_create_cmds.Add("network interface create -vserver $svm_name -lif $svm_lif_name -role $svm_lif_role -data-protocol $svm_lif_data_protocol -home-node $home_node_name -home-port $svm_ifgrp_vlan_port -address $svm_lif_ip -netmask $svm_lif_netmask -status-admin up -firewall-policy $svm_lif_firewall_policy")
#			$svm_lif_ip_counter++
#			$svm_lif_counter++
#			$node_range_counter++
#			}
#	else 
#		{
#		$node_range_counter = $node_range_start
#		$home_node_name 	= ($node_attrs | where { $_.node_number -eq $node_range_counter } | select node_name).node_name
#		}
#	}
#
##endregion

##region Node

		
#$cdot_data.get_Item("cdot_nodes") | %{			
#	[int]$node_number		= $_.node_number
#	$node_name_chk			= $_.node_name
#	$node_model				= $_.node_model
#	$software_licenses_str	= $_.software_licenses
#	$node_def_info 			= Get-NodeDefInfo -cluster_name $cluster_name -node_number $node_number
#	$node_def_name			= $cluster_name + "-" + $node_number.ToString("00")
#	$node_aggr_mroot_def_name	= $node_def_info[1]
#
#	[Void]$node_rename_cmds.Add("system node rename $node_def_name -newname $node_name")
#	[Void]$aggr_rename_cmds.Add("storage aggregate rename -aggregate $node_aggr_mroot_def_name -newname $node_aggr_mroot_name")	
#	
#	if ($software_licenses_str -match ";")
#		{
#		$software_licenses	= $software_licenses_str.Split(";")
#		foreach ($software_license in $software_licenses)
#			{
#			[Void]$license_cmds.Add("system license add -license-code $software_license")
#			}
#		}
#	else
#		{
#		[Void]$license_cmds.Add("system license add -license-code $software_licenses_str")
#		}
#	$node_attr = "" | select node_number, node_name, mgmt_ip_node, mgmt_ip_sp
#	$node_attr.node_number = $node_number
#	$mgmt_ip_node 			= $cdot_ip_addresses | where { ($_.network_label -eq "mgmt_node") -and ($_.node_id -eq $node_number) } | select ip_address | %{ $_.ip_address }
#	$node_attr.mgmt_ip_node = $mgmt_ip_node
#	$mgmt_ip_sp		= $cdot_ip_addresses | where { ($_.network_label -eq "mgmt_sp") -and ($_.node_id -eq $node_number) } | select ip_address | %{ $_.ip_address }
#	$node_attr.mgmt_ip_sp 	= $mgmt_ip_sp
#	
#	if ($node_name_chk -ne "use_default")
#		{
#		$node_name				= $node_name_chk
#		$node_attr.node_name 	= $node_name
#		$node_name_nodash		= $node_name.Replace("-","_")
#		}
#	else
#		{
#		$node_name				= $node_def_info[0]
#		$node_attr.mgmt_ip_node = $node_name
#		$node_name_nodash		= $node_name.Replace("-","_")
#		}
#	
#	[Void]$node_attrs.Add($node_attr)
#	$node_aggr_mroot_name = $node_name_nodash + "_mroot"
#	$autosupport_email_from	= $node_name + "@" + $domain_name
#	
#		
#	# ifgrp cmds
#	[Void]$network_create_ifgrp_ten_gbe_cmds.Add("network port ifgrp create -node $node_name -ifgrp $ten_gbe_ifgrp_name -mode $ten_gbe_ifgrp_mode -distr-func $ten_gbe_ifgrp_distr_func")
#
#	foreach ($ifgrp_port in $ten_gbe_ifgrp_ports)
#		{
#
#		$abt_ifgrps_obj = "" | select node_name, node_port_name, ifgrp_name, vlan_ids
#		$abt_ifgrps_obj.node_name 	= $node_name
#		$abt_ifgrps_obj.node_port_name 	= $ifgrp_port
#		$abt_ifgrps_obj.ifgrp_name 	= $ten_gbe_ifgrp_name
#		$abt_ifgrps_obj.vlan_ids 	= $ten_gbe_vlans -join ","
#		[Void]$abt_ifgrps.Add($abt_ifgrps_obj)
#		
#		[Void]$network_create_ifgrp_ten_gbe_cmds.Add("network port ifgrp add-port -node $node_name -ifgrp $ten_gbe_ifgrp_name -port $ifgrp_port")
#		}
#	
#	[Void]$network_create_ifgrp_one_gbe_cmds.Add("network port ifgrp create -node $node_name -ifgrp $one_gbe_ifgrp_name -mode $one_gbe_ifgrp_mode -distr-func $one_gbe_ifgrp_distr_func")
#
#	foreach ($ifgrp_port in $one_gbe_ifgrp_ports)
#		{
#
#		$abt_ifgrps_obj = "" | select node_name, node_port_name, ifgrp_name, vlan_ids
#		$abt_ifgrps_obj.node_name 	= $node_name
#		$abt_ifgrps_obj.node_port_name 	= $ifgrp_port
#		$abt_ifgrps_obj.ifgrp_name 	= $one_gbe_ifgrp_name
#		$abt_ifgrps_obj.vlan_ids 	= $one_gbe_vlans -join ","
#		[Void]$abt_ifgrps.Add($abt_ifgrps_obj)
#
#		[Void]$network_create_ifgrp_one_gbe_cmds.Add("network port ifgrp add-port -node $node_name -ifgrp $one_gbe_ifgrp_name -port $ifgrp_port")			
#		}
#
#	
#	# ntp cmds
#	foreach ($ntp_server in $ntp_server_names)
#		{
#		[Void]$ntp_cmds.Add("ntp server create -node $node_name -server $ntp_server -version max")
#		}
#
#	# vlan cmds

#		
#	# cdp cmds
#	[Void]$cdp_cmds.Add("system node run -node $node_name -command options cdpd.enable on")
#
#	if ($autosupport_enable -eq "enable")
#		{
#		[Void]$autosupport_cmds.Add("system autosupport modify -node $node_name -state enable -mail-hosts $smtp_server_name -from $autosupport_email_from -support enable -transport $autosupport_transport -to $autosupport_email_to")
#		}
#		[int]$aggr_counter = 0
#		$cdot_data.get_Item("cdot_aggrs") | where { $_.node_number -eq $node_number } | %{		
#			$node_number			= $_.node_number
#			$aggr_action			= $_.action
#			$aggr_flashpool_enable	= $_.flash_pool_enable
#			$aggr_disk_size_str		= $_.disk_size_str
#			$aggr_disk_type			= $_.disk_type
#			$aggr_disk_num			= $_.disk_count
#			$aggr_raid_size			= $_.raid_size
#			$aggr_raid_type			= $_.raid_type
#
#			$aggr_disk_size_gb 	= ($cdot_data.get_Item("cdot_disks") | where { $_.disk_size_str -eq $aggr_disk_size_str } | select disk_size_gb).disk_size_gb
#			$aggr_disk_type_lc 	= $aggr_disk_type.ToLower()
#
#			if ($aggr_action -eq "create")
#				{						
#				[Void]$aggr_no_snap_cmds.Add("system node run -node $node_name -command snap sched -A $node_aggr_mroot_name 0 0 0")
#				[Void]$aggr_no_snap_cmds.Add("system node run -node $node_name -command aggr options $node_aggr_mroot_name nosnap on")
#				[Void]$aggr_no_snap_cmds.Add("system node run -node $node_name -command snap delete -A -a -f $node_aggr_mroot_name")
#				[Void]$aggr_no_snap_cmds.Add("system node run -node $node_name -command snap reserve -A $node_aggr_mroot_name 0")						
#				if ($aggr_flashpool_enable -eq "enable") 
#					{
#					$aggr_name = $node_name_nodash + "_" + $aggr_disk_size_str + "_" + $aggr_disk_type_lc + "_fp_aggr" + $aggr_counter
#	
#					[Void]$non_mroot_aggrs.Add($aggr_name)
#
#					[Void]$aggr_create_cmds.Add("storage aggregate create -aggregate $aggr_name -node $node_name -diskcount $aggr_disk_num -disktype $aggr_disk_type -disksize $aggr_disk_size_gb -maxraidsize $aggr_raid_size -raidtype $aggr_raid_type")
#					[Void]$aggr_create_cmds.Add("storage aggregate modify -aggregate $aggr_name -node $node_name -hybrid-enabled true")
#
#					[Void]$aggr_no_snap_cmds.Add("system node run -node $node_name -command snap sched -A $aggr_name 0 0 0")
#					[Void]$aggr_no_snap_cmds.Add("system node run -node $node_name -command aggr options $aggr_name nosnap on")
#					[Void]$aggr_no_snap_cmds.Add("system node run -node $node_name -command snap delete -A -a -f $aggr_name")
#					[Void]$aggr_no_snap_cmds.Add("system node run -node $node_name -command snap reserve -A $aggr_name 0")
#					$aggr_counter++
#					}
#				elseif ($aggr_flashpool_enable -eq "disable")
#					{
#					$aggr_name = $node_name_nodash + "_" + $aggr_disk_size_str + "_" + $aggr_disk_type_lc + "_aggr" + $aggr_counter
#					[Void]$non_mroot_aggrs.Add($aggr_name)
#
#					[Void]$aggr_create_cmds.Add("storage aggregate create -aggregate $aggr_name -node $node_name -diskcount $aggr_disk_num -disktype $aggr_disk_type -disksize $aggr_disk_size_gb -maxraidsize $aggr_raid_size -raidtype $aggr_raid_type")
#
#					[Void]$aggr_no_snap_cmds.Add("system node run -node $node_name -command snap sched -A $aggr_name 0 0 0")
#					[Void]$aggr_no_snap_cmds.Add("system node run -node $node_name -command aggr options $aggr_name nosnap on")
#					[Void]$aggr_no_snap_cmds.Add("system node run -node $node_name -command snap delete -A -a -f $aggr_name")
#					[Void]$aggr_no_snap_cmds.Add("system node run -node $node_name -command snap reserve -A $aggr_name 0")
#					}
#				}
#			elseif ($aggr_action -eq "add")
#				{
#					[Void]$aggr_create_cmds.Add("storage aggregate add -aggregate $aggr_name -diskcount $aggr_disk_num -disktype $aggr_disk_type -disksize $aggr_disk_size_gb -raidtype $aggr_raid_type")
#				}
#			
#			}
#	}
	
# hwassist cmds

$node_attrs | %{
	[int]$node_number	= $_.node_number
	$node_name			= $_.node_name
	if ($node_number % 2 -ne 0)
		{
		$node_number_partner 		= ($node_number + 1)
		$mgmt_node_ip_node_partner	= ($node_attrs | where {  $_.node_number -eq $node_number_partner } | select mgmt_ip_node).mgmt_ip_node
		}
	else
		{
		$node_number_partner 		= ($node_number - 1)
		$mgmt_node_ip_node_partner	= ($node_attrs | where {  $_.node_number -eq $node_number_partner } | select mgmt_ip_node).mgmt_ip_node
		}
	$hardware_assist_cmd_str = "storage failover modify -hwassist-partner-ip $mgmt_node_ip_node_partner -node $node_name -enabled true"
	[Void]$hardware_assist_cmds.Add((Build-CmdContent $hardware_assist_cmd_str))
	}

#endregion

#region SVM

foreach ($svm_protocol in $all_svm_protocols)
	{
	if (!($svm_data_protocol_lookup.ContainsKey($svm_protocol)))
		{
		[Void]$svm_disallowed_protocols.Add($svm_protocol)
		}
	}

if ($svm_data_protocol_lookup.ContainsKey("nfs"))
	{
	$svm_nfs_enabled = $true
	}
else
	{
	$svm_nfs_enabled = $false
	}
if ($svm_data_protocol_lookup.ContainsKey("cifs"))
	{
	$svm_cifs_enabled = $true
	}
else
	{
	$svm_cifs_enabled = $false
	}
$aggr_list_str 		= $non_mroot_aggrs -join ","
$svm_root_aggr_name = $non_mroot_aggrs[0]

$svm_disallowed_protocols_str = $svm_disallowed_protocols -join ","
	
$cdot_data.get_Item("cdot_svms") | %{
	$svm_number						= $_.svm_number
	$svm_name						= ($cdot_merged_data | where { $_.network_label -eq "mgmt_svm" } | select svm_name).svm_name	
	$svm_ls_mir_sched_mins			= $_.ls_mir_sched_mins
	$svm_ls_mir_sched_name			= $_.ls_mir_sched_name
	$svm_network_attrs				= $cdot_merged_data | where { $_.network_label -eq "mgmt_cluster" }
	$svm_mgmt_lif_vlan_id_str		= $svm_network_attrs.vlan_id
	$mgmt_ip_cluster_subnet			= $svm_network_attrs.subnet
	$mgmt_ip_cluster_netmask		= $svm_network_attrs.netmask
	$mgmt_cluster_gateway			= $svm_network_attrs.gateway
	$mgmt_cluster_routing_group		= Get-RoutingGroup -role "cluster-mgmt" -subnet $mgmt_ip_cluster_subnet -netmask $mgmt_ip_cluster_netmask
	$mgmt_cluster_fg_vlan_port		= $ten_gbe_node_ifgrp + "-" + $svm_mgmt_lif_vlan_id_str

	$svm_create_cmd_str = "vserver create -vserver $svm_name -rootvolume $svm_root_vol_name -aggregate $svm_root_aggr_name -ns-switch file -rootvolume-security-style unix -language en_US -quota-policy default;" + `
							"vserver modify -vserver $svm_name -disallowed-protocols $svm_disallowed_protocols_str;"
	if (($svm_nfs_enabled) -or ($svm_cifs_enabled))
		{
		$svm_create_cmd_str = $svm_create_cmd_str + `
								"export-policy rule create -vserver $svm_name -policyname default -clientmatch 0.0.0.0/0 -rorule none -rwrule none -superuser none;" + `
								"vserver nfs create -vserver $svm_name -v3 enabled -v4.0 enabled;"
		}
	$svm_create_cmd_str = $svm_create_cmd_str + `
							"network routing-groups route create -vserver $svm_name -routing-group $mgmt_cluster_routing_group -gateway $mgmt_cluster_gateway -metric 20;" + `
							"vserver show -vserver $svm_name;"
	[Void]$network_create_cm_routing_group_cmds.Add()
	[Void]$svm_create_cmds.Add((Build-CmdContent $svm_create_cmd_str))
	}


#endregion

#region Footer

#$abt_lifs_obj					= "" | select lif_owner, lif_name, lif_network_label, vlan_id, ip_address, fg_port, fg_name
#$abt_lifs_obj.lif_owner			= $cluster_name
#$abt_lifs_obj.lif_name			= $mgmt_cluster_lif_name
#$abt_lifs_obj.lif_network_label	= $svm_lif_network_label
#$abt_lifs_obj.vlan_id			= $svm_lif_vlan_id
#$abt_lifs_obj.ip_address		= $svm_lif_ip
#$abt_lifs_obj.fg_port	= $mgmt_cluster_fg_port
#$abt_lifs_obj.fg_name			= $mgmt_cluster_fg_name
#[Void]$abt_lifs.Add($abt_lifs_obj)

$aggr_rename_cmd_str = "aggregate show;"
[Void]$aggr_rename_cmds.Add((Build-CmdContent $aggr_rename_cmd_str))

$node_rename_cmd_str = "system node show;"
[Void]$node_rename_cmds.Add((Build-CmdContent $node_rename_cmd_str))

if ($fc_tgt_ports -is [System.Array])
	{
	$onboard_tgt_disable_cmd_str = "ucadmin show"
	[Void]$onboard_tgt_disable_cmds.Add((Build-CmdContent $onboard_tgt_disable_cmd_str))

	$onboard_tgt_modify_cmd_str = "ucadmin show"
	[Void]$onboard_tgt_modify_cmds.Add((Build-CmdContent $onboard_tgt_modify_cmd_str))
	}
#
#$network_create_ifgrp_ten_gbe_cmd_str = "network port ifgrp show;"
#[Void]$network_create_ifgrp_ten_gbe_cmds.Add((Build-CmdContent $network_create_ifgrp_ten_gbe_cmd_str))
#
#$network_create_vlan_one_gbe_cmd_str = "network port show"
#[Void]$network_create_vlan_one_gbe_cmds.Add((Build-CmdContent $network_create_vlan_one_gbe_cmd_str))

$network_create_mgmt_node_fg_cmd_str = "network interface failover-groups show;" +
										"network interface failover show;"
[Void]$network_create_mgmt_node_fg_cmds.Add((Build-CmdContent $network_create_mgmt_node_fg_cmd_str))

$network_create_mgmt_cluster_fg_cmd_str = "network interface failover-groups show;" +
											"network interface failover show;"	
[Void]$network_create_mgmt_cluster_fg_cmds.Add((Build-CmdContent $network_create_mgmt_cluster_fg_cmd_str))

$network_create_data_fg_cmd_str = "network interface failover-groups show;" +
									"network interface failover show"
[Void]$network_create_data_fg_cmds.Add((Build-CmdContent $network_create_mgmt_cluster_fg_cmd_str))

if ($ic_enabled)
	{
	[Void]$network_create_ic_fg_cmds.Add("network interface failover-groups show")
	[Void]$network_create_ic_fg_cmds.Add("network interface failover show")
	[Void]$network_create_ic_fg_cmds.Add("")
	
	[Void]$network_create_ic_routing_group_cmds.Add("network interface failover-groups show")
	[Void]$network_create_ic_routing_group_cmds.Add("network interface failover show")
	[Void]$network_create_ic_routing_group_cmds.Add("")
	
	[Void]$network_create_ic_lif_cmds.Add("network interface show")
	[Void]$network_create_ic_lif_cmds.Add("")	
	}

[Void]$network_rename_mgmt_lif_cmds.Add("network interface show")
[Void]$network_rename_mgmt_lif_cmds.Add("")

[Void]$network_modify_mgmt_lif_cmds.Add("network interface show")
[Void]$network_modify_mgmt_lif_cmds.Add("network interface show -failover")
[Void]$network_modify_mgmt_lif_cmds.Add("")

[Void]$network_create_cm_routing_group_cmds.Add("network routing-groups route show")
[Void]$network_create_cm_routing_group_cmds.Add("")

$svm_lif_create_cmd_str = "network interface show"
[Void]$svm_lif_create_cmds.Add((Build-CmdContent $svm_lif_create_cmd_str))

[Void]$ntp_cmds.Add("set -privilege diagnostic")
[Void]$ntp_cmds.Add("system services ntp config show")
[Void]$ntp_cmds.Add("system services ntp config modify -enabled true")
[Void]$ntp_cmds.Add("system services ntp config show")
[Void]$ntp_cmds.Add("set -privilege admin")
[Void]$ntp_cmds.Add("")

[Void]$cdp_cmds.Add("")

[Void]$hardware_assist_cmds.Add("storage failover hwassist show")
[Void]$hardware_assist_cmds.Add("")

[Void]$ping_cmds.Add("")

[Void]$aggr_create_cmds.Add("storage aggregate show")
[Void]$aggr_create_cmds.Add("")

[Void]$aggr_no_snap_cmds.Add("system node run -node * -command snap list -A")
[Void]$aggr_no_snap_cmds.Add("system node run -node * -command snap sched -A")
[Void]$aggr_no_snap_cmds.Add("")

[Void]$svm_remove_mroot_cmds.Add($section_break)
[Void]$svm_remove_mroot_cmds.Add("### Remove mroot aggrs from SVM List")
[Void]$svm_remove_mroot_cmds.Add($section_break)
[Void]$svm_remove_mroot_cmds.Add("")
[Void]$svm_remove_mroot_cmds.Add("vserver show -vserver $svm_name -fields aggr-list")
[Void]$svm_remove_mroot_cmds.Add("vserver modify -vserver $svm_name -aggr-list $aggr_list_str")
[Void]$svm_remove_mroot_cmds.Add("vserver show -vserver $svm_name -fields aggr-list")
[Void]$svm_remove_mroot_cmds.Add("")

[Void]$svm_create_ls_mir_cmds.Add($section_break)
[Void]$svm_create_ls_mir_cmds.Add("### Create SVM root/loadshare mirror schedule")
[Void]$svm_create_ls_mir_cmds.Add($section_break)
[Void]$svm_create_ls_mir_cmds.Add("")
[Void]$svm_create_ls_mir_cmds.Add("schedule interval create -name $svm_ls_mir_sched_name -minutes $svm_ls_mir_sched_mins")
[Void]$svm_create_ls_mir_cmds.Add("")

[Void]$svm_create_ls_mir_cmds.Add($section_break)
[Void]$svm_create_ls_mir_cmds.Add("### Create SVM root/loadshare mirrors")
[Void]$svm_create_ls_mir_cmds.Add($section_break)
[Void]$svm_create_ls_mir_cmds.Add("")

[Void]$svm_snapmirror_ls_mir_cmds.Add($section_break)
[Void]$svm_snapmirror_ls_mir_cmds.Add("### Create root/loadshare mirror snapmirror relationship")
[Void]$svm_snapmirror_ls_mir_cmds.Add($section_break)
[Void]$svm_snapmirror_ls_mir_cmds.Add("")

$svm_ls_mir_counter = 1
foreach ($non_mroot_aggr_line in $non_mroot_aggrs)
	{
	if ($non_mroot_aggr_line -ne $svm_root_aggr_name) 
		{
		$svm_ls_mir_counter_str		= $svm_ls_mir_counter.ToString("00")
		$svm_ls_mir_name			= $svm_root_vol_name + "_ls" + $svm_ls_mir_counter_str
		$svm_ls_mir_src				= $cluster_name + "://" + $svm_name + "/" + $svm_root_vol_name
		$svm_ls_mir_dst				= $cluster_name + "://" + $svm_name + "/" + $svm_ls_mir_name
		[Void]$svm_create_ls_mir_cmds.Add("volume create -vserver $svm_name -volume $svm_ls_mir_name -aggregate $non_mroot_aggr_line -size 1GB -state online -type DP")
		[Void]$svm_snapmirror_ls_mir_cmds.Add("snapmirror create -source-path $svm_ls_mir_src -destination-path $svm_ls_mir_dst -type LS -tries 8 -vserver $svm_name -schedule $svm_ls_mir_sched_name")
		$svm_ls_mir_counter++
		}
	}
[Void]$svm_create_ls_mir_cmds.Add("")

[Void]$svm_snapmirror_ls_mir_cmds.Add("")
[Void]$svm_snapmirror_ls_mir_cmds.Add($section_break)
[Void]$svm_snapmirror_ls_mir_cmds.Add("### Start root/loadshare snapmirror")
[Void]$svm_snapmirror_ls_mir_cmds.Add($section_break)
[Void]$svm_snapmirror_ls_mir_cmds.Add("")
[Void]$svm_snapmirror_ls_mir_cmds.Add("snapmirror initialize-ls-set -source-path $svm_ls_mir_src -foreground true")
[Void]$svm_snapmirror_ls_mir_cmds.Add("snapmirror show -type LS")
[Void]$svm_snapmirror_ls_mir_cmds.Add("")

[Void]$license_cmds.Add("system license show")
[Void]$license_cmds.Add("")

if ($autosupport_enable -eq "enable")
	{
	[Void]$autosupport_cmds.Add("system autosupport invoke -node * -type test -message test")
	[Void]$autosupport_cmds.Add("system autosupport show")
	[Void]$autosupport_cmds.Add("")
	}
else {
	[Void]$autosupport_cmds.Add("Autosupport not enabled, to enable, set variable 'cdot_autosupport_enable' equal to 'enable'")
	}

$all_cmds = $header_summary_cmds + `
			$system_initialize_cmds + `
			$term_rows_0_cmds + `
			$two_node_failover_ha_verify_cmds + `
			$node_rename_cmds + `
			$aggr_rename_cmds + `
			$onboard_tgt_disable_cmds + `
			$onboard_tgt_modify_cmds + `
			$reboot_all_cmds + `
			$ucadmin_show_cmds + `
			$network_create_ifgrp_ten_gbe_cmds + `
			$network_create_ifgrp_one_gbe_cmds + `
			$network_create_vlan_ten_gbe_cmds + `
			$network_create_vlan_one_gbe_cmds + `
			$network_create_mgmt_node_fg_cmds + `
			$network_create_mgmt_cluster_fg_cmds + `
			$network_create_ic_fg_cmds + `
			$network_create_ic_lif_cmds + `
			$network_create_ic_routing_group_cmds + `
			$network_create_data_fg_cmds + `
			$network_rename_mgmt_lif_cmds + `
			$network_modify_mgmt_lif_cmds + `
			$network_create_cm_routing_group_cmds + `
			$timezone_cmds + `
			$ntp_cmds + `
			$snmp_cmds + `
			$cdp_cmds + `
			$hardware_assist_cmds + `
			$network_interface_revert_cmds + `
			$ping_cmds + `
			$license_cmds + `
			$aggr_create_cmds + `
			$aggr_no_snap_cmds + `
			$auto_giveback_cmds + `
			$two_node_failover_ha_disable_enable_cmds + `
			$svm_create_cmds + `
			$svm_lif_create_cmds + `
			$svm_create_ls_mir_cmds + `
			$svm_snapmirror_ls_mir_cmds + `
			$svm_remove_mroot_cmds + `
			$autosupport_cmds + `
			$create_mgmt_users_cmds + `
			$autosupport_cmds
			
Write-Host ""
Write-Host "$section_break"
Write-Host "### Writing cdot_cmd_output_file:"
Write-Host "$section_break"
Write-Host ""
Write-Host -ForegroundColor Green "$cmd_output_file"
Write-Host ""

if ($build_cmds -eq "y")
	{
	$all_cmds | Out-File $cmd_output_file -Encoding ASCII
	}

#endregion

#region Presite/As-Built Documentation

$word_template_name = "cdot_as-built_template.docx"
$word_template_file = Join-Path -Path $cdot_templates_path -ChildPath $word_template_name

if ($build_docs -eq "y")
	{
	$word_rpt_name	= $customer_name_key + "_" + $cluster_name + "_as-built.docx"
	$word_rpt_file	= Join-Path -Path $cdot_reports_path -ChildPath $word_rpt_name
	}
else
	{
	Write-Host "No build_docs specified, exiting..."
	exit
	}

$tmpl_introduction_body_file 	= Join-Path -Path $cdot_templates_path "cdot_introduction_body.txt"
$tmpl_customer_body_file 		= Join-Path -Path $cdot_templates_path "cdot_customer_body.txt"
$tmpl_cluster_body_file 		= Join-Path -Path $cdot_templates_path "cdot_cluster_body.txt"
$tmpl_nodes_body_file 			= Join-Path -Path $cdot_templates_path "cdot_nodes_body.txt"
$tmpl_networks_body_file 		= Join-Path -Path $cdot_templates_path "cdot_networks_body.txt"
$tmpl_ifgrps_body_file 			= Join-Path -Path $cdot_templates_path "cdot_ifgrps_body.txt"
$tmpl_lifs_body_file 			= Join-Path -Path $cdot_templates_path "cdot_lifs_body.txt"

$tmpl_introduction_body = Get-Content -Path $tmpl_introduction_body_file
$tmpl_customer_body 	= Get-Content -Path $tmpl_customer_body_file
$tmpl_cluster_body 		= Get-Content -Path $tmpl_cluster_body_file
$tmpl_nodes_body 		= Get-Content -Path $tmpl_nodes_body_file
$tmpl_networks_body 	= Get-Content -Path $tmpl_networks_body_file
$tmpl_ifgrps_body 		= Get-Content -Path $tmpl_ifgrps_body_file
$tmpl_lifs_body 		= Get-Content -Path $tmpl_lifs_body_file

# Instantiate a new Word object

$word 				= New-Object -ComObject word.application
$word.visible 		= $true
$document 			= $word.documents.add($word_template_file)
$selection			= $word.Selection

SearchAWord $document "DOCUMENT NAME" "cDOT Cluster As-Built"
SearchAWord $document "CUSTOMER NAME" $customer_name
 
$MoveCursorToEnd = $selection.EndKey(6, 0) 
$selection.TypeParagraph() 
$selection.TypeParagraph() 

$selection.Style="Heading 1"
$selection.TypeText("cDOT Cluster As-Built")
$selection.TypeParagraph()

$selection.Style="Heading 2"
$selection.TypeText("Introduction")
$selection.TypeParagraph()
$selection.Style="Normal"
$selection.TypeText("$tmpl_introduction_body")
$selection.TypeParagraph()
$selection.TypeParagraph()

$selection.Style="Heading 2"
$selection.TypeText("Customer Information")
$selection.TypeParagraph()
$selection.Style="Normal"
$selection.TypeText("$tmpl_exec_summary_body")
$selection.TypeParagraph()
$selection.TypeParagraph()
$selection.Style="Normal"
Create-AbWordTables "Customer" $abt_customer

$selection.Style="Heading 2"
$selection.TypeText("Cluster")
$selection.TypeParagraph()
$selection.Style="Normal"
$selection.TypeText("$tmpl_cluster_body")
$selection.TypeParagraph()
$selection.TypeParagraph()
$selection.Style="Normal"
Create-AbWordTables "Cluster" $abt_cluster

$selection.Style="Heading 2"
$selection.TypeText("Nodes")
$selection.TypeParagraph()
$selection.Style="Normal"
$selection.TypeText("$tmpl_nodes_body")
$selection.TypeParagraph()
$selection.TypeParagraph()
$selection.Style="Normal"
#Create-AbWordTables "Nodes" $abt_nodes

$selection.Style="Heading 2"
$selection.TypeText("Networks")
$selection.TypeParagraph()
$selection.Style="Normal"
$selection.TypeText("$tmpl_networks_body")
$selection.TypeParagraph()
$selection.TypeParagraph()
$selection.Style="Normal"
Create-AbWordTables "Networks" $abt_networks

$selection.Style="Heading 2"
$selection.TypeText("Network Interface Groups (IFGRPs)")
$selection.TypeParagraph()
$selection.Style="Normal"
$selection.TypeText("$tmpl_ifgrps_body")
$selection.TypeParagraph()
$selection.TypeParagraph()
$selection.Style="Normal"
Create-AbWordTables "IFGRPs" $abt_ifgrps

$selection.Style="Heading 2"
$selection.TypeText("Network Logical Interfaces (LIFs)")
$selection.TypeParagraph()
$selection.Style="Normal"
$selection.TypeText("$tmpl_lifs_body")
$selection.TypeParagraph()
$selection.TypeParagraph()
$selection.Style="Normal"
Create-AbWordTables "LIFs" $abt_lifs

$table_name = "cDOT Variable Glossary"
Write-Host "Working on $table_name"
$selection.ParagraphFormat.Alignment = 1
# Get table column headers
$t_obj					= $cdot_data.get_Item("cdot_glossary").GetEnumerator() | %{ $_ } | select var_name, var_definition
$table_hdrs				= $t_obj[0].psobject.properties | select Name | %{ $_.Name }
$rows					= 1
$columns				= $table_hdrs.Count
$range 					= $selection.Range
$table					= $document.Tables.add($range,$rows,$columns)
$table.Borders.Enable	= $true	
$selection.InsertCaption(-2, " $table_name") 

# Build table column headers
$y = 1
foreach ($table_hdr in $table_hdrs)
	{
	$Table.Cell(1,$y).Shading.BackgroundPatternColor = $wdColorGray15
	$table.cell(1,$y).Borders.Enable	= $true
	$table.cell(1,$y).Range.Font.Bold 	= $true
	$table.cell(1,$y).Range.Text 	= $table_hdr
	$y++
	}	
	
# Build table body
$x = 2
foreach ($table_row in $t_obj)
	{
	$y = 1
	[Void]$table.rows.Add()
	foreach ($table_hdr in $table_hdrs)
		{
		if ($table_row | select -ExpandProperty $table_hdr)
			{
			$cell_value = $table_row | select -ExpandProperty $table_hdr
			$table.cell($x,$y).Range.Font.Bold 	= $false
			$table.cell($x,$y).range.text 		= $cell_value
			$y++
			}
		else
			{
			$cell_value = "not_specified"
			$table.cell($x,$y).Range.Font.Bold 	= $false
			$table.cell($x,$y).range.text 		= $cell_value
			$y++				
			}
		}
	$x++
	}
$selection.EndKey(6)
$selection.TypeParagraph()
	
if (Test-Path -Path $word_rpt_file)
	{
	Remove-Item $word_rpt_file
	}
#save word document
$saveFormat = [Enum]::Parse([Microsoft.Office.Interop.Word.WdSaveFormat], "wdFormatDocumentDefault");
$document.SaveAs([ref] $word_rpt_file,[ref] 16)
$document.Close()
$word.quit()

#endregion

