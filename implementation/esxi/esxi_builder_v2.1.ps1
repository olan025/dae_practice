Param(
  [Parameter(Mandatory=$True,Position=1)]
  [string]$datasource_name,
  [Parameter(Mandatory=$True,Position=2)]
  [string]$build_id
  
)


### vspherebuilder assumption variables
$esxi_mgmt_build_vmnic = "vmnic0"
$ks_boot_proto = "static"

####################### Script Functional Start
if ($datasource_name -match "xls")
	{
		Export-WSToCSV -datasource_name $datasource_name
		# JA: Added -datasource_name arg to Get-CsvData to utilize its path now that using wrapper script
		$cdata = Get-CsvData -datasource_name $datasource_name -select_all_str "cdot_controllers|cdot_disks" -build_id $build_id
	}
elseif ($datasource_name -match ".db")
	{
		$datasource_name = $database_name + ".mysqldb"
		# JA: Future - Plan to add -datasource_name arg to Get-DBData to utilize so not hardcoding DB name
		$cdata 	= Get-DBData -datasource_name $datasource_name -select_all_str "cdot_controllers|cdot_disks" -build_id $build_id
	}

###### Set Build_ID Data
	$build_id_data = $cdata.get_item("Build_ids")
	$build_id = $build_id_data.build_id
	$customer_name = $build_id_data.customer_name_key
	$esxi_cluster_name = $build_id_data.esxi_cluster_name
	$esxi_host_model	= $build_id_data.esxi_host_model

###### Set Data-Center Level Variables
	$datacenter = $cdata.get_item("datacenter")
	$dns1 = $datacenter.dc_dns1
	$dns2 = $datacenter.dc_dns2
	$domain_name = $datacenter.dc_domain_name
	$search_domains = $datacenter.dc_search_domains
	$ntp1 = $datacenter.dc_ntp1
	$ntp2 = $datacenter.dc_dns2
	$dc_default_vmnet = $datacenter.dc_default_vm_net
	$dc_esxi_enable_ssh = $datacenter.dc_esxi_enable_ssh
	$dc_esxi_enable_shell = $datacenter.dc_esxi_enable_shell
	$dc_esxi_ssh_suppress_warn = $datacenter.dc_esxi_ssh_suppress_warn
	$dc_esxi_shell_suppress_warn = $datacenter.dc_esxi_shell_suppress_warn
	$dc_local_vmfs_prefix = $datacenter.dc_local_vmfs_prefix

###### Grab vSwitch Data
	$vswitch_data = $cdata.get_item("cluster_vswitches")
	$vswitch_total = $vswitch_data.Count

###### Grab vmk_Portgroup Data
	$vmk_port_data = $cdata.get_Item("vmkports")
	$vmk_ports_mgmt = $vmk_port_data | where {$_.vmk_role -eq "management"}
	$vmk_ports_vmo = $vmk_port_data | where {$_.vmk_role -match "vmo"}
	$vmk_ports_nfs = $vmk_port_data | where {$_.vmk_role -eq "nfs"}
	$vmk_ports_iscsi = $vmk_port_data | where {$_.vmk_role -match "iscsi"}
	$vmk_ports_vmotion_total = $vmk_ports_vmo.Count
	if($vmk_ports_vmotion_total -eq $null){$vmk_ports_vmotion_total = 1}
	$vmk_ports_mgmt_total = $vmk_ports_mgmt.Count
	if($vmk_ports_mgmt_total -eq $null){$vmk_ports_mgmt_total = 1}
	$vmk_ports_nfs_total = $vmk_ports_nfs.Count
	if($vmk_ports_nfs_total -eq $null){$vmk_ports_nfs_total = 1}
	$vmk_ports_iscsi_total = $vmk_ports_iscsi.Count
	#if($vmk_ports_iscsi_total -eq $null){$vmk_ports_iscsi_total = 1}
	if($vmk_ports_vmotion_total -gt 1){"MultiNic_vMotion with $vmk_ports_vmotion_total ports defined."}
	Elseif($vmk_ports_vmotion_total -eq 1){"SingleNic_vMotion with $vmk_ports_vmotion_total ports defined."}
	if($vmk_ports_iscsi_total -eq 0){"iSCSI interfaces not found"}
	Elseif($vmk_ports_iscsi -ne 2){"Expected two interfaces for iSCSI, but the number doesn't equal 2"}
	
	# Identify which switch has the mgmt vSwitch role by looking at VMK ports configured for mgmt
	$vswitch_role_management = $vmk_port_data | where {$_.vmk_role -eq "management"}
	if($vswitch_role_management -is [System.Array])
		{
			$vswitch_role_management = ($vswitch_role_management | sort -Property vswitch -Unique).vswitch
			if($vswitch_role_management -is [System.Array])
				{echo "More than one vSwitch defined for Management. Script not built for that yet."}
		}
	Elseif($vswitch_role_management -isnot [System.Array])
		{
			$vswitch_role_management = $vswitch_role_management.vswitch
		}
	
	# Identify which switch has the vMotion vSwitch role by looking at VMK ports configured for vMotion
	$vswitch_role_vmotion = $vmk_port_data | where {$_.vmk_role -match "vmotion"}
	if($vswitch_role_vmotion -is [System.Array])
		{
			$vswitch_role_vmotion = ($vswitch_role_vmotion | sort -Property vswitch -Unique).vswitch
			if($vswitch_role_vmotion -is [System.Array])
				{echo "More than one vSwitch defined for vMotion. Script not built for that yet."}
		}
	Elseif($vswitch_role_vmotion -isnot [System.Array])
		{
			$vswitch_role_vmotion = $vswitch_role_vmotion.vswitch
		}
	
	# Identify which switch has the NFS vSwitch role by looking at VMK ports configured for nfs
	$vswitch_role_nfs = $vmk_port_data | where {$_.vmk_role -eq "nfs"}
	if($vswitch_role_nfs -is [System.Array])
		{
			$vswitch_role_nfs = ($vswitch_role_nfs | sort -Property vswitch -Unique).vswitch
			if($vswitch_role_nfs -is [System.Array])
			{echo "More than one vSwitch defined for NFS. Script not built for that yet."}
		}
	Elseif($vswitch_role_nfs -isnot [System.Array])
		{
			$vswitch_role_nfs = $vswitch_role_nfs.vswitch
		}
	
	# Identify which switch has the iSCSI vSwitch role by looking at VMK ports configured for iSCSI
	$vswitch_role_iscsi = $vmk_port_data | where {$_.vmk_role -match "iscsi"}
	if($vswitch_role_iscsi -is [System.Array])
		{
			$vswitch_role_iscsi = ($vswitch_role_iscsi | sort -Property vswitch -Unique).vswitch
			if($vswitch_role_iscsi -is [System.Array])
			{echo "More than one vSwitch defined for iSCSI. Script not built for that yet."}
		}
	Elseif($vswitch_role_iscsi -isnot [System.Array])
	{
		$vswitch_role_iscsi = $vswitch_role_iscsi.vswitch
	}
######### Define Global Sections for the KS File
	$ks_title							= New-Object System.Collections.ArrayList
	$ks_header							= New-Object System.Collections.ArrayList
	$ks_disk_partitioning				= New-Object System.Collections.ArrayList
	$ks_install_media					= New-Object System.Collections.ArrayList
	$ks_root_password					= New-Object System.Collections.ArrayList
	$ks_default_mgmt_int				= New-Object System.Collections.ArrayList
	$ks_default_mgmt_int_data			= New-Object System.Collections.ArrayList
	$ks_reboot_after_install_flag		= New-Object System.Collections.ArrayList
	$ks_script_pre						= New-Object System.Collections.ArrayList
	$ks_script_post						= New-Object System.Collections.ArrayList
	$ks_script_1st_reboot				= New-Object System.Collections.ArrayList
	$ks_vswitch_config					= New-Object System.Collections.ArrayList
	$ks_portgroup_config				= New-Object System.Collections.ArrayList
	$ks_rename_ds						= New-Object System.Collections.ArrayList
	$ks_remote_ssh_shell				= New-Object System.Collections.ArrayList
	$ks_add_mgmt_vmk0					= New-Object System.Collections.ArrayList
	$ks_advanced_settings				= New-Object System.Collections.ArrayList
	$ks_ntp								= New-Object System.Collections.ArrayList
	$ks_firewall						= New-Object System.Collections.ArrayList
	$ks_search_domains					= New-Object System.Collections.ArrayList
	$ks_wrap_up							= New-Object System.Collections.ArrayList
	$ks_all_cmds						= @()

########### Sets the values for the KS header before looping into the hosts
	[Void]$ks_header.Add("$section_break")
	[Void]$ks_header.Add("$section_break")
	[Void]$ks_header.Add("# Written By: $script_user")
	[Void]$ks_header.Add("# Date: $date_time")
	[Void]$ks_header.Add("# Produced for $customer_name")
	[Void]$ks_header.Add("# Datalink Automation Engineering")
	[Void]$ks_header.Add("$section_break")
	[Void]$ks_header.Add("$section_break")
	[Void]$ks_header.Add("# Accept License agreement")
	[Void]$ks_header.Add("$section_break")
	[Void]$ks_header.Add("vmaccepteula")
	[Void]$ks_header.Add("")

############### Sets the value of the Disk Partitioning section
	[Void]$ks_disk_partitioning.Add("$section_break")
	[Void]$ks_disk_partitioning.Add("# Disk Partitioning")
	[Void]$ks_disk_partitioning.Add("# Clear all partitions in first detected disk and overwrite any VMFS")
	[Void]$ks_disk_partitioning.Add("# partitions on the specified drives.")
	[Void]$ks_disk_partitioning.Add("$section_break")
	[Void]$ks_disk_partitioning.Add("clearpart --firstdisk --overwritevmfs")
	[Void]$ks_disk_partitioning.Add("")
	
######## Set the installation media location
	[Void]$ks_install_media.Add("$section_break")
	[Void]$ks_install_media.Add("# Installation media location")
	[Void]$ks_install_media.Add("$section_break")
	[Void]$ks_install_media.Add("# Fresh installation on first disk and overwrite an existing VMFS datastore")
	[Void]$ks_install_media.Add("install --firstdisk --overwritevmfs")
	[Void]$ks_install_media.Add("")


######## Set the root password 
	[Void]$ks_root_password.Add("$section_break")
	[Void]$ks_root_password.Add("# Root password and Authication format")
	[Void]$ks_root_password.Add("# Default is shadow password enabled, MD5-based passwords enabled")
	[Void]$ks_root_password.Add("# Encrypted Root Password in MD5 format")
	[Void]$ks_root_password.Add("$section_break")
	[Void]$ks_root_password.Add("# root password in MD5 format")
	## Grab the root password from the pre-site in the future
	[Void]$ks_root_password.Add('rootpw --iscrypted $1$.wz1x1am$XP2tidjBqL8wszbyaXdeL/')
	[Void]$ks_root_password.Add("")

######## Set the management interface header
	[Void]$ks_default_mgmt_int.Add("$section_break")
	[Void]$ks_default_mgmt_int.Add("# Set default Management Interface")
	[Void]$ks_default_mgmt_int.Add('# addvmportgroup set to "0" to disable the creation of default guest VM Network"')
	[Void]$ks_default_mgmt_int.Add("$section_break")

######## Set the reboot to not eject
	[Void]$ks_reboot_after_install_flag.Add("$section_break")
	[Void]$ks_reboot_after_install_flag.Add("# Reboot after installation")
	[Void]$ks_reboot_after_install_flag.Add("$section_break")
	[Void]$ks_reboot_after_install_flag.Add("reboot --noeject")
	[Void]$ks_reboot_after_install_flag.Add("")
	
######## Set the prescript section
	[Void]$ks_script_pre.Add("$section_break")
	[Void]$ks_script_pre.Add("# Specifies script to run before the kickstart configuration is evaluated")
	[Void]$ks_script_pre.Add("$section_break")
	[Void]$ks_script_pre.Add("%pre --interpreter=busybox")
	[Void]$ks_script_pre.Add("")
	
######## Set the Postscript
	[Void]$ks_script_post.Add("$section_break")
	[Void]$ks_script_post.Add("# Specifies script to run after ESXi is installed and before reboot")
	[Void]$ks_script_post.Add("$section_break")
	[Void]$ks_script_post.Add("%post --interpreter=busybox --ignorefailure=true")
	[Void]$ks_script_post.Add("")

######## Set the 1st reboot
	[Void]$ks_script_1st_reboot.Add("$section_break")
	[Void]$ks_script_1st_reboot.Add("# Specifies script to run after ESXi installation and after first reboot")
	[Void]$ks_script_1st_reboot.Add("# Most of the shell command will enabled after the first reboot")
	[Void]$ks_script_1st_reboot.Add("$section_break")
	[Void]$ks_script_1st_reboot.Add("%firstboot --interpreter=busybox")
	[Void]$ks_script_1st_reboot.Add("")

######## Set Search domains vim-cmd?
	[Void]$ks_search_domains.Add("$section_break")
	[Void]$ks_search_domains.Add("## Set DNS Search Domains")
	[Void]$ks_search_domains.Add("$section_break")
	[Void]$ks_search_domains.Add("vim-cmd hostsvc/net/dns_set --searchdomain=$search_domains")
	[Void]$ks_search_domains.Add("")



### Research this section for better method, eliminate vim-cmd?
######### Set Remote SSH and Shell
	[Void]$ks_remote_SSH_Shell.Add("$section_break")
	[Void]$ks_remote_SSH_Shell.Add("# Enable Remote SSH")
	[Void]$ks_remote_SSH_Shell.Add("# Enable Local Local Console")
	[Void]$ks_remote_SSH_Shell.Add("$section_break")
	[Void]$ks_remote_SSH_Shell.Add("vim-cmd hostsvc/enable_ssh")
	[Void]$ks_remote_SSH_Shell.Add("vim-cmd hostsvc/start_ssh")
	[Void]$ks_remote_SSH_Shell.Add("vim-cmd hostsvc/enable_esx_shell")
	[Void]$ks_remote_SSH_Shell.Add("vim-cmd hostsvc/start_esx_shell")
	[Void]$ks_remote_SSH_Shell.Add("")
	[Void]$ks_remote_SSH_Shell.Add("# Suppress Shell Warning in Host")
	[Void]$ks_remote_SSH_Shell.Add("esxcli system settings advanced set -o /UserVars/SuppressShellWarning -i 1")
	[Void]$ks_remote_SSH_Shell.Add("esxcli system settings advanced set -o /UserVars/ESXiShellTimeOut -i 1")
	[Void]$ks_remote_SSH_Shell.Add("")

######## Set Mgmt back on vmk0
	[Void]$ks_add_mgmt_vmk0.Add("$section_break")
	## Add Management to VMK0 (find replacement for better method)
	[Void]$ks_add_mgmt_vmk0.Add("$section_break")
	[Void]$ks_add_mgmt_vmk0.Add("/etc/init.d/hostd stop")
	[Void]$ks_add_mgmt_vmk0.Add("sleep 5")
	[Void]$ks_add_mgmt_vmk0.Add("cat > /etc/vmware/hostd/hostsvc.xml << __HOSTSVC_XML__")
	[Void]$ks_add_mgmt_vmk0.Add("<ConfigRoot>")
	[Void]$ks_add_mgmt_vmk0.Add("  <mangementVnics>")
	[Void]$ks_add_mgmt_vmk0.Add('    <nic id="0000">vmk0</nic>')
	[Void]$ks_add_mgmt_vmk0.Add("  </mangementVnics>")
	[Void]$ks_add_mgmt_vmk0.Add("  <mode>normal</mode>")
	[Void]$ks_add_mgmt_vmk0.Add("  <service>")
	[Void]$ks_add_mgmt_vmk0.Add("    <TSM>on</TSM>")
	[Void]$ks_add_mgmt_vmk0.Add("    <TSM-SSH>on</TSM-SSH>")
	[Void]$ks_add_mgmt_vmk0.Add("    <ntpd>on</ntpd>")
	[Void]$ks_add_mgmt_vmk0.Add("  </service>")
	[Void]$ks_add_mgmt_vmk0.Add("</ConfigRoot>")
	[Void]$ks_add_mgmt_vmk0.Add("__HOSTSVC_XML__")
	[Void]$ks_add_mgmt_vmk0.Add("")
	[Void]$ks_add_mgmt_vmk0.Add("/etc/init.d/hostd start")
	[Void]$ks_add_mgmt_vmk0.Add("")
	
	
######## Set NTP 
######## This section likely needs to be updated
	[Void]$ks_ntp.Add("$section_break")
	[Void]$ks_ntp.Add("##  NTP Configuration")
	[Void]$ks_ntp.Add("$section_break")
	[Void]$ks_ntp.Add("cp /etc/ntp.conf /etc/ntp.conf.orig")
	[Void]$ks_ntp.Add("cat << EOF  > /etc/ntp.conf")
	[Void]$ks_ntp.Add("restrict default kod nomodify notrap nopeer")
	[Void]$ks_ntp.Add("restrict 127.0.0.1")
	if($ntp1){[Void]$ks_ntp.Add("server $ntp1")}
	if($ntp2){[Void]$ks_ntp.Add("server $ntp2")}
	[Void]$ks_ntp.Add("driftfile /etc/ntp.drift")
	[Void]$ks_ntp.Add("EOF")
	[Void]$ks_ntp.Add("")
	[Void]$ks_ntp.Add("# Set NTP service settings")
	[Void]$ks_ntp.Add("/sbin/chkconfig --level 345 ntpd on")
	[Void]$ks_ntp.Add("")

######## THIS SECTION NEEDS TO BE REWRITTEN to benefit from powershell, not shell
######## Set Firewall Services
	[Void]$ks_firewall.Add("$section_break")
	[Void]$ks_firewall.Add("##  Enable Firewall")
	[Void]$ks_firewall.Add("$section_break")
	[Void]$ks_firewall.Add("esxcli network firewall set --default-action false --enabled yes")
	[Void]$ks_firewall.Add('FIREWALL_SERVICES="syslog sshClient ntpClient updateManager httpClient netdump"')
	[Void]$ks_firewall.Add('for SERVICE in ${FIREWALL_SERVICES}')
	[Void]$ks_firewall.Add("do")
	[Void]$ks_firewall.Add("esxcli network firewall ruleset set --ruleset-id ${SERVICE} --enabled yes")
	[Void]$ks_firewall.Add("done")
	[Void]$ks_firewall.Add("")
	
######## Set Wrap Up stuff
######## This Section needs to be updated for the new way we do things here!
	[Void]$ks_wrap_up.Add("$section_break")
	[Void]$ks_wrap_up.Add("##  Put to Maintenance Mode, backup logs and Reboot")
	[Void]$ks_wrap_up.Add("$section_break")
	[Void]$ks_wrap_up.Add("# backup ESXi configuration to persist changes")
	[Void]$ks_wrap_up.Add("/sbin/auto-backup.sh")
	[Void]$ks_wrap_up.Add("# enter maintenance mode")
	[Void]$ks_wrap_up.Add("# vim-cmd hostsvc/maintenance_mode_enter")
	[Void]$ks_wrap_up.Add("# copy %first boot script logs to persisted datastore")
	[Void]$ks_wrap_up.Add('cp /var/log/hostd.log "/vmfs/volumes/$(hostname -s)_datastore1/firstboot-hostd.log"')
	[Void]$ks_wrap_up.Add('cp /var/log/esxi_install.log "/vmfs/volumes/local_$(hostname -s)/firstboot-esxi_install.log"')
	[Void]$ks_wrap_up.Add("reboot")
	[Void]$ks_wrap_up.Add("$section_break")
	[Void]$ks_wrap_up.Add("##  End of kickstart Script")
	[Void]$ks_wrap_up.Add("$section_break")

############################ Each host Section ############################
Foreach($esxi_host in $cdata.get_Item("hosts"))
{	
	# Clear objects for host loop
	$ks_title.Clear()
	$ks_vswitch_Config.Clear()
	$ks_portgroup_Config.Clear()
	$ks_rename_ds.Clear()
	$ks_default_mgmt_int_data.Clear()
	# Set variable values per host
	# set the host specific values 
	### Might have to rewrite this section to remove roles from vmks
	$esxi_hostname_fqdn = $esxi_host.esxi_hostname_fqdn
	$esxi_hostname_short = $esxi_hostname_fqdn.Split(".")[0]
	$esxi_vmk0_mgmt_ip	= $esxi_host.esxi_vmk0_ip
	$esxi_vmk0_mgmt_nm	= $esxi_host.esxi_vmk0_nm
	$esxi_vmk0_mgmt_gw	= $esxi_host.esxi_vmk0_gw
	
		
	[Void]$ks_title.Add("$section_break")
	[Void]$ks_title.Add("### Kickstart Created by Datalink Automation Engineering")
	[Void]$ks_title.Add("### Hostname: $esxi_hostname_fqdn")
	[Void]$ks_title.Add("$section_break")
	
	######## Rename the local datastore
	[Void]$ks_rename_ds.Add("$section_break")
	[Void]$ks_rename_ds.Add("# Rename datastore1 to the prefix loaded from the system")
	[Void]$ks_rename_ds.Add("$section_break")
	[Void]$ks_rename_ds.Add("vim-cmd hostsvc/datastore/rename datastore1 " + $dc_local_vmfs_prefix + $esxi_hostname_short)
	[Void]$ks_rename_ds.Add("")
	
	# Perform variable validation or calculation for advanced options for HOST LOOP
	#### need to modify to remove hard code vmnic1 for vSwitch0 configuration.
	#### Thi entire section may be rewritten pending on how things go with new modular automation requests.
	#### Don't tell me how to do it...yet
	Foreach($vswitch in $vswitch_data)
	{
	
		$vswitch_name = $vswitch.esxi_vswitch_name
		$vswitch_ports = $vswitch.esxi_vswitch_num_ports
		$vswitch_mtu = $vswitch.esxi_vswitch_mtu
		$vswitch_act_uplinks = $vswitch.esxi_vswitch_active_uplinks
		$vswitch_act_uplinks_list = $vswitch_act_uplinks.Split(",")
		$vswitch_cdp_status = $vswitch.cdp_status
		[Void]$ks_vswitch_config.Add("$section_break")
		[Void]$ks_vswitch_config.Add("#### Start $vswitch_name section")
		[Void]$ks_vswitch_config.Add("$section_break")
		# This should change the logic on how it grabs the vSwitch for Management interface.
		# rewrite that and the vmnic1 hard code.
		if($vswitch_name -eq "vSwitch0")
			{
				# vSwitch0 get's special treatment - as the installation nic is already added (vmnic0 typically)
				[Void]$ks_vswitch_config.Add("esxcli network vswitch standard set --ports $vswitch_ports --vswitch-name $vswitch_name")
				[Void]$ks_vswitch_config.Add("esxcli network vswitch standard uplink add --uplink-name vmnic1 --vswitch-name $vswitch_name")
				[Void]$ks_vswitch_config.Add("esxcli network vswitch standard policy failover set --active-uplinks $vswitch_act_uplinks --vswitch-name $vswitch_name")
				[Void]$ks_vswitch_config.Add("esxcli network vswitch standard policy failover set --failure-detection link --failback yes --notify-switches yes --vswitch-name $vswitch_name")

			}
		Else 
			{
				[Void]$ks_vswitch_config.Add("esxcli network vswitch standard add --ports $vswitch_ports --vswitch-name $vswitch_name")
				Foreach($vswitch_uplink in $vswitch_act_uplinks_list)
					{
						[Void]$ks_vswitch_config.Add("esxcli network vswitch standard uplink add --uplink-name $vswitch_uplink --vswitch-name $vswitch_name")
					}
				[Void]$ks_vswitch_config.Add("esxcli network vswitch standard policy failover set --active-uplinks $vswitch_act_uplinks --vswitch-name $vswitch_name")
				[Void]$ks_vswitch_config.Add("esxcli network vswitch standard policy failover set --failure-detection link --failback yes --notify-switches yes --vswitch-name $vswitch_name")
				[Void]$ks_vswitch_config.Add("# esxcli network vswitch standard policy security set --allow-forged-transmits yes --allow-mac-change yes --allow-promiscuous no --vswitch-name $vswitch_name")

			
			}
		[Void]$ks_vswitch_config.Add("esxcli network vswitch standard set --mtu $vswitch_mtu --cdp-status $vswitch_cdp_status --vswitch-name $vswitch_name")
		[Void]$ks_vswitch_config.Add("$section_break")
		[Void]$ks_vswitch_config.Add("#### End $vswitch_name section")
		[Void]$ks_vswitch_config.Add("$section_break")
		[Void]$ks_vswitch_config.Add("")
	}
	
	
		
	####### VMK Portgroup Section
	# Set Default Management VLAN and add the second vmnic to the management port as active / active.
	## Rewrite VLAN for management portion
	######### Lots of Rewrite options here
		[Void]$ks_portgroup_config.Add("$section_break")
		[Void]$ks_portgroup_config.Add("# Configure Management Network failover policy since adding vmnic to vSwitch0")
		[Void]$ks_portgroup_config.Add("# Set policy to active / active")
		[Void]$ks_portgroup_config.Add("# REM this line out if you want vmk0 ACTIVE / STANDBY with vmnic0 ACTIVE")
		[Void]$ks_portgroup_config.Add("$section_break")

	
	# If multi-nic vmotion is used, then we loop and pin the vmk's on the vmotion switch
	Foreach($vmk_port in $vmk_port_data)
		{
			if($vmk_port.vmk_num -eq "vmk0")
			{
				[Void]$ks_default_mgmt_int.Add("")
				[Void]$ks_portgroup_config.Add('esxcli network vswitch standard portgroup policy failover set -p "Management Network" -a vmnic0,vmnic1')
								# Set command strings into sections with resultant values
				$vmk_vlan = $vmk_port.vmk_vlan
				if($vmk_vlan -eq "")
					{
						[Void]$ks_default_mgmt_int_data.Add("network --bootproto=$ks_boot_proto --device=$esxi_mgmt_build_vmnic --ip=$esxi_vmk0_mgmt_ip --netmask=$esxi_vmk0_mgmt_nm --gateway=$esxi_vmk0_mgmt_gw --hostname=$esxi_hostname_fqdn --nameserver=$dns1,$dns2 --addvmportgroup=0")
						[Void]$ks_portgroup_config.Add('# needs update for DAF # esxcli network vswitch standard portgroup set -p "Management Network"')
					}
				elseif($vmk_vlan -ne "")
					{
						[Void]$ks_default_mgmt_int_data.Add("network --bootproto=$ks_boot_proto --device=$esxi_mgmt_build_vmnic --ip=$esxi_vmk0_mgmt_ip --netmask=$esxi_vmk0_mgmt_nm --gateway=$esxi_vmk0_mgmt_gw --vlan=$vmk_vlan --hostname=$esxi_hostname_fqdn --nameserver=$dns1,$dns2 --addvmportgroup=0")
						[Void]$ks_portgroup_config.Add('esxcli network vswitch standard portgroup set -p "Management Network"' + "--vlan-id=$vmk_vlan")
					}

				[Void]$ks_portgroup_config.Add("")
			}
			Else
			{
				$vmk_name = $vmk_port.vmk_num
				$vmk_ip = ($esxi_host | select ("esxi_"+"$vmk_name"+"_ip")).("esxi_"+"$vmk_name"+"_ip")
				$vmk_nm = ($esxi_host | select ("esxi_"+"$vmk_name"+"_nm")).("esxi_"+"$vmk_name"+"_nm")
				$vmk_pg_name = $vmk_port.vmk_pg_name
				$vmk_mtu = $vmk_port.vmk_mtu
				$vmk_vlan = $vmk_port.vmk_vlan
				$vmk_role = $vmk_port.vmk_role
				$vmk_vswitch = $vmk_port.vswitch
				[Void]$ks_portgroup_config.Add("$section_break")
				[Void]$ks_portgroup_config.Add("# $vmk_role vmkernel $vmk_name")
				[Void]$ks_portgroup_config.Add("esxcli network vswitch standard portgroup add --portgroup-name $vmk_pg_name --vswitch-name $vmk_vswitch")
				[Void]$ks_portgroup_config.Add("esxcli network ip interface add --interface-name $vmk_name --mtu $vmk_mtu --portgroup-name $vmk_pg_name")
				if($vmk_vlan -ne $null){[Void]$ks_portgroup_config.Add("esxcli network vswitch standard portgroup set --portgroup-name=$vmk_pg_name --vlan-id=$vmk_vlan")}
				Elseif($vmk_vlan -eq $null){[Void]$ks_portgroup_config.Add("esxcli network vswitch standard portgroup set --portgroup-name=$vmk_pg_name")}
				[Void]$ks_portgroup_config.Add("esxcli network ip interface ipv4 set --interface-name $vmk_name --ipv4 $vmk_ip --netmask $vmk_nm --type static")
				if($vmk_role -match "vmotion"){[Void]$ks_portgroup_config.Add("vim-cmd hostsvc/vmotion/vnic_set $vmk_name")}
				$vswitch_failover_cmd = vSwitch-FailoverCmd $vmk_port
				[Void]$ks_portgroup_config.Add("$vswitch_failover_cmd")
			}
		}
	



	
	# Combine the sections to form the output
	$ks_all_cmds = $ks_title + 
	$ks_header + 
	$ks_disk_partitioning + $ks_install_media +
	$ks_root_password +
	$ks_default_mgmt_int +
	$ks_default_mgmt_int_data +
	$ks_reboot_after_install_flag +
	$ks_script_pre +
	$ks_script_post +
	$ks_script_1st_reboot +
	$ks_vswitch_config +
	$ks_portgroup_config +
	$ks_rename_ds +
	$ks_remote_ssh_shell +
	$ks_add_mgmt_vmk0 +
	$ks_advanced_settings +
	$ks_ntp +
	$ks_firewall +
	$ks_search_domains +
	$ks_wrap_up +
	$ks_all_cmds +

# output the file
# JA: modified out-file path to work with wrapper, now using $vmware_cmds_path defined in ..\global_environmentals.ps1
$ks_all_cmds | Out-File "$vmware_cmds_path\$esxi_hostname_fqdn.cfg"

# Clear up host specific stuff
	[Void]$ks_default_mgmt_int.Remove("network --bootproto=$ks_boot_proto --device=$esxi_mgmt_build_vmnic --ip=$esxi_vmk0_mgmt_ip --netmask=$esxi_vmk0_mgmt_nm --gateway=$esxi_vmk0_mgmt_gw --hostname=$esxi_hostname_fqdn --nameserver=$dns1,$dns2 --addvmportgroup=0")
	[Void]$ks_default_mgmt_int.Remove("")
}

