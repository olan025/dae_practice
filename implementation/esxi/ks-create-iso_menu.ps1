$header = "DEFAULT menu.c32
MENU TITLE ESXi-5.0.0-469512-standard Boot Menu
NOHALT 1
PROMPT 0
TIMEOUT 80
LABEL install
  KERNEL mboot.c32
  APPEND -c boot.cfg
  MENU LABEL ESXi-5.0.0-469512-standard ^Installer
LABEL hddboot
  LOCALBOOT 0x80
  MENU LABEL ^Boot from local disk"

$label1 = "LABEL -
        menu label ^Anheuser-Busch Installs: Written by Datalink
        menu disable"

$label2 = "LABEL - Production Hosts
        menu label ^Production Hosts:
        menu disable"
$hosts = ipcsv .\hosts.csv
$outfile = ".\menu.txt"
$date = date
echo "#$date" | Out-File $outfile 
Foreach($hostname in $hosts)
{
	$hoststring = ($hostname.host).ToUpper()
	echo "LABEL $hoststring" | Out-File $outfile -Append
	echo " menu label `"$hoststring`" "| Out-File $outfile -Append
	echo " menu indent 2" | Out-File $outfile -Append
	echo " kernel mboot.c32" | Out-File $outfile -Append
	echo " append vmkboot.gz ks=cdrom:/KS/$hoststring.CFG --- vmkernel.gz --- sys.vgz --- cim.vgz --- ienviron.vgz --- install.vgz" | Out-File $outfile -Append
	echo "" | Out-File $outfile -Append
}

