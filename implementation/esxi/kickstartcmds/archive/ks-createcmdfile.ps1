$hosts = ipcsv .\hosts.csv
$commandfile = ".\generatedcommands.txt"
$copytemplatecmds = @()
echo "# Powershell generated ks-makefile" | Out-File $commandfile
$date = date
echo "# $date" | Out-File $commandfile -Append
Foreach($hostname in $hosts)
{
	"" | out-file $commandfile -Append
	$hoststring = ($hostname.host).ToUpper()
	$hostvmk0ip = $hostname.vmk0
	$hostvmk1ip = $hostname.vmk1
	echo "#### HOST: $hoststring" | Out-File $commandfile -Append
	$hostfilename = $hoststring + ".CFG"
	$copytempcmd = "cp ./template-UCS.CFG $hoststring.CFG"
	$copytempcmd | Out-File $commandfile -Append
	$replacehostcmd = "sed -i '' -e 's/template/$hoststring/g' $hostfilename" #| Out-File $commandfile -Append
	$replacehostcmd | Out-File $commandfile -Append
	$replacevmk0ipcmd = "sed -i '' -e 's/vmk0ip/$hostvmk0ip/g' $hostfilename" #| Out-File $commandfile -Append
	$replacevmk0ipcmd | Out-File $commandfile -Append
	$replacevmk1ipcmd = "sed -i '' -e 's/vmk1ip/$hostvmk1ip/g' $hostfilename" #| Out-File $commandfile -Append
	$replacevmk1ipcmd | Out-File $commandfile -Append
	
}