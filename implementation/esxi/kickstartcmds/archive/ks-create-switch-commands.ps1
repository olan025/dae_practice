
# Simple script to create ESXCLI commands for creating portgroups on vSphere hosts.
# Requires a portgrouplist.csv with "portgroup","vSwitchX","vlanID" as the headers
# outputs a file called portgroupcmds.txt which can be copied to clipboard, and pasted into a putty SSH session to the host
# Written: ryan grendahl - 11/13/13

$filename = ".\portgrouplist.csv"
$outputfile = ".\portgroupcmds.txt"
$portgrouplist = ipcsv $filename

$date | Out-File -FilePath $outputfile
Foreach($portgroup in $portgrouplist)
{

	$portgroupName = $portgroup.portgroup
	$vSwitchX = $portgroup.vSwitch
	$vlanid = $portgroup.vlanid
	$createcmd = "esxcli network vswitch standard portgroup add --portgroup-name=""$portgroupname"" --vswitch-name=$vSwitchX"
	$vlancmd = "esxcli network vswitch standard portgroup set --portgroup-name=""$portgroupname"" --vlan-id=$vlanid"
	$createcmd | Out-File -Append -FilePath $outputfile
	$vlancmd | Out-File -Append -FilePath $outputfile
}
