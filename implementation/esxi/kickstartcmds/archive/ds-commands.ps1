$datastores = ipcsv ".\nfs-datastores.csv"
$dscommandfile = ".\dscommands.txt"
Foreach($datastore in $datastores)
{
	$dsname = $datastore.dsname
	$dsshare = $datastore.sharepath
	$dstarget = $datastore.targetip
	$dscommand = "esxcfg-nas -a -o $dstarget -s $dsshare $dsname"
	$dscommand | Out-File $dscommandfile -Append

}