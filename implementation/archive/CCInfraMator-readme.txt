# CC wrapper script
.\CCInfraMator_v2.0.ps1 -build_cmds [y|n] -build_docs [y|n] -build_id [build_id_number]

# Synopsis
# CCInfraMator_v2.0.ps1 extracts CSVs from global_presite_v2.0.xls
# If global_build_ids content for esxi_cluster_name, ucs_domain_name, nexus_stack_name, cdot_cluster_name
# not set to "not_used", the script uses those values to call the module build script
 
# global_presite.xls
 
################################################################################
### Table: global_build_ids.csv
################################################################################
# build_id
# customer_id
# location_id
# datacenter_id
# esxi_cluster_name
# esxi_host_model
# ucs_domain_name
# ucs_host_profile
# nexus_stack_name
# cdot_cluster_name
# cdot_node_model
################################################################################

################################################################################
### Table: global_customers.csv
################################################################################
# build_id
# customer_id
# customer_name
# contact_name
# contact_email
# contact_phone
################################################################################

################################################################################
### Table: global_datacenters.csv
################################################################################
# datacenter_id
# domain_name
# dns_server_ips
# ntp_server_names
# smtp_server_name
# snmp_server_name
# snmp_community_string
# timezone
# default_admin_user
# default_admin_pass
################################################################################

################################################################################
### Table: global_locations.csv
################################################################################
# location_id
# customer_address
# system_install_address
# rma_address
# rma_attn_to_name
################################################################################

################################################################################
### Table: global_networks.csv
################################################################################
# build_id
# network_label
# vlan_id
# subnet
# netmask
# gateway
# ip_address_start
# ip_address_count
################################################################################


 
 