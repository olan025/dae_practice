Function vSwitch-FailoverCmd ($vmk_port)
		{
			$vmk_act_uplinks = $vmk_port.vmk_act_uplinks
			$vmk_stby_uplinks = $vmk_port.vmk_stby_uplinks
			$vmk_unsd_uplinks = $vmk_port.vmk_unsd_uplinks
			$failover_set_cmd = "esxcli network vswitch standard portgroup policy failover set -p $vmk_pg_name"
			If($vmk_act_uplinks)
				{
					$failover_set_cmd = $failover_set_cmd + " -a $vmk_act_uplinks"
				}
			If($vmk_stby_uplinks)
				{
					$failover_set_cmd = $failover_set_cmd + " -s $vmk_stby_uplinks"
				}
			If($vmk_unsd_uplinks)
				{
					$failover_set_cmd = $failover_set_cmd + " -u $vmk_unsd_uplinks"
				}
			Return $failover_set_cmd
		}

function Export-WSToCSV
{	
	Param(
	  [Parameter(Mandatory=$True,Position=1)]
	  [string]$datasource_name
	)

	$excel_workbook_file 	= Split-Path -Path $datasource_name -Leaf
	$conf_base 				= Split-Path -Path $datasource_name
	$conf_path				= Join-Path -Path $conf_base -ChildPath "conf"
	
	$E = New-Object -ComObject Excel.Application
    $E.Visible 			= $false
    $E.DisplayAlerts 	= $false
	if (Test-Path -Path $datasource_name)
		{
    	$wb = $E.Workbooks.Open($datasource_name)
		}
	else
		{
		Write-Host -ForegroundColor Red "Excel workbook not found, expected: $datasource_name"
		exit
		}
	Write-Host ""
	Write-Host "$section_break"
	Write-Host "### Exporting XLS worksheets to CSV files"
	Write-Host "$section_break"
	Write-Host ""
	
    foreach ($ws in $wb.Worksheets)
    {
     	$csv_file_name = $ws.Name + ".csv"
		$csv_file_path = Join-Path $conf_path -ChildPath $csv_file_name
		Write-Host -ForegroundColor Green "$excel_workbook_file => $csv_file_path"
        $ws.SaveAs($csv_file_path, 6)
    }
    $E.Quit()
	Stop-Process -ProcessName EXCEL
}

function Get-MySQLData 
	{
	Param(
	  [Parameter(Mandatory=$True,Position=1)]
	  [string]$table_name,
	  [Parameter(Mandatory=$False,Position=2)]
	  [int]$build_id

	)

	[void][system.reflection.Assembly]::LoadFrom("C:\Program Files (x86)\MySQL\MySQL Connector Net 6.9.3\Assemblies\v4.5\MySQL.Data.dll")

	$MySQLAdminUserName = 'root'
	$MySQLDatabase 		= $database_name
	$MySQLHost 			= '127.0.0.1'
	
	if ($build_id)
		{
		$Query = "select * from " + $table_name + " where build_id = " + $build_id
		}
	else
		{
		$Query = "select * from " + $table_name
		}
	#$ConnectionString = "server=" + $MySQLHost + ";port=3306;uid=" + $MySQLAdminUserName + ";pwd=" + $MySQLAdminPassword + ";database="+$MySQLDatabase		
	$ConnectionString = "server=" + $MySQLHost + ";port=3306;uid=" + $MySQLAdminUserName + ";database="+$MySQLDatabase

	Try {
	  [void][System.Reflection.Assembly]::LoadWithPartialName("MySql.Data")
	  $Connection = New-Object MySql.Data.MySqlClient.MySqlConnection
	  $Connection.ConnectionString = $ConnectionString
	  $Connection.Open()

	  $Command = New-Object MySql.Data.MySqlClient.MySqlCommand($Query, $Connection)
	  $DataAdapter = New-Object MySql.Data.MySqlClient.MySqlDataAdapter($Command)
	  $DataSet = New-Object System.Data.DataSet
	  $RecordCount = $dataAdapter.Fill($dataSet, "data")
	  $DataSet.Tables[0]
	  }

	Catch {
	  Write-Host "ERROR : Unable to run query : $query `n$Error[0]"
	 }

	Finally {
	  $Connection.Close()
	  }
	}
	
function Get-DBData
	{
	Param(
	  [Parameter(Mandatory=$True,Position=1)]
	  [int]$build_id
	)
	$cdotcfg.build_id 		= Get-MySQLData -table_name "build_ids" -build_id $build_id
	$cdotcfg.customer 		= Get-MySQLData -table_name "customers" -build_id $build_id
	$cdotcfg.networks 		= Get-MySQLData -table_name "networks" -build_id $build_id
	$cdotcfg.cluster 		= Get-MySQLData -table_name "clusters" -build_id $build_id
	$cdotcfg.nodes 			= Get-MySQLData -table_name "nodes" -build_id $build_id
	$cdotcfg.aggrs 			= Get-MySQLData -table_name "aggrs" -build_id $build_id
	$cdotcfg.ifgrps 		= Get-MySQLData -table_name "ifgrps" -build_id $build_id
	$cdotcfg.svms 			= Get-MySQLData -table_name "svms" -build_id $build_id
	$cdotcfg.lifs 			= Get-MySQLData -table_name "lifs" -build_id $build_id
	$cdotcfg.controllers 	= Get-MySQLData -table_name "controllers"
	$cdotcfg.disks 			= Get-MySQLData -table_name "disks"
	$cdotcfg
	}

function Get-CsvData
	{
	Param(
	  [Parameter(Mandatory=$True,Position=1)]
	  [string]$datasource_name,
	  [Parameter(Mandatory=$True,Position=2)]
	  [string]$select_all_str,
	  [Parameter(Mandatory=$True,Position=2)]
	  [int]$build_id
	)
	$conf_base 				= Split-Path -Path $datasource_name
	$conf_path				= Join-Path -Path $conf_base -ChildPath "conf"
	
	$cfgdata 	= @{}
	$csv_files 	= (Get-ChildItem -Path $conf_path | where { $_.Name -match ".csv$" })| %{ $_.Name }
	foreach ($csv_file in $csv_files)
		{
		$csv_file_path 	= Join-Path -Path $conf_path -ChildPath $csv_file
		$csv_key 		= $csv_file.Replace(".csv","")
		if ($csv_key -notmatch $select_all_str)
			{
			$csv_data 		= Import-Csv -Path $csv_file_path | where { $_.build_id -eq $build_id }
			}
		else
			{
			$csv_data 		= Import-Csv -Path $csv_file_path
			}
		$cfgdata.Add($csv_key, $csv_data)		
		}
	$cfgdata
	}

function Get-TableDefs ($datasource_name)
	{
	$base_path 				= Split-Path -Path $datasource_name
	$datasource_basename 	= Split-Path -Path $datasource_name -Leaf
	$conf_path				= Join-Path -Path $base_path -ChildPath "conf"
	$reports_path			= Join-Path -Path $base_path -ChildPath "reports"
	$csv_str				= Join-Path -Path $conf_path -ChildPath "*.csv"
		
	$table_defs_name = $datasource_basename + "_table_defs.txt"
	$table_defs_file = Join-Path -Path $reports_path -ChildPath $table_defs_name
	$table_defs = New-Object System.Collections.ArrayList
	(Get-ChildItem -Path $csv_str | select Name) |
		%{ 
		$table_name				= $_.Name
		$table_file				= Join-Path -Path $conf_path -ChildPath $table_name
		$table_name_str			= "### Table: " + $table_name
		[Void]$table_defs.Add($section_break)
		[Void]$table_defs.Add($table_name_str)
		[Void]$table_defs.Add($section_break)
		$column_names_str 		= Get-Content -Path $table_file | select -First 1
		$column_names 			= $column_names_str.Split(",")
		foreach ($column_name in $column_names)
			{
			$column_name_str = "# " + $column_name
			[Void]$table_defs.Add($column_name_str)
			}
		[Void]$table_defs.Add($section_break)
		[Void]$table_defs.Add("")
		}
	Write-Host ""	
	Write-Host "$section_break"
	Write-Host "### Writing cdot_table_defs_file:"
	Write-Host "$section_break"
	Write-Host ""
	Write-Host -ForegroundColor Green "$table_defs_file"
	$table_defs | Out-File $table_defs_file -Encoding ASCII
	}

function Get-MgmtIPs 
	{
	Param(
	  [Parameter(Mandatory=$True,Position=1)]
	  [string]$network_label,
	  [Parameter(Mandatory=$False,Position=2)]
	  [int]$node_number,
	  [Parameter(Mandatory=$False,Position=3)]
	  [int]$node_count

	)	
	if ($network_label -eq "mgmt_cluster")
		{
		$mgmt_ip_cluster_subnet_str 		= ($cdot_networks | where { $_.network_label -eq "mgmt_cluster" } | select subnet).subnet
		$mgmt_ip_cluster_subnet_octets		= $mgmt_ip_cluster_subnet_str.Split(".")
		$mgmt_ip_cluster_subnet_octets[3] 	= ($cdot_networks | where { $_.network_label -eq "mgmt_cluster" } | select ip_address_start).ip_address_start
		$mgmt_ip_cluster					= $mgmt_ip_cluster_subnet_octets -join "."
		$mgmt_ip_cluster
		}	
	elseif (($network_label -eq "mgmt_node") -and ($node_number -ge 1) -and ($node_count -ge 1))
		{
		$mgmt_ip_node_subnet_str 		= ($cdot_networks | where { $_.network_label -eq "mgmt_node" } | select subnet).subnet
		$mgmt_ip_node_subnet_octets		= $mgmt_ip_node_subnet_str.Split(".")
		if ($node_number -eq 1)
			{
			[int]$mgmt_ip_node_start		= ($cdot_networks | where { $_.network_label -eq "mgmt_node" } | select ip_address_start).ip_address_start
			$mgmt_ip_node_subnet_octets[3] 	= $mgmt_ip_node_start.ToString()
			$mgmt_ip_node					= $mgmt_ip_node_subnet_octets -join "."
			[int]$node_counter 				= 1
			while ($node_counter -le $node_count)
				{
				$node_mgmt_ip_lookup.Add($node_counter,$mgmt_ip_node)
				$mgmt_ip_node_start++
				$node_counter++
				$mgmt_ip_node_subnet_octets[3] 	= $mgmt_ip_node_start.ToString()
				$mgmt_ip_node					= $mgmt_ip_node_subnet_octets -join "."
				}
			$node_mgmt_ip_lookup.get_Item(1)
			}
		else 
			{
			$node_mgmt_ip_lookup.get_Item($node_number)
			}
		}
	elseif (($network_label -eq "mgmt_sp") -and ($node_number -ge 1) -and ($node_count -ge 1))
		{
		$mgmt_ip_sp_subnet_str 		= ($cdot_networks | where { $_.network_label -eq "mgmt_sp" } | select subnet).subnet
		$mgmt_ip_sp_subnet_octets	= $mgmt_ip_sp_subnet_str.Split(".")
		if ($node_number -eq 1)
			{
			[int]$mgmt_ip_sp_start		= ($cdot_networks | where { $_.network_label -eq "mgmt_sp" } | select ip_address_start).ip_address_start
			$mgmt_ip_sp_subnet_octets[3] 	= $mgmt_ip_sp_start.ToString()
			$mgmt_ip_sp					= $mgmt_ip_sp_subnet_octets -join "."
			[int]$node_counter 				= 1
			while ($node_counter -le $node_count)
				{
				$sp_mgmt_ip_lookup.Add($node_counter,$mgmt_ip_sp)
				$mgmt_ip_sp_start++
				$node_counter++
				$mgmt_ip_sp_subnet_octets[3] 	= $mgmt_ip_sp_start.ToString()
				$mgmt_ip_sp						= $mgmt_ip_sp_subnet_octets -join "."
				}
			$sp_mgmt_ip_lookup.get_Item(1)
			}
		else 
			{
			$sp_mgmt_ip_lookup.get_Item($node_number)
			}
		}
	else
		{
			"Invalid arguments, confirm node_number passed for node based IPs"
		}
	}

function Get-NodeDefInfo 
	{
		Param(
		  [Parameter(Mandatory=$True,Position=1)]
		  [string]$cluster_name,
		  [Parameter(Mandatory=$True,Position=2)]
		  [int]$node_number
			)	
	if ($node_number -eq 1)
		{
		$pair_number				= $node_number
		$def_node_name 				= $cluster_name_nodash + "1a"
		$def_node_mroot_aggr_name 	= "aggr0"
		}	
	elseif ($node_number % 2 -ne 0)
		{
		$pair_number 				= (($node_number + 1) / 2)
		$node_number_str 			= $node_number.ToString("00")
		$def_node_name 				= $cluster_name_nodash + $pair_number + "a"
		$def_node_mroot_aggr_name 	= "aggr0_" + $cluster_name + "_" + $node_number_str + "_0"
		}
	else
		{
		$pair_number 				= ($node_number / 2)
		$node_number_str 			= $node_number.ToString("00")
		$def_node_name 				= $cluster_name_nodash + $pair_number + "b"
		$def_node_mroot_aggr_name 	= "aggr0_" + $cluster_name + "_" + $node_number_str + "_0"
		}
	$def_node_name
	$def_node_mroot_aggr_name
	}

function Get-NetworkBits
	{
	Param(
	  [Parameter(Mandatory=$True,Position=1)]
	  [string]$netmask
		)
			
	function toBinary ($dottedDecimal){
	 $dottedDecimal.split(".") | %{$binary=$binary + $([convert]::toString($_,2).padleft(8,"0"))}
	 $binary
	}

	$netmask_bin 	= toBinary $netmask
	$network_bits 	= $netmask_bin.indexOf("0")
	$network_bits
	}

function Get-RoutingGroup 
	{
		Param(
		  [Parameter(Mandatory=$True,Position=1)]
		  [string]$role,
		  [Parameter(Mandatory=$True,Position=2)]
		  [string]$subnet,
		  [Parameter(Mandatory=$True,Position=3)]
		  [string]$netmask

			)
	switch ($role)
		{

		"cluster" 		{ $rg_prefix = "c" }
		"cluster-mgmt" 	{ $rg_prefix = "c" }
		"data" 			{ $rg_prefix = "d" }
		"intercluster" 	{ $rg_prefix = "i" }
		"node-mgmt" 	{ $rg_prefix = "n" }
		default			{ "Invalid role specified ($role), valid roles are: [ cluster | cluster-mgmt | data | intercluster | node-mgmt ]" }
		}		
		$network_bits 	= Get-NetworkBits -netmask $netmask
		$rg_str 		= $rg_prefix + $subnet + "/" + $network_bits
		$rg_str
	}
	
function Build-CmdHeader
	{
	Param(
		[Parameter(Mandatory=$True,Position=1)]
		[string]$ch_str
  		)
	$ch_break 		= "# " + "-" * 78
	$ch_contents 	= New-Object System.Collections.ArrayList
	[Void]$ch_contents.Add($ch_break)
	
	if ($ch_str -match ";")
		{
		$ch_strs = $ch_str.Split(";")
		$ch_strs | %{
			$ch_txt_line = "# + " + $_
			[Void]$ch_contents.Add($ch_txt_line)
			}
		[Void]$ch_contents.Add($ch_break)
		[Void]$ch_contents.Add("")
		}
	else
		{
		$ch_txt_line = "# + " + $ch_str
		[Void]$ch_contents.Add($ch_txt_line)
		[Void]$ch_contents.Add($ch_break)
		[Void]$ch_contents.Add("")
		}
	$ch_contents
	}

function Build-CmdContent
	{
	Param(
		[Parameter(Mandatory=$True,Position=1)]
		[string]$cc_str
  		)
	$cc_contents 	= New-Object System.Collections.ArrayList
	
	if ($cc_str -match ";")
		{
		$cc_strs = $cc_str.Split(";")
		$cc_strs | %{
			[Void]$cc_contents.Add($_)
			}
		}
	else
		{
		[Void]$cc_contents.Add($cc_str)
		}
	$cc_contents
	}

function Create-AbWordTables ($table_name, $t_obj)
	{
	Write-Host "Working on $table_name"
	$selection.ParagraphFormat.Alignment = 1
	# Get table column headers
	$table_hdrs				= $t_obj[0].psobject.properties | select Name | %{ $_.Name }

	$rows					= 1
	$columns				= $table_hdrs.Count
	$range 					= $selection.Range
	$table					= $document.Tables.add($range,$rows,$columns)
	$table.Borders.Enable	= $true	
	$selection.InsertCaption(-2, " $table_name") 

	# Build table column headers
	$y = 1
	foreach ($table_hdr in $table_hdrs)
		{
		$Table.Cell(1,$y).Shading.BackgroundPatternColor = $wdColorGray15
		$table.cell(1,$y).Borders.Enable	= $true
		$table.cell(1,$y).Range.Font.Bold 	= $true
		$table.cell(1,$y).Range.Text 	= $table_hdr
		$y++
		}	
		
	# Build table body
	$x = 2
	foreach ($table_row in $t_obj)
		{
		$y = 1
		[Void]$table.rows.Add()
		foreach ($table_hdr in $table_hdrs)
			{
			if ($table_row | select -ExpandProperty $table_hdr)
				{
				$cell_value = $table_row | select -ExpandProperty $table_hdr
				$table.cell($x,$y).Range.Font.Bold 	= $false
				$table.cell($x,$y).range.text 		= $cell_value
				$y++
				}
			else
				{
				$cell_value = "not_specified"
				$table.cell($x,$y).Range.Font.Bold 	= $false
				$table.cell($x,$y).range.text 		= $cell_value
				$y++				
				}
			}
		$x++
		}
	$selection.EndKey(6)
	$selection.TypeParagraph()
	}

function Create-PsWordTables ($table_name, $t_obj, $num_empty_rows)
	{
	Write-Host "Working on $table_name"
	$selection.ParagraphFormat.Alignment = 1
	# Get table column headers
	$table_hdrs				= $t_obj[0].psobject.properties | select Name | %{ $_.Name }

	$rows					= 1
	$columns				= $table_hdrs.Count
	$range 					= $selection.Range
	$table					= $document.Tables.add($range,$rows,$columns)
	$table.Borders.Enable	= $true	
	$selection.InsertCaption(-2, " $table_name") 

	# Build table column headers
	$y = 1
	foreach ($table_hdr in $table_hdrs)
		{
		$Table.Cell(1,$y).Shading.BackgroundPatternColor = $wdColorGray15
		$table.cell(1,$y).Borders.Enable	= $true
		$table.cell(1,$y).Range.Font.Bold 	= $true
		$table.cell(1,$y).Range.Text 		= $table_hdr
		$y++
		}	
		
	# Build table body
	$x = 2
#	foreach ($table_row in $t_obj)
#		{
		

#		foreach ($table_hdr in $table_hdrs)
#			{
#			if ($table_row | select -ExpandProperty $table_hdr)
#				{
#				$cell_value = $table_row | select -ExpandProperty $table_hdr
#				$table.cell($x,$y).Range.Font.Bold 	= $false
#				$table.cell($x,$y).range.text 		= $cell_value
#				$y++
#				}
#			else
#				{
#				$cell_value = ""
#				$table.cell($x,$y).Range.Font.Bold 	= $false
#				$table.cell($x,$y).range.text 		= $cell_value
#				$y++				
#				}
#			}
#		$x++
		while ($x -le $num_empty_rows)
			{
			$y = 1
			while ($y -le $columns)
				{
				[Void]$table.rows.Add()
				$table.cell($x,$y).range.text = ' '
				$y++
				}
			$x++
			}
#		}
	$selection.EndKey(6)
	$selection.TypeParagraph()
	}

function SearchAWord($Document,$findtext,$replacewithtext)
	{
	$FindReplace		= $Document.ActiveWindow.Selection.Find
	$matchCase 			= $false;
	$matchWholeWord 	= $true;
	$matchWildCards 	= $false;
	$matchSoundsLike 	= $false;
	$matchAllWordForms 	= $false;
	$forward 			= $true;
	$format 			= $false;
	$matchKashida 		= $false;
	$matchDiacritics 	= $false;
	$matchAlefHamza 	= $false;
	$matchControl 		= $false;
	$read_only 			= $false;
	$visible 			= $true;
	$replace 			= 2;
	$wrap 				= 1;
	$FindReplace.Execute($findText, $matchCase, $matchWholeWord, $matchWildCards, $matchSoundsLike, $matchAllWordForms, $forward, $wrap, $format, $replaceWithText, $replace, $matchKashida ,$matchDiacritics, $matchAlefHamza, $matchControl)
	}

function Setup-BuildPaths ($dirs_str)
	{
	$setup_dirs = $dirs_str.Split(";")

	foreach ($setup_dir in $setup_dirs)
		{
		if (!(Test-Path -Path $setup_dir)) 	
			{
				Write-Host "Creating Directory: $setup_dir"
				New-Item -ItemType Directory -Path $setup_dir | Out-Null
				sleep -Milliseconds 250
			}
		}
	}