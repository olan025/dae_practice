Param(
  [Parameter(Mandatory=$True,Position=1)]
  [string]$build_cmds,
  [Parameter(Mandatory=$True,Position=2)]
  [string]$build_docs,
  [Parameter(Mandatory=$True,Position=3)]
  [string]$build_id
)

$global_base_path 		= (Get-Location).Path
$global_fn_file			= Join-Path -Path $global_base_path -ChildPath "global_functions.ps1"
$global_env_file		= Join-Path -Path $global_base_path -ChildPath "global_environmentals.ps1"
$global_presite_file	= Join-Path -Path $global_base_path -ChildPath "global_presite.xls"
$global_conf_path		= Join-Path -Path $global_base_path -ChildPath "conf"

# Source global environmentals
. $global_env_file

# Source global functions
. $global_fn_file

# Create any required directories that don't exist
Setup-BuildPaths ($setup_dirs_str)

if (Test-Path -Path $global_presite_file)
	{
	$existing_csv_files = (Get-ChildItem -Path $global_conf_path) | %{ $_.Name }
	if ($existing_csv_files)
		{
		foreach ($existing_csv_file in $existing_csv_files)
			{
			$csv_path = Join-Path $global_conf_path -ChildPath $existing_csv_file
			Remove-Item -Path $csv_path | Out-Null
			}
		}
	Export-WSToCSV -datasource_name $global_presite_file
	$gdata = Get-CsvData -datasource_name $global_presite_file -select_all_str "global_customers|global_locations|global_datacenters" -build_id $build_id
	Get-TableDefs -datasource_name $global_presite_file
	}

$global_build_ids = $gdata.get_Item("global_build_ids") | where { $_.build_id -eq $build_id }

foreach ($row in $global_build_ids)
 	{
	$global_build_id 			= $row.build_id
	$global_customer_id			= $row.customer_id
	$global_location_id			= $row.location_id
	$global_datacenter_id   	= $row.datacenter_id
	$global_esxi_cluster_name 	= $row.esxi_cluster_name
	$global_esxi_host_model		= $row.esxi_host_model
	$global_ucs_domain_name		= $row.ucs_domain_name
	$global_ucs_host_profile	= $row.ucs_host_profile
	$global_nexus_stack_name	= $row.nexus_stack_name
	$global_cdot_cluster_name	= $row.cdot_cluster_name
	$global_cdot_node_model 	= $row.cdot_node_model
	$global_customer 	= $gdata.get_Item("global_customers")	| where { $_.customer_id -eq $global_customer_id }
	$global_datacenter 	= $gdata.get_Item("global_datacenters") | where { $_.datacenter_id -eq $global_datacenter_id }
	$global_location 	= $gdata.get_Item("global_locations") 	| where { $_.location_id -eq $global_location_id }
	$global_networks 	= $gdata.get_Item("global_networks") 	| where { $_.build_id -eq $global_build_id }

	if ($global_esxi_cluster_name -ne "not_used")
		{
		$vmware_build_cmd = "$vmware_module_script –datasource_name $vmware_datasource_name -build_id $build_id"
		"Invoke-Expression $vmware_build_cmd"
		Invoke-Expression $vmware_build_cmd
		}
	if ($global_ucs_domain_name -ne "not_used")
		{
		$ucs_build_cmd 	= "$ucs_module_script –datasource_name $ucs_datasource_name -build_cmds $build_cmds -build_docs $build_docs -build_id $build_id"
		"Placehoder for Invoke-Expression $ucs_build_cmd"
		#Invoke-Expression $ucs_build_cmd
		}
	if ($global_nexus_stack_name -ne "not_used")
		{
		$nexus_build_cmd 	= "$nexus_module_script –datasource_name $nexus_datasource_name -build_cmds $build_cmds -build_docs $build_docs -build_id $build_id"
		"Placehoder for Invoke-Expression $nexus_build_cmd"
		#Invoke-Expression $nexus_build_cmd
		}
	if ($global_cdot_cluster_name -ne "not_used")
		{
		$cdot_build_cmd 	= "$cdot_module_script –datasource_name $cdot_datasource_name -build_cmds $build_cmds -build_docs $build_docs -build_id $build_id"
		"Invoke-Expression $cdot_build_cmd"
		Invoke-Expression $cdot_build_cmd
		}
	}