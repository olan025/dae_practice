# Define paths for all supported build modules

$setup_dirs_str			= ""
$global_base_path = (Get-Location).Path
$global_conf_path 		= Join-Path -Path $global_base_path -ChildPath "conf"
$global_reports_path 	= Join-Path -Path $global_base_path -ChildPath "reports"
$setup_dirs_str			= $global_conf_path + ";" + $global_reports_path

$vmware_base_path		= Join-Path -Path $global_base_path -ChildPath "vmware"
$vmware_conf_path 		= Join-Path -Path $vmware_base_path -ChildPath "conf"
$vmware_cmds_path 		= Join-Path -Path $vmware_base_path -ChildPath "cmds"
$vmware_reports_path 	= Join-Path -Path $vmware_base_path -ChildPath "reports"
$vmware_images_path 	= Join-Path -Path $vmware_base_path -ChildPath "images"
$vmware_templates_path 	= Join-Path -Path $vmware_base_path -ChildPath "templates"
$vmware_module_script	= Join-Path -Path $vmware_base_path -ChildPath "esxi_ks_maker-v1.2_ja_globalized.ps1" 	
$vmware_datasource_name	= Join-Path -Path $vmware_base_path -ChildPath "vspherepresite.xls"
$setup_dirs_str			= $setup_dirs_str + ";" + $vmware_base_path + ";" + $vmware_conf_path + ";" + $vmware_cmds_path + ";" + $vmware_reports_path + ";" + $vmware_images_path + ";" + $vmware_templates_path

$ucs_base_path			= Join-Path -Path $global_base_path -ChildPath "ucs"
$ucs_conf_path 			= Join-Path -Path $ucs_base_path -ChildPath "conf"
$ucs_cmds_path 			= Join-Path -Path $ucs_base_path -ChildPath "cmds"
$ucs_reports_path 		= Join-Path -Path $ucs_base_path -ChildPath "reports"
$ucs_images_path 		= Join-Path -Path $ucs_base_path -ChildPath "images"
$ucs_templates_path 	= Join-Path -Path $ucs_base_path -ChildPath "templates"
$ucs_module_script		= Join-Path -Path $ucs_base_path -ChildPath "ucsbuilder.ps1" 	
$ucs_datasource_name	= Join-Path -Path $ucs_base_path -ChildPath "ucspresite.xls"
$setup_dirs_str			= $setup_dirs_str + ";" + $ucs_base_path + ";" + $ucs_conf_path + ";" + $ucs_cmds_path + ";" + $ucs_reports_path + ";" + $ucs_images_path + ";" + $ucs_templates_path

$nexus_base_path		= Join-Path -Path $global_base_path -ChildPath "nexus"
$nexus_conf_path 		= Join-Path -Path $nexus_base_path -ChildPath "conf"
$nexus_cmds_path 		= Join-Path -Path $nexus_base_path -ChildPath "cmds"
$nexus_reports_path 	= Join-Path -Path $nexus_base_path -ChildPath "reports"
$nexus_images_path 		= Join-Path -Path $nexus_base_path -ChildPath "images"
$nexus_templates_path 	= Join-Path -Path $nexus_base_path -ChildPath "templates"
$nexus_module_script	= Join-Path -Path $nexus_base_path -ChildPath "nexusbuilder.ps1" 	
$nexus_datasource_name	= Join-Path -Path $nexus_base_path -ChildPath "nexuspresite.xls"
$setup_dirs_str			= $setup_dirs_str + ";" + $nexus_base_path + ";" + $nexus_conf_path + ";" + $nexus_cmds_path + ";" + $nexus_reports_path + ";" + $nexus_images_path + ";" + $nexus_templates_path

$cdot_base_path			= Join-Path -Path $global_base_path -ChildPath "netapp"
$cdot_conf_path 		= Join-Path -Path $cdot_base_path -ChildPath "conf"
$cdot_cmds_path 		= Join-Path -Path $cdot_base_path -ChildPath "cmds"
$cdot_reports_path 		= Join-Path -Path $cdot_base_path -ChildPath "reports"
$cdot_images_path 		= Join-Path -Path $cdot_base_path -ChildPath "images"
$cdot_templates_path 	= Join-Path -Path $cdot_base_path -ChildPath "templates"
$cdot_module_script		= Join-Path -Path $cdot_base_path -ChildPath "cDOTClusterBuilder_v7.ps1" 	
$cdot_datasource_name	= Join-Path -Path $cdot_base_path -ChildPath "cdotpresite_v7.xls"
$setup_dirs_str			= $setup_dirs_str + ";" + $cdot_base_path + ";" + $cdot_conf_path + ";" + $cdot_cmds_path + ";" + $cdot_reports_path + ";" + $cdot_images_path + ";" + $cdot_templates_path

$wdColorGray15 			= 14277081
$database_name			= 'clusterbuilder'

$date_time 	= Get-Date -Format "MM/dd/yyyy@HH:mm"
$section_break 			= "#" * 80
$script_user 			= [Environment]::UserName
$script_user_domain 	= [Environment]::UserDomainName
$script_machine_name	= [Environment]::MachineName
$script_run_str			= "Run by: " + $script_user_domain + "`\" + $script_user + " on system " + $script_machine_name + " at " + $date_time
$all_svm_protocols 		= @("nfs","cifs","fcp","iscsi")