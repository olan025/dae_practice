﻿##################### Environment and Common Variables ##########################
	$reportrecipient = "rgrendahl2@kroll.com" # Who the Build Log Goes to at the end
	$smtpserver = "mailer.ccp.edp.local"
	$reportfrom = "vAutomator@krollontrack.com"
	$runuser = ([System.Security.Principal.WindowsIdentity]::GetCurrent()).name
	$rundate = get-date
	$path = "c:\My Dropbox\vAutomator"
	$vcInventory = "$path\csvs\vclist.csv"
	$hostinventoryfile = "path\csvs\hostinventory.csv"
	$dclistfile = "$path\csvs\dc-list.csv"
	$guestInventoryfile = "$path\csvs\guestinventory.csv"
	$netInventoryfile = "$path\csvs\networks.csv"
	$vmbuildersleeptime = "15"
	$logfilelocation = "$path\logs"
	$outputfilelocation = "$path\output"
	$syslogserver = "10.40.0.119" # Also may exist in dclist.csv if multiple

##################### General Scripting Toggles ######################
	# on Screen output colors
	$goodcolor = "Green"
	$warningcolor = "Yellow"
	$failurecolor = "Red"

	# $vc value now defined by function Test-ViConnections
	#$vc = "e2p1psh1vcz0001" # if only 1, put in the name. if Multiple, enter "multiple"
	#$vc = "multiple" # uncomment if environment is using vclist.csv for multiples