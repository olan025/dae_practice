#####################################################################################
### script 			- template.ps1									#################
### Orig Author 	- grendahl										#################
### Date 			- 10.28.2010									#################
### Version			- 1.0											#################
#####################################################################################
### Mod Author		-												#################
###	Mod Date		-												#################
#####################################################################################
param 
		(  
			# This parameter is read by Test-ViConnections function to determine what to do about $vc.
			[Parameter(Position=0, Mandatory=$false)]  
			[string] $pvc = ""
		) 

####### SCRIPT NAME  Used for Logging ###############
	$scriptname = "vmhost_net_report"	
	$debug = "on" # on or off

## Load Environmentals.ps1 and  Load Functons.ps1
if(! (Test-Path ..\environmentals.ps1)){echo "You are not executing this from the right location.";exit}
. ..\environmentals.ps1
	if(! $?) {Write-Warning "Did not load environmentals.ps1";exit}
	Else {Write-Host "Loaded Environmentals.ps1"}
if(! (Test-Path ..\functions.ps1)){echo "You are not executing this from the right location.";exit}
. ..\functions.ps1
	if(! $?) {Write-Warning "Did not load functions.ps1";exit}
	Else {Write-Host "Loaded Functions.ps1"}

## Setup Log, Setup Output Locatoin, Load VI-ToolKit, Connect to VC if needed
	Test-LogLocation
	$logfile = Define-Logfile
	$outputdir = Test-OutputLocation
	Load-ViToolkit
	$vc = Test-ViConnections
	$output = "$outputdir\"  + $scriptname + "_" + $vc + "_" + $runDate.day + "-" + $runDate.Month + "-" + $runDate.Year + ".csv"

################## SCRIPT BODY START - Assume Connected to VC ###################


################## SCRIPT BODY END - Assume Connected to VC still ###############
#Kill-Connections # Used to disconnect everything at the end of a VI session
