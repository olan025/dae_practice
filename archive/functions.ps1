###################### FUNCTION LIBRARY ###############################
# Function checks to see if the ViToolKit is loaded and loads it if not already there.
Function Load-Vitoolkit 
	{
		$snapins = Get-PSSnapin | where {$_.Name -match "VMware.VimAutomation.Core"}
		if($snapins -eq $null){add-pssnapin VMware.VimAutomation.Core}
	}

Function Send-MailFile ($file)
	{
		send-mailmessage -from $reportfrom -to $reportrecipient -subject $scriptname -attachment $file -smtpserver $smtpserver -Body "$scriptname"
	}

# Function checks to see if there are any connections and will then kill them
Function Kill-Connections
	{
		If($DefaultVIServer)
			{
				Disconnect-Viserver -Server $DefaultViServer -confirm:$false
				echo "Disconnected $DefaultViServer"
			}
		If($DefaultVIServers)
			{
				Disconnect-VIServer -Server $DefaultVIServers -Confirm:$false
			}
	}

# Script to sort out the connected VC.
# 1. Kill connections and attach to one specified as parameter
# 2. If not connected and no parameter, it will prompt user for vc
# 3. If connected, and no parameter, then it will use the existing conection
Function Test-VIConnections
	{
	If((! $DefaultVIServer) -and (! $pvc))
		{
			Write-Host "Connection to VirtualCenter not found."
			$vc = Read-Host "Enter Virtual Center to connect to."
			Connect-VIServer $vc | Out-Null
		}
	ElseIf($pvc)
		{
			$vc = $pvc
			Write-Host "VC entered as parameter."
			Kill-Connections | Out-Null
			Connect-VIServer $vc | Out-Null
		}
	ElseIf($DefaultVIServer)
		{
			Write-Host "You are connected to $DefaultVIServer."
			$vc = $DefaultViServer.Name
		}
	Else 
		{
			Echo "Test-VIConnections state unexpected. I am confused."
		}
		Return $vc
	}
	
	
# Makes logfile directory for a script and checks it each time the script is run.
Function Test-LogLocation
	{
	if(! $scriptname){echo "Scriptname Variable Not Defined in Script";exit}
	if(! (test-path "$logfilelocation\$scriptname\"))
		{
			MD "$logfilelocation\$scriptname\" | Out-Null
			Write-Host "LogDirectory for $scriptname Created."
		}
	Else 
		{
			Write-Host "Log Directory for $scriptname exists."
		}
	}

# Creates the $logfile variable and places it in the LogFileLocation
Function Define-Logfile
	{
		if(! $logfilelocation){echo "Output file location not defined in environmentals.ps1";exit}
		if(! $scriptname){echo "Scriptname Variable Not Defined in Script";exit}
		if(! (test-path "$logfilelocation\$scriptname")){Test-LogLocation}
		if(! $?){echo "Logfile Issues!"}
		$logfile = "$logfilelocation\$scriptname\" + $scriptname + "_" + $runDate.Month + "-" + $runDate.day  + "-" + $runDate.Year + ".log"
		"#"*80 > $logfile
		echo "Execution Date: $runDate" >> $logfile
		echo "Execution By: $runUser" >> $logfile
		echo "Execution On: $env:computername" >> $logfile
		if($debug -eq "On"){echo "!!!Execution Mode Debug Enabled!!!" >>$logfile}
		echo "Execution of: $scriptname" >> $logfile
		"#"*80 >> $logfile
		
		Return $logfile
	}
	
# Makes output directory for a script and checks it each time the script is run.
Function Test-OutputLocation
	{
		if(! $outputfilelocation){echo "Output file location not defined in environmentals.ps1";exit}
		if(! $scriptname){echo "Scriptname Variable Not Defined in Script";exit}
		if(! (test-path "$outputfilelocation\$scriptname\"))
		{
			MD "$outputfilelocation\$scriptname\" | Out-Null
			Write-Host "Output Directory for $scriptname Created."
		}
	Else 
		{
			Write-Host "Output Directory for $scriptname exists."
		}
	$outputdir = "$outputfilelocation\$scriptname\"
	Return $outputdir
	}
	
# Tests the connection to a defaultviserver and prompts user with vclist
Function Test-ViConnection
	{
		if(! $DefaultVIServer)
		{
			Echo "No Active Connection to a Virtual Center Found"
			$vcInventory
			Echo "Try connecting to one of these Virtual Centers first";exit
		}
	}

# Custom VIServer Connect
Function Custom-VIconnect ([string]$vc)
	{
		if(! $vc){echo "VC Variable not defined!";exit}
		connect-viserver -Server ($vc.ToUpper())
		if(! $DefaultViServer){echo "Not connected to VC $vc.";exit}
	}

# Wait till all tasks are finished
Function Finish-Tasks 
	{
		$status = Get-Task -Status "running" | select Id, PercentComplete
		while($status -ne $null)
		{
			$status = Get-Task -Status "running" | select Id, PercentComplete
			Start-Sleep 10
		}
	}

# Function taken from Virtual-Al - Great little Logging feature!
Function Write-CustomOut ($Details)
	{
		$LogDate = Get-Date -Format T
		Write-Host "$($LogDate) $Details"
	}

# Function taken from Virtual-Al - Great little Logging feature!
Function Write-CustomLog ($Details)
	{
		$LogDate = Get-Date -Format T
		echo "$($LogDate) $Details" >> $logfile
	}

Function Write-CustomCombo ($Details)
	{
		$LogDate = Get-Date -Format T
		Write-Host "$($LogDate) $Details"
		echo "$($LogDate) $Details" >> $logfile
	}

# Grabs the API Version of an ESX Host Object
Function Check-VMhostAPIVersion ($vmhost)
	{
		if(! $vmhostview){$vmhostview = $vmhost | get-view}
		$apiversion = $vmhostview.Config.Product.ApiVersion
		Return $apiversion
	}

# Convert IP to Binary
Function ConvertTo-BinaryIP ( [String]$IP ) 
	{
		$IPAddress = [Net.IPAddress]::Parse($IP)
		Return [String]::Join('.',
		$( $IPAddress.GetAddressBytes() | %{
		[Convert]::ToString($_, 2).PadLeft(8, '0') } ))
	}
	
# Convert IP to Decmial
Function ConvertTo-DecimalIP ( [String]$IP ) 
	{
		$IPAddress = [Net.IPAddress]::Parse($IP)
		$i = 3
		$IPAddress.GetAddressBytes() | %{
		$DecimalIP += $_ * [Math]::Pow(256, $i); $i-- }
		Return [UInt32]$DecimalIP
	}

## Test Script for seeing if the datastores match in an esx host
function Check-ClusterDiskConsistancy 
	{
	param($cluster)
	if($cluster -eq "")
		{
			Write-Host "Specify a Cluster"
			break
		}
		
	$vmhosts = get-cluster $cluster | get-vmhost
	$hostcount = $vmhosts.count
	$vmhost0count = ($vmhosts[0] | Get-ScsiLun -LunType disk).count
	$err = 0
	
	For($i=1;$i -lt $hostcount; $i++)
		{
			$comparecount = 0
			$vmhost = $vmhosts[$i]
			$comparecount = (Get-ScsiLun -VmHost $vmhost -LunType disk).count
			If($comparecount -ne $vmhost0count)
				{
					$err++
				}
		}
	
	If($err -gt 0)
		{
			Return $false
		}
	Else 
		{
			Return $true
		}
	
	}

function Get-VmhostsConnected ($vmhosts)
	{
		if(! $vmhosts){$vmhosts = Get-VMHost}
		$vmhostsConnected = $vmhosts | where {$_.State -eq "Connected"}
		if($vmhostsConnected){Return $vmhostsConnected}
		Else {Return $false}
	}

function Get-VMhostsStandAlone ($vmhosts)
	{
		if(! $vmhosts){$vmhosts = Get-VMHost}
		$vmhostsStandalone = $vmhosts | where {$_.IsStandAlone -eq $true}
		if($vmhostsStandalone){Return $vmhostsStandalone}
		Else {Return $false}
	}

function Get-VMhostsESXi ($vmhosts)
	{
		if(! $vmhosts){$vmhosts = Get-VMHost}
		$vmhostsview = $vmhosts | Get-View
		$vmhostsesxi = $vmhostsview | where {$_.Config.Product.ProductLineId -eq "embeddedEsx"}
		if($vmhostsesxi){Return $vmhostsesxi}
		Else {Return $false}
	}
	
function Get-VMhostsESXFull ($vmhosts)
	{
		if(! $vmhosts){$vmhosts = Get-VMHost;echo "DOH"}
		$vmhostsview = $vmhosts | Get-View
		$vmhostsesxfull = $vmhostsview | where {$_.Config.Product.ProductLineId -eq "esx"}
		if($vmhostsesxfull){
					Return $vmhostsesxfull
				}
		Else {
			Return $false
		}
	}


# Sets the Syslog setting on ESX hosts passed to this function.
function Check-VMhostSyslog ($vmhost)
{
	if(! $syslogserver){echo "Syslog Server variable not defined. Check Environmentals or dclists.csv.";break}
	$syslogcheck = (Get-VMHostSyslogServer -VMHost $vmhost).host
	if($syslogcheck -eq $null)
		{
			echo "$vmhost syslog not configured. Setting to $syslogserver"
			if(! $logfile){Write-CustomLog "$vmhost syslog not configured. Setting to $syslogserver"}
			Set-VMHostSysLogServer -VMHost $vmhost -SysLogServer $syslogserver -SysLogServerPort 514
		}
	elseif($syslogcheck -eq $syslogserver)
		{
			echo "$vmhost syslog already correct."
			if(! $logfile){Write-CustomLog "$vmhost syslog already correct."}
			
		}
	elseif($syslogcheck -ne $syslogserver)
		{
			echo "$vmhost syslog currently set to $syslogcheck. Updating to $syslogserver"
			if(! $logfile){Write-CustomLog "$vmhost syslog currently set to $syslogcheck. Updating to $syslogserver"}
			Set-VMHostSysLogServer -VMHost $vmhost -SysLogServer $syslogserver -SysLogServerPort 514
		}
}

# Generates a list of esx names with VMK and Service Console IPs.
function Get-VmhostsIpList
{
	Param($inputObject)
	if(! $logfile){Write-CustomLog "Entered Get-VMhostsIPList Function"}
	$report = @()
	if(! $inputObject){$inputObject = Get-VMHost | Sort-Object -Property Name;echo "Collecting hosts"}
	
	Foreach($vmhost in $inputObject)
	{
		$vmhostname = $vmhost.name
		echo "$vmhostname"
		Foreach($vmknic in $vmhost.NetworkInfo.Virtualnic)
			{
				$row = "" | Select VMhost, Type, Nic, IP, Mac
				$row.VMhost = $vmhostname
				$row.Type = "vmk"
				$row.Nic = $vmknic.DeviceName
				$row.IP = $vmknic.IP
				$row.Mac = $vmknic.Mac
				$report += $row
			}
		Foreach($scnic in $vmhost.NetworkInfo.Consolenic)
			{
				$row = "" | Select VMhost, Type, Nic, IP, Mac
				$row.VMhost = $vmhostname
				$row.Type = "sc"
				$row.Nic = $scnic.DeviceName
				$row.IP = $scnic.IP
				$row.Mac = $scnic.Mac
				$report += $row
			}
	
	}
	if(! $logfile){Write-CustomLog "Finished Get-VMhostsIPList Function"}
	Return $report
}		

	
# switch binary or decimal ip back to common dot'd ip
Function ConvertTo-DottedDecimalIP( [String]$IP ) 
	{
		Switch -RegEx ($IP) {
		"([01]{8}\.){3}[01]{8}" {
		Return [String]::Join('.', $( $IP.Split('.') | %{
		[Convert]::ToInt32($_, 2) } ))
    }
    "\d" 
	{
      	$IP = [UInt32]$IP
      	$DottedIP = $( For ($i = 3; $i -gt -1; $i--) {
        $Remainder = $IP % [Math]::Pow(256, $i)
        ($IP - $Remainder) / [Math]::Pow(256, $i)
        $IP = $Remainder
       } 
	  )
      Return [String]::Join('.', $DottedIP)
    }
    default 
	{
      Write-Error "Cannot convert this format"
    }
  }
}

# calculates the subnetmask bit length
Function ConvertTo-MaskLength( [String]$Mask ) 
	{

		$IPMask = [Net.IPAddress]::Parse($Mask)
		$Bits = "$( $IPMask.GetAddressBytes() | %{
			[Convert]::ToString($_, 2) } )" -Replace "[\s0]"
		Return $Bits.Length
	}

# convert mask length to a subnet mask
Function ConvertTo-Mask( [Byte]$MaskLength ) 
	{
		Return ConvertTo-DottedDecimalIP ([Convert]::ToUInt32(
		$(("1" * $MaskLength).PadRight(32, "0")), 2))
	}
	
# Calculate the subnet network address
Function Get-NetworkAddress( [String]$IP, [String]$Mask ) 
	{
		Return ConvertTo-DottedDecimalIP $(
			(ConvertTo-DecimalIP $IP) -BAnd
			(ConvertTo-DecimalIP $Mask))
	}

# Pause Function to cause delay until key press
Function Pause
{
	Write-Host -NoNewLine "Press any key to continue . . . "
	[Console]::ReadKey($true) | Out-Null
	Write-Host
}

# Get-Old Tools - Fast
# http://get-admin.com/blog/scripting/powershell-scripting/powercli-speedboost-advanced-functionality-hidden-within-get-view/
Function Get-OldTools
{
	Get-View -ViewType "VirtualMachine" `
	-property Guest, Name `
	-filter @{
				"Guest.GuestFamily"="windowsGuest";
				"Guest.ToolsStatus"="ToolsOld";
				"Guest.GuestState"="running"
			}
			
	Select-Object -Property Name, @{
			Name = "ToolsVersion"
			Expression = {$_.Guest.ToolsVersion}
			} MoRef
			Sort-Object ToolsVersion
}